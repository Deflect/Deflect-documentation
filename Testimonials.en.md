---
layout: default
title: Testimonials
lang: en
ref: Testimonials
tags:
 - Testimonials
---

Testimonials
============

Over the past years we have helped millions of people access Deflect protected
websites. Some of them have responded with a personal testimony, collated
herein.

![map of Deflected websites](img/Deflect-clients-may-2015.png)

> Deflect.ca did an excellent job at helping protect the website of a renowned
> human rights NGO from an extended ddos attack. The website had been down for
> almost a week. We first tried Cloudflare, but it looked as if the ddos still
> came through. To be clear: that was with all settings maxed out on a paid
> 200$/month business plan. Once we switched to Deflect things looked like
> normal traffic and the active chat support provided by Deflect really helped
> keeping things safe and under control.

Kasper Souren


> IPOS directly benefited from Deflect's expertise and professionalism
> when our main website was subject to an unprecedented attack. At the
> time the services of similar companies including CloudFlare and Google
> PageSpeed failed to protect IPOS' election tracking poll against a major
> DDOS attack during the 2013 presidential elections in Iran. However,
> Deflect were able to quickly setup a CDN front and accept traffic from
> IPOS' main domain and fight back against the attack. Thank you Deflect.

Ali Reza, IPOS solution


> Deflect has provided us with website protection that our organization could
> not otherwise afford. The service thereby allows our organization to function
> with the level of security required by small activist groups working to defend
> human rights. Without Deflect, our primary communication tool - the website -
> would be at a significantly higher risk of attack. Perhaps even more
> important, bringing the CTC into the eQualit.ie family has provided access to
> a community of expertise that is deeply valued.

Carole Samdup, Executive Director, Canada Tibet Committee


> Our organization has been using secure hosting services for several years for
> our partners, targeting closed societies and repressive countries. We have
> decided to try the Deflect service and were very satisfied. The team was
> quick, responsive and professional. We were provided all the information and
> support needed throughout the whole processes of activation and monitoring.
> Thank you Deflect.


> Deflect team were the pioneers in creating a unique and brilliant idea to
> protect oppressed sites against ddos and other types of attacks. They managed
> to protect one of our client's sites on multiple occasions very effectively,
> allowing journalists the freedom of speech which is one of the major internet
> essences. What I also noticed and admired about deflect team is their fast
> support response, friendly manner and dealing with clients as members of their
> team which creates a bond of likeness and respect. Thank you Deflect.

Mahmoud Jisri. Operations manager at CyberiaHosting.ca


> Besides the fact that Deflect has helped our foundation to prevent DoS
> attacks, all the team was always very kind in helping even when all we needed
> was some advice regarding security.

Roberto Palomino. ElCultivo.mx


> It was great to have the technical support from Deflect to keep our site
> online and prevent further attacks. The team reacted swiftly to answer our
> questions and solve problems. Our inspiration to do journalism from exile
> would have been frustrated had it not been to the service Deflect gave us.
> Thank you Deflect!

Mesfin


> Deflect saved my journalism and my independent capacity to publish the results
> of my investigations. I specialise in corruption and the effect of
> globalisation and organised crime on international sports federations like
> FIFA and the IOC. My blogging is welcomed by sports fans globally because I
> reveal corruption scandals that many 'establishment' sports correspondents
> don't want to touch. Last Autumn my website was overwhelmed by botnets. Many
> days	it was knocked offline and even my ethical ISP was brought down for a
> while. It was soon clear that a particular disclosure had discomforted some
> rich and powerful administrators in world sport. They could not sue because
> the story was documented. So they resorted to dirty tricks. Deflect galloped
> to my rescue and did a whole lot of things that I, a simple investigative
> reporter, will never understand. My site was restored and remains so. My
> thanks - plus those of a world-wide community committed to honesty in
> reporting.

Andrew Jennings


> On behalf of Osservatorio Balcani e Caucaso (OBC), the Italian non-profit
> media organisation I direct, I would like to express our gratitude and
> appreciation for the cyber-protection services you are currently offering us.
> During the past few months, OBC (www.balcanicaucaso.org) has been the target
> of a persistent and intense DDos attack, most probably launched from abroad
> and aimed at intimidating us. Needless to say, it provoked a great deal of
> preoccupation and endangered the work we have been carrying out for more than
> 13 years, however we could not compromise the quality and reliability of the
> news we publish and tried our best to protect ourselves. As a short-term
> emergency measure, we turned to a premium mitigation system that in no way
> could be economically sustainable in the medium and long run for a small
> organisation like ours. Very soon after, we were surprised to receive your
> offer and precious solidarity that has allowed us to start again focussing on
> our mission, that is to say the production of news, research, training and
> dissemination of activities on the democratisation processes of South-east
> Europe and the Caucasus. Your help made us feel less vulnerable and also a
> part of a wider spectrum of CSOs committed to protecting rights, justice and
> peace worldwide. Thank you once more for your solidarity and friendship. And
> of course for the protection you guarantee.

Luisa Chiodi PHD


> We came to know deflect in June of 2012, when a violence broke in Rakhine
> states of Myanmar(Burma). As a result till today there are more than 150,000
> IDPs in various camps. During that time our website, the first Rohingya News
> agency, has been continuously attacked and went down. With full dedication,
> determination, support and expertise, the Deflect team brought it back on-line
> and till today they are protecting. We the world most persecuted people, the
> Rohingyas wish you all the best and thank the donors who helped them in
> continuing their mission. Thank you very much.

Ronnie 

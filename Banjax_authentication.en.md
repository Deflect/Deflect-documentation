---
layout: default
title: Banjax authentication
lang: en
ref: Banjax_authentication
tags:
 - Deflect Tech
 - Security
---

Banjax authentication
=====================

When your website is behind Deflect, requests for a new page will come from our
caching servers. This means that they may be several minutes old and may not
have the very latest updates. This is not ideal for when you are editing the
website and need to see updates immediately. Deflect provides a special way to
authenticate yourself to the system and access your website **without
caching**. We call this Banjax authentication. After you have created the
password in the [Dashboard](Dashboard_Walkthrough/Step2_Admin_Credentials), the login page
to your website (e.g. /wp-admin, /login, /administrator, etc.) will appear like
this:

![Banjax Authentication](img/Banjax_authentication.png)

Only those in possession of the authentication password will be able to proceed.
This has an extra side effect of protecting your website editorial from password
brute-force attacks.

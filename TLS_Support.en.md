---
layout: default
title: HTTPS/TLS Support
lang: en
ref: TLS_Support
tags:
 - Support
 - Deflect Tech
 - Security
---

# TLS Support

## TLS/HTTPS: protect your website's visitors' privacy

When someone visits your website by entering a common URL starting with http://
their logins and passwords and other confidential data can be intercepted by
undesired third parties. Strongly motivated attackers can even alter visited
pages to try injecting malware into your users' browsers.

Threats like these can be prevented by allowing your readers to access your
website through a so-called encrypted tunnel [TLS
encryption](https://en.wikipedia.org/wiki/Transport_Layer_Security), by using a
URL starting with https:// instead of http:// (in many browsers you will see a
green padlock next to the address bar when https:// is used). This option must
be made available by the administrator of the visited website, and this page
explains what you can do. 

**It's free and easy to enable TLS on your website with Deflect, as users'
safety is our first concern.**

**Read** [**this guide**](Dashboard_Walkthrough/Step3_TLS) **to learn how to set up your website to make
TLS encryption available to your visitors.**


### How TLS/HTTPS works on Deflect

Normally when two computers communicate securely, without a [caching
proxy](About_Deflect#Deflect_in_Action) service like Deflect, the client (for
example a browser) requests the server's TLS certificate, checks that it trusts
the certificate, then encrypts the connection using it. ISPs, businesses and
governments - those who own the networks, or any other actor sitting in the
middle between the client and the server - cannot look at what's inside the
communication.

This is a good solution to prevent ill-intentioned intrusions between your
readers and your website, but it clearly defeats the purpose of the distributed
caching. This is because the caching proxy server, like other middle parties,
does not have access to the encrypted data and is therefore unable to cache it.
To overcome this problem, Deflect has developed a system that creates two
encrypted tunnels instead of just one - one for the connection between the
public and the Deflect edges where your website is cached, the other for the
connection between the Deflect network and your actual website (origin).

![Deflect TLS scheme](img/TLS-scheme.png)

Deflect offers several options to encrypt connections to your website, and can
generate TLS certificates both for encrypting connections between your readers
and the Deflect edges (public-facing certificates) and between the edges and
your website (origin certificates).

Deflect generates public-facing TLS certificates through
[Let's Encrypt](https://letsencrypt.org/), a certification authority
(CA) launched in 2015 and co-founded by the [Electronic Frontier
Foundation](https://eff.org) that issues free and easy-to-install SSL
certificates with the explicit aim of facilitating and spreading the use of web
cryptography.
Since Let's Encrypt certificates need to be renewed frequently, to encrypt
connections between edges and your web server Deflect uses longer-lasting origin
certificates signed by its own [certification
authority](https://en.wikipedia.org/wiki/Certification_authority).
Of course, if you already have a TLS certificate, you can use it to encrypt
connections between Deflect and your website (origin certificate) as well as
between the public and the edges (public-facing certificate), but in the latter
case you will need to share your private TLS key with us. If you would rather
not share it with anybody, we can generate a Let's Encrypt certificate to
encrypt connections that reach any of the Deflect edges while connections
between Deflect edges and your web server will be encrypted by your own
certificate.

**If you are not familiar with TLS but think that encrypting connections to
your website is a good idea, we are willing to help!** Contact us through the
dashboard panel and we will look for a solution that can work for you.


### Enable HTTPS connections to your website under Deflect

If you are enabling HTTPS connections for the first time on your website and you
**don't have a TLS certificate** yet, you have 3 options:

#### Ask Deflect to do everything for you

![TLS option1](img/TLS1.png)

You can ask Deflect to do everything for you: we will generate a public-facing
[Let's Encrypt](https://letsencrypt.org/) certificate to encrypt connections
between your readers and Deflect edges, and an origin certificate to secure
connections between Deflect and your website (see option 1 in [the public-facing
certificate setup
guide](Dashboard_Walkthrough/Step3_TLS#Create_and_manage_your_public-facing_certificate) and option 1
in [the guide to the Deflect Dashboard Control
Panel](Dashboard_Walkthrough/Step5_Control_Panel#TLS_configuration_and_certificates)).

*This option is recommended if you are not familiar with OpenSSL. Both
public-edge and edge-origin communications will be encrypted and the edge can
validate that it is talking to your server rather than some other server
impersonating yours.*

#### Generate your origin certificate and have it signed by Deflect

![TLS option2](img/TLS2.png)

If you feel comfortable using OpenSSL, Deflect will only generate a
public-facing Let's Encrypt certificate, while **you generate your origin
certificate** and have it **signed by Deflect** so that connections between
edges and origin can be validated and Deflect can be sure that it is talking to
your server (see option 1 in [the public-facing certificate setup
guide](Dashboard_Walkthrough/Step3_TLS#Create_and_manage_your_public-facing_certificate) and option 2
in [the guide to the Deflect Dashboard Control
Panel](Dashboard_Walkthrough/Step5_Control_Panel#TLS_configuration_and_certificates).

*This is an advanced option, recommended if you are comfortable using OpenSSL
and creating a certificate signing request. You won't need to share your private
key with Deflect, and the edge will still be able to validate your origin
certificate.*

#### Generate a self-signed origin certificate

![TLS option3](img/TLS3.png)

You can also **generate a self-signed origin certificate**
without having it signed by Deflect. Deflect will only generate a public-facing
Let's Encrypt certificate, while connections between Deflect and your website
will be encrypted through your own self-signed certificate (see option 1 in
[the public-facing certificate setup guide](Dashboard_Walkthrough/Step3_TLS#Create_and_manage_your_public-facing_certificate).

*You can choose this option if you would rather not share your private key with
anyone but have no experience with certificate signing requests. All connections
will be encrypted, but Deflect will not be able to confirm that the certificate
really belongs to you.*


If your website already accepts HTTPS connections and **you have a TLS
certificate** that you would like to keep using, you have two options:


#### Upload a custom TLS certificate bundle

![TLS option4](img/TLS4.png)


**Upload a custom TLS certificate bundle** - in this case, both
connections to Deflect and connections between Deflect and your website will be
encrypted through your TLS certificate (see option 2 in
[the public-facing certificate setup guide](Dashboard_Walkthrough/Step3_TLS#Create_and_manage_your_public-facing_certificate).

*This option allows you to keep using the certificate you have already
generated, but requires you to share your private key with Deflect.*


#### Use a third-party certificate on your origin server

![TLS option5](img/TLS5.png)


Ask Deflect to generate a public-facing Let's Encrypt certificate
while you use your **third-party certificate as an origin certificate** (see
option 1 in [the public-facing certificate setup guide](Dashboard_Walkthrough/Step3_TLS#Create_and_manage_your_public-facing_certificate).

*This option allows you to keep using the certificate you have already
generated without having to share your private key with anyone.*


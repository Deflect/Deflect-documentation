---
layout: default
title: Why Deflect
lang: en
ref: Why_Deflect
tags:
 - Features
---

Why Deflect
===========


Deflect is not the only DDoS-mitigation service available for human rights
defenders, civil society groups, independent media and freedom of speech
activists. At the time of writing, here are two other (well-known) initiatives
which offer DDoS mitigation services free of charge.

- [Project Galileo](https://www.cloudflare.com/galileo)
- [Project Shield](https://projectshield.withgoogle.com/en/about)

Each offers a robust, capable service, so you will likely be protected
whichever one you choose. However, there are key differences with each provider
that we believe are crucial to consider before making your choice. You can also
read an excellent overview of the field with extensive advice on how to protect
your site from an attack
[here](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

<table>
<tr>
<th><strong>Features</strong>
</th>
<th align="center"><strong>Deflect</strong>
</th>
<th align="center"><strong>Project Galileo</strong>
</th>
<th align="center"><strong>Project Shield</strong>
</th></tr>
<tr>
<td><strong>Open Source</strong>
</td>
<td align="center"><a href="Deflect_DIY/Deflect_intro" title="What is
Deflect">Yes</a>
</td>
<td align="center"><a rel="nofollow" class="external text"
href="https://github.com/cloudflare">Some Components</a>
</td>
<td align="center"><a rel="nofollow" class="external text"
href="https://developers.google.com/speed/pagespeed/psol">Optimization
libraries</a>
</td></tr>
<tr>
<td><strong>SSL</strong>
</td>
<td align="center"><a href="TLS_Support" title="SSL/TLS Support">Yes</a>
</td>
<td align="center"><a rel="nofollow" class="external text"
href="https://www.cloudflare.com/ssl/">Yes</a>
</td>
<td align="center">Yes
</td></tr>
<tr>
<td><strong>Headquarters</strong>
</td>
<td align="center">Canada
</td>
<td align="center">USA
</td>
<td align="center">USA
</td></tr>
<tr>
<td><strong>Loyal to</strong>
</td>
<td align="center">Users - non commercial
</td>
<td align="center">Stakeholders - commercial
</td>
<td align="center">Shareholders
</td></tr>
<tr>
<td><strong>Sign-Up</strong>
</td>
<td align="center"><a rel="nofollow" class="external free"
href="https://dashboard.deflect.ca/signup">https://dashboard.deflect.ca/signup</a>
</td>
<td align="center">By referral or <a rel="nofollow" class="external text"
href="http://www.cloudflare.com/galileo">online</a>
</td>
<td align="center">By referral or <a rel="nofollow" class="external text"
href="https://projectshield.withgoogle.com/public/#application-form">online</a>
</td></tr>
<tr>
<td><strong>Eligibility</strong>
</td>
<td align="center"><a href="Eligibility" title="Eligibility">Deflect's
eligibility criteria</a>
</td>
<td align="center">NGOs and civil society groups
</td>
<td align="center">Media, elections and human rights orgs
</td></tr>
<tr>
<td><strong>Disk Encryption</strong>
</td>
<td align="center">Yes
</td>
<td align="center">No
</td>
<td align="center">No
</td></tr>
<tr>
<td><strong>Data Logging</strong>
</td>
<td align="center">Opt-out
</td>
<td align="center">Yes
</td>
<td align="center">Yes
</td></tr>
<tr>
<td><strong>Infrastructure</strong>
</td>
<td align="center"><a href="About_Deflect" title="About Deflect">About
Deflect</a>
</td>
<td align="center"><a rel="nofollow" class="external text"
href="https://www.cloudflarestatus.com">Data Centers</a>
</td>
<td align="center"><a rel="nofollow" class="external text"
href="http://www.google.ca/about/datacenters/inside/locations/index.html">Data
Centers</a>
</td></tr>
</table>

Having said all that, Deflect is the only service which:

- Operates on a non-profit basis and exists specifically to protect human rights
  groups and independent media websites
- Does not accept groups which promote hate speech, incite violence, advocate
  censorship, use our network for criminal activities or wilfully disseminate
  propaganda
- Never changes or denies service due to very large attack size
- Encrypt our partners' data at rest: on each caching server
- Does not give away or sell any data to a third party
- Is trying to [solve the problem](Deflect_DIY/Deflect_intro), not just react to it
- Is a dedicated service run by [dedicated people](Support) with backgrounds in
  human rights, journalism, digital security and tech-activism. We do this
  because we believe in it and we want to support the activists and journalists
  who risk so much to do their work.

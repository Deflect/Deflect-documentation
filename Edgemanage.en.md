---
layout: default
title: Edgemanage
lang: en
ref: Edgemanage
tags:
 - Deflect Tech
 - Deflect Labs
---

Edgemanage
==========

Edgemanage is a tool used to ensure maximum availability of Deflect edges. It
observes the health of [edges](About_Deflect#Design) in the Deflect edge pool
and selects the best ones to be active (i.e. in DNS) at any given time.

See the Edgemanage documentation on [Github](https://github.com/equalitie/edgemanage).

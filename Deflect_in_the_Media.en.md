---
layout: default
title: Deflect in the media
lang: en
ref: Deflect_in_the_Media
tags:
 - Media
---

Deflect in the Media
====================

Recent Media Coverage
---------------------

**Deflect Labs' 3rd report -- Black Lives Matter**


*English*

- Matthew Braga, "How Canadians are keeping Black Lives Matter website online",
  CBC News, Dec 19, 2016
- Colin Daileda, "Black Lives Matter website hit with more than 100 DDoS attacks
  this year", Mashable, Dec 16, 2016
- Cory Doctorow, "How hackers tried to knock blacklivesmatter.com offline",
  Boing Boing, Dec 14, 2016
- Corin Faife, "The DDoS vigilantes trying to silence Black Lives Matter", Ars
  Technica, Dec 14, 2016
- Justin Ling, "Black Lives Matter Website Saw Over 100 DDoS Attacks in Seven
  Months", Motherboard, Dec 14, 2016
- Russell Brandom, "Anonymous groups attacked Black Lives Matter website for six
  months", The Verge, Dec 14, 2016
- Monique Judge, "Report: Black Lives Matter Website the Target of More Than 100
  DDoS Attacks", The Root


**Deflect Labs' 2nd report -- BDS Movement**

*English*

- Ali Abunimah, "Attacks on BDS websites linked to Israel", The Electronic
  Intifada, June 6, 2016
- Jack Moore, "BDS Movement Accuses Israel of Series of Cyber Attacks",
  Newsweek, June 3, 2016
- Sarah Lazare, "Tech Report Says Palestinian Rights Orgs Are Under
  Sophisticated Cyber Attack", Alternet, June 3, 2016
- Justin Ling, "Someone Is Launching Cyber Attacks on Websites Critical of
  Israel", Vice News, June 2, 2016 "Attacks on BDS websites smack of Israel’s
  despair at its growing isolation", BDSMovement.net, June 2, 2016


*Hebrew*

- חוקרים: אתרי ה-BDS וארגוני זכויות אדם ספגו מתקפות "ברמת תחכום נדירה, Haaretz,
  June 2, 2016


**Deflect Labs' 1st report -- Kotsubynske**

*English*

- Justin Ling, "The Activists on the Forefront of Ukraine's Cyberwar", Vice
  News, March 29, 2016

*Ukrainian*

- Konstantin Oleinik, "Як ІТ-технології покращують безпеку України", Global
  Ukraine News, May 23, 2016


**On Deflect**

*English*

- Jillian C. York, "A guide to online security for activists", The Electronic
  Intifada', Aug 2, 2016


*Czech*

- "Kyberválka na Ukrajině má kanadskou stopu.  Ottawa poslala miliony", iDNES,
  April 1, 2016


*German*

- Andrea Jonjic, "Weitere Dimension in der DDoS-Debatte: Angriffe gegen
  unabhängige kritische Medien, die sich kaum wehren können", Netzpolitik, Jan
  29, 2013

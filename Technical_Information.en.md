---
layout: default
title: Technical Information
lang: en
ref: Technical_Information
tags:
 - Get started
---


Technical Information
=====================

There are some simple technical steps and a few ethical requirements to join the
Deflect network. Please, check if you meet Deflect's [eligibility
criteria](Eligibility) before applying.

Information to supply
---------------------

- We need a list of all domains you want protected by Deflect
- We need a copy of your domain's [zone
  file](https://en.wikipedia.org/wiki/Zone_file)
- We need a technical description of the server your website is located on
- We need a description of all dynamic, interactive features on your website
  (Flash, forums, banners, advertising, widgets, APIs, etc.)
- We would like to know the average monthly traffic to your website if that is
  possible

If you think you have all the information you need, you can read about [**what
happens next**](Procedure) and check out the [**Walkthrough**](Dashboard_Walkthrough/Walkthrough)
for a step-by-step account of how to join. 

---
layout: default
title: BotnetDBP - Botbanger
lang: en
ref: BotnetDBP_Botbanger
tags:
 - Deflect Tech
 - Deflect Labs
 - BotnetDBP
---

# Botbanger

[![Botbanger](img/GitHub-Mark-120px-plus.png)](https://github.com/equalitie/Botbanger)

[Botbanger](https://github.com/equalitie/Botbanger) is the component responsible
for using the SVM model constructed by Learn2Ban to test HTTP traffic and
determine the legitimacy of the requester in real time.

The core functions of the tool are as follows:

1. gathering of log data
2. building a feature set of requesting IP addresses 
3. calculating the probability of each requester being a bot.

Data aggregation and retrieval is done by the Botsniffer tool which passes the
log data to Botbanger for parsing. The log line is converted into a vector
matching the feature set considered by the Learn2ban SVM model.

After this feature set has been derived, the model prediction functionality is
called and the result is recorded. The decision to ban is based on statistical
evidence and thus requires multiple examples to have accuracy, so for each
interaction with the Deflect network, information about a given IP is logged. If
the IP continues to make requests, Botbanger will build a narrative of its
behaviour and recalculate the likelihood of its legitimacy for every new request
received. By analysing the determined features for a given IP address, paired
against the pre-defined models and based on its threshold certainty, the address
is then identified as either legitimate or malicious.

In the event that an IP is determined to be malicious, it is marked and sent to
the Swabber module for banning at the IP Tables level. 

---
layout: default
title: Step01 - Start setup 
lang: en
ref: Step01_Start_Setup
tags:
 - Dashboard Walkthrough
 - Support
---

On your first login to the dashboard you will be asked to click a button to
confirm that you want to start setting up your website on Deflect

Press **Start Setup** to proceed to the [**next step**](Step1_IP_DNS_setup)

![Set up your website](../img/Step0b.png)

If you registered your account by mistake, in this page you can also delete your
user account by clicking the "Delete" button.


Press **Start setup** to continue to the [**next step**](Step1_IP_DNS_setup).


**Back to** [**Step 0**](Step0_Register)

---
layout: default
title: Step0 - Register 
lang: en
ref: Step0_Register
tags:
 - Dashboard Walkthrough
 - Support
---

The Dashboard allows you to register your website(s) for Deflect protection. It
is also a [control panel](Step5_Control_Panel) where you can manage DNS
records, security settings and traffic statistics once you are behind Deflect.

If you meet Deflect's [eligibility criteria](../Eligibility) and can provide us
with the [necessary technical information](../Technical_Information), you can
get started at once.

You only need few steps to create an account:

- Go to
  [https://dashboard.deflect.ca/signup](https://dashboard.deflect.ca/signup)
- Specify the name of the website you want to protect
- Enter the IP address of the server it is hosted on (if you do not know it,
  please go to
  [http://ip-lookup.net/domain.php](http://ip-lookup.net/domain.php) and enter
  your website name to receive the IP address. It will be formed by four numbers
  separated by dots, e.g. **173.254.238.77**)
- Enter your email address 
- Add your [PGP
  key](http://en.flossmanuals.net/basic-internet-security/ch027_mail-encryption-gpg/)
  (optional) if you want us to send you encrypted emails. **Please note that
  automated emails sent by the Deflect Dashboard will not be encrypted**

![Signup form](../img/Signup.png)

Once you have completed this form, you will receive an email with a link. By
clicking that link, you will access an activation page where you can enter your
new password.
<a name="NewPassword"></a>
![Activate your account](../img/Dashboard_activate1.png)

Choose a strong password, enter it twice and then click the "Set password"
button. You will get a confirmation that your password has been saved with a
link to the sign-in form.

|                                     |                                         |
|-------------------------------------|-----------------------------------------|
| ![Signin](../img/Dashboard_signin.png) | ![Sign in](../img/Dashboard_activate2.png) |

Log in through the sign-in form and proceed to the [**next step**](Step01_Start_Setup).


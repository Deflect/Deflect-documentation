---
layout: default
title: Step2 - Admin Credentials
lang: en
ref: Step2_Admin_Credentials
tags:
 - Dashboard Walkthrough
 - Support
---

On this page you will create a new password to protect access to your website's
editorial. This is explained in further detail in the [page on Banjax
authentication](../Banjax_authentication). Specify your website's login address.
For example, default addresses for the following web content systems editorials
are:

- Wordpress: /wp-admin
- Drupal: /admin
- Joomla: /administrator

Copy and paste whatever login address is appropriate for your website (you only
need to put in the address that appears after the domain name)

![Security](../img/Step2.png)

You will be able to create a new password in this section should you forget or
wish to change it. Please proceed to the [**next step**](Step3_TLS).

**Back to** [**Step 1**](Step1_IP_DNS_setup)

---
layout: default
title: Step1 - IP and DNS setup 
lang: en
ref: Step1_IP_DNS_setup
tags:
 - Dashboard Walkthrough
 - Support
---

The first screen you will see when starting your setup will ask you to confirm
(if this is the first website you register) or to enter your website's [IP
address](https://learn.equalit.ie/wiki/How_does_the_Internet_actually_work%3F#Internet_Protocol_.28IP.29_Address).
If you do not know it, please go to
[http://ip-lookup.net/domain.php](http://ip-lookup.net/domain.php) and enter
your website name to receive the IP address. It will be formed by four numbers
separated by dots, e.g. like this: 173.254.238.77

When you have entered or checked your website's IP address, click "Save IP".
  
![IP address form](../img/Dasboard1.png)

At this point the Deflect system has located your website's DNS settings. They
will be shown on this screen for you to edit and confirm. Please make sure that
all relevant DNS records are represented. Even though every domain will have
their own records, here is a short explanation

- **A**  website.com  129.128.127.210 -> this is the main record for
    the domain, also referred to as the origin IP or root record
- **CNAME**  www  website.com -> this is usually an alias (CNAME) allowing
  people to type www.website.com and get to your site
- **MX**  website.com  mail.website.com -> this record is for email (MX), e.g. user@website.com
- **A**  mail.website.com  129.128.127.210 -> this record allows email delivery
  to your server. It is connected with the MX record above
- **NS**  ns1.website.com -> nameserver for your domain. You can leave this
  record as is
- **TXT**  website.com  'This is a test' -> sometimes a text record (TXT) is
  appended to a domain. You can leave this as is

![DNS settings](../img/Step1.png)

The system has been pretty good in correctly identifying DNS records. Check your
DNS zone scheme to see if something is different and add or edit any settings
that are missing or incorrect. When you're sure that everything is correct,
press the **Save, my records are OK** button to proceed to the [**next step**](Step2_Admin_Credentials).

### Important

- For every subdomain you want protected by Deflect that points to a different
  IP address, please use the 'Add new website' button to register as a separate
  site.


**Back to** [**Step 01**](Step01_Start_Setup)

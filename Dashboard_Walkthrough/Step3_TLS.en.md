---
layout: default
title: Step3 - HTTPS/TLS 
lang: en
ref: Step3_TLS
tags:
 - Dashboard Walkthrough
 - Support
---

<a name="HTTPS_TLS_configuration"></a>
## HTTPS / TLS configuration

On Deflect, you can configure TLS encryption in [many different ways](../TLS_Support).
Depending on your specific needs, you can choose to:

1. **redirect all HTTP traffic to HTTPS** - the safest solution for
non-experts: Deflect will do everything for you and insecure connections will be
redirected to an encrypted tunnel. *Please note that for TLS/HTTPS to work,
your web server needs to support TLS encryption*.

![redirect all HTTP traffic to HTTPS](../img/07_SSL_a.png)
    
2. **accept both HTTP and HTTPS connections** - this option is recommended if you
   prefer to retain full control of which elements are served over HTTP or over
   HTTPS. You can use it if you are planning to set up TLS but still need to
   configure your web server accordingly.

3. **only accept HTTPS connections** - this option only allows access over an
   encrypted connection. Your readers will be unable to connect if they do not
   request https:// URLs. *This option can result in broken links and should be
   used with caution*.

4. **use HTTP only** and not encrypt connections - not recommended: does not
   allow for encrypted connections and is not secure. You should choose this
   option only if your web hosting solution doesn't allow for HTTPS. If you are
   interested in protecting your visitors, you could use this as a temporary
   option and ask us to help you find a solution to encrypt your connections. If
   you do plan to use TLS, but it isn't configured yet, you can use option 2.

![HTTP only](../img/07_SSL_d.png)

<a name="Create_and_manage_your_public-facing_certificate"></a>
## Create and manage your public-facing certificate

To encrypt connections between your visitors and Deflect, you will need a
public-facing certificate, that Deflect will create automatically for you or
that you can buy or generate through a third party and upload to Deflect. You
can choose between these options when setting up your website for the first time
on Deflect or in the ["Security" tab](Step5_Control_Panel#Security_tab) of the
Dashboard Control Panel.

1. By checking the "Use a free Let's Encrypt certificate" option, you will ask
   Deflect to generate a public-facing Let's Encrypt certificate for you. This
   will allow Deflect to automatically encrypt all traffic between Deflect and
   your users with Let's Encrypt-issued certificates.

![Use a free Let's Encrypt certificate](../img/07_SSL_b.png)

2. If you prefer to use your third-party certificate, click "Upload a custom TLS
   certificate bundle" in the "Public TLS Certificates" of the "Security" screen
   and then upload your certificate and key file as requested. You may also need
   to upload the chain certificate (CA file).

![Upload a custom TLS certificate bundle](../img/07_SSL_c.png)

When you are satisfied with your initial TLS setup, click "Save TLS
configuration" to proceed to the [**final step**](Step4_Finalize). When your
website registration is complete, you will be able to change these settings in
the ["Security" tab](Step5_Control_Panel#Security_tab) of the Dashboard Control
Panel.

**Back to** [**Step 2**](Step2_Admin_Credentials)

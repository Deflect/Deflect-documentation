---
layout: default
title: Dashboard Walkthrough index
lang: en
ref: Walkthrough
tags:
 - Dashboard Walkthrough
 - Support
---

How to join Deflect step-by-step

- [Step0: Register](Step0_Register)
- [Step01: Start Setup](Step01_Start_Setup)
- [Step1: IP and DNS setup](Step1_IP_DNS_setup)
- [Step2: Admin Credentials](Step2_Admin_Credentials)
- [Step3: HTTPS/TLS](Step3_TLS)
- [Step4: Finalize](Step4_Finalize)
- [Step5: Control Panel](Step5_Control_Panel)

---
layout: default
title: Step4 - Finalize
lang: en
ref: Step4_Finalize
tags:
 - Dashboard Walkthrough
 - Support
---

This is the final screen for registering your website on Deflect. When you reach
this screen, our team will receive the website's details and begin setting it up
on our infrastructure. When the set-up is complete, you will be notified here to
change **NS Records** for this domain and point them to Deflect. 

![Finalizing your configuration 1](../img/Step4a.png)

When we have completed the set-up this screen will change to:

![Finalizing your configuration 2](../img/Step4b.png)

Now you need to point the domain's nameservers (NS) to Deflect.

This is usually done with your DNS provider. Log into your DNS hosting account
and look for the Nameserver setting. There will be two or more in there by
default.

Once this has been set, press **Done. NS settings saved**. This completes your
registration. The website will begin appearing on Deflect as the changes
propagate on the Internet. This usually takes between 1 - 48 hours. 

Proceed to the [**Control Panel guide**](Step5_Control_Panel) to learn how
to use the dashboard.

**Back to** [**Step 3**](Step3_TLS)

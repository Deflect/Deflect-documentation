---
layout: default
title: Step5 - Control_Panel
lang: en
ref: Step5_Control_Panel
tags:
 - Dashboard Walkthrough
 - Support
---

Once you have joined Deflect, you can use the Dashboard also as a control panel
where you can manage DNS records, add new websites, change your account settings
or the panel language, report incidents, ask for support, manage your websites'
settings and notify an attack.

- [Side panel](#Side_panel)
    - [Deflected websites](#Deflected_websites)
    - [My account](#My_account)
    - [Help](#Help)
- [Website settings](#Website_settings)
    - [DNS records tab](#DNS_records_tab)
    - [Users tab](#Users_tab)
    - [Security tab](#Security_tab)
        - [TLS configuration and certificates](#TLS_configuration_and_certificates)
    - [Settings tab](#Settings_tab)
- [Notify a DDoS attack](#Notify_a_DDoS_attack)

<a name="Side_panel"></a>
## Side panel
<a name="Deflected_websites"></a>
### Deflected websites

In the **Deflected websites** section of the side panel, you will find a list
of the websites you are protecting with Deflect.

If you are waiting for your request to be finalized, by clicking on the website
address in the sidebar, you will access a [page](Step4_Finalize) where you
can check the status of your request.

In the same section you can also add a new website by clicking on **"Add a new
website"**.

- To add a new site to the websites you want to protect, enter its URL
  in the form, click on "Add website", then follow the step-by-step
  instructions starting from [Step 1](Step1_IP_DNS_setup). *Note
  that you want to register a subsite (e.g. sub.domain.org) you will
  have to add it as a new website by clicking on this button.*

![Add new website](../img/Dashboard_NewWebsite.png)

Once your website is on Deflect, by clicking on its address in the
sidebar you will access a control panel to check your website's
statistics, [manage DNS records](#DNS_records_tab), [add new
users](#Users_tab), and configure [security](#Security_tab) and
[administration](#Settings_tab) settings.


<a name="My_account"></a>
### My account

In the **My account** section, you can manage your account settings by clicking
on **Settings**. You will thus access a [screen](Step0_Register#NewPassword) where
you can update your email address and password.

<a name="Help"></a>
## Help

In this section you can either Report an incident or ask for support.

If your protected website is experiencing problems, or you think that Deflect is
not working properly, please click on **"Report an incident"** and fill the form
specifying the concerned website and providing as many details as possible.

![Report an incident](../img/Dashboard_ReportIncident.png)

If, on the other hand, you need our help to set up your website, click on
**"Support"** and fill the form by choosing a support type, specifying the
concerned website and adding any information that may be needed in the comment.

![Support](../img/Dashboard_Support.png)


<a name="Website_settings"></a>
## Website settings

To manage your website, click on its address in the sidebar of the
Dashboard: you will access a control panel where you can check your
website's statistics, [manage DNS records](#DNS_records_tab), [add new
users](#Users_tab), and configure [security](#Security_tab) and
[administration settings](#Settings_tab).


<a name="DNS_records_tab"></a>
### DNS records tab

When you register a website on Deflect, the Deflect system locates
automatically your website's DNS settings, and you can simply accept
them or edit what needs to be changed by deleting or adding records. If
you need to change your DNS records when your site is already protected
by Deflect, you can click on the DNS records tab in your Dashboard.

![DNS records tab](../img/DNStab01.png)

Every change you make to your DNS records goes through a validation
process that checks if your changes are correct and your DNS records are
reliable. If your changes are correct, they will be implemented
automatically. As you will notice, the Dashboard will also inform you if
your change is pending or has been implemented, to offer you as much
control as possible over every operation.

![pending changes](../img/DNStab06.png)

Here is a short explanation on DNS records and their meaning:

- **A** website.com 129.128.127.210 -> this is the main record for the
  domain, also referred to as the origin IP or root record
- **CNAME** www website.com -> this is usually an alias (CNAME) allowing
  people to type www.website.com and get to your site
- **MX** website.com mail.website.com -> this record is for email (MX)
  e.g. user@website.com
- **A** mail.website.com 129.128.127.210 -> this record allows email
  delivery to your server. It is connected with the MX record above
- **NS** ns1.website.com -> nameserver for your domain.  You can leave
  this record as is
- **TXT** website.com 'This is a test' -> sometimes a text record (TXT)
  is appended to a domain. You can leave this as is


<a name="Users_tab"></a>
### Users tab

To manage your website's users, click on **"users"** in the toolbar. A window
with a list of users and a form for adding new users to the Deflect control
panel will appear.

If you want to allow new users to access the control panel for the selected
website, just enter the new user's email address in the form and click on **"Add
user"**. The new user will be added to the "Current users" list.

![Users tab](../img/Dashboard_users.png)

<a name="Security_tab"></a>
### Security tab

Click on **"security"** in the toolbar to access your website's security
  settings. In this section you can change your [administration
  credentials](Step2_Admin_Credentials) for accessing the editorial section of
  your website (for example the Wordpress or Joomla administration interface),
  and you can also add or change your [SSL/TLS certificates](Step3_TLS).

![Security tab](../img/Dashboard_security.png)

<a name="TLS_configuration_and_certificates"></a>
#### TLS configuration and certificates

*Before you set up your TLS configuration, learn how TLS encryption
works on Deflect [here](../TLS_Support).*

Read this guide on how to set up your [HTTPS / TLS
configuration](Step3_TLS#HTTPS_TLS_configuration).

Read this guide on how to
[Create and manage your public-facing TLS
certificate](Step3_TLS#Create_and_manage_your_public-facing_certificate).

To encrypt connections between Deflect edges and your website, you will
need to install an origin certificate in your web server. Deflect can
create automatically a certificate for you or you can buy or generate
one through a third party. You can choose between these options in the
"Origin Certificates" form in this section.

![Origin certs](../img/Dashboard_origin_certs.png)

- to ask Deflect to create an origin certificate for you, click the "Let
  Deflect generate a certificate and key bundle" button.
- to create an origin certificate yourself and upload a Certificate
  Signing Request for Deflect to sign, click the "Upload a certificate 
  signing request" button and paste Your PEM encoded CSR in the form
  that will automatically open.

By clicking the "Generate Origin certificate bundle" button, a
certificate will be generated automatically and appear in the list
of generated origin certificates. Click "Download bundle" to
download a zip file containing the files you need to install in your
web server to enable TLS connections with Deflect.

![Origin certs list](../img/Dashboard_origin_certs_list.png)


<a name="Settings_tab"></a>
### Settings tab

To manage general website configuration, click on **"settings"** in the toolbar.
In the section that will open, you will be able to change your website's IP
address and decide if you want to collect visitor logs (by default, this
function is disabled).

In the settings section, you can also decide how long your website's content
will stay cached in the Deflect servers. If your website is frequently updated,
it's a good idea to set it to the minimum value (10 minutes), but if your
content doesn't change often, increasing this number will help reduce the load
on your server.

![Settings tab](../img/Dashboard_settings.png)

<a name="Notify_a_DDoS_attack"></a>
## Notify a DDoS attack

If you have good reasons to think that one of your website is under attack, by
clicking on it in the **Deflected websites** sidebar section, you will access
the control interface for the relevant site. Next to the website's name, in the
top part of the screen, click on **"My site is under attack"**, and then click
on "One of the above applies, my site is under attack" in the following screen.
The Deflect team will immediately be notified that your website is under attack
and increase the level of protection.

![My site is under attack](../img/Site_under_attack.png)
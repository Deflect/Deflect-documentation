---
layout: default
title: Deflect Privacy Notice
lang: en
ref: Privacy_Notice
tags:
 - Features
---

Deflect Privacy Notice
======================

Our Data Security and Data Integrity Promise
--------------------------------------------

Our prevailing principle will always be the protection of your privacy and the
security of your information. If you are using our Services we know that privacy
and security are of the highest importance to you. We take all reasonable steps
and utilize the best free and open source tools at our disposal to protect your
data from loss, misuse and unauthorized access, disclosure, alteration and
destruction. We have put in place appropriate physical, electronic and
operational procedures to safeguard and secure your Data. We only process your
Data in a way that is compatible with the purpose for which it was collected or
authorized. At all times we strive to abide by our [Declaration for Distributed
Online Services](https://equalit.ie/declaration-distributed-online-services/).

We have appointed a Privacy Officer who is accountable for our data handling
practices. If you have a question or complaint about our data handling
practices, please contact us at privacy@equalit.ie.


1. Effective Date and Scope
---------------------------

This Privacy Notice is effective as of October 1, 2016 and forms an integral
part of our Terms of Service. In this privacy notice (Privacy Notice), the terms
“we”, “our” and “us” mean eQualit.ie.

This Privacy Notice governs our practices with respect to Data that we collect
through the Deflect Service about your organization’s websites that are
registered with Deflect (“you”) and visitors to your Deflected websites.


2. What Type of Data Do We Collect?
-----------------------------------

By Data we mean any type of information collected through the Services,
including your Personal Information and metadata related to your use of the
Service and visitors to your websites.

Personal Information is information that identifies you or could be combined
with other information, to identify you. Personal information may also be
information containing details as to whether you have visited a website or your
IP address if that personal information can be associated with you. The only
Personal Information we collect from you is your email address and the name of
the website you are registering. We may also ask to specify the name of the
organization this website represents and other details that may help us qualify
your eligibility for Deflect, if relevant. Other data may include any Personal
Information that you voluntarily provide us through our website, emails, or help
tickets that are associated with your user account. We keep your Personal
Information for as long as you are registered with us or until you delete your
account with us.

Some examples of other types of Data that we collect are:

- The date you registered for our services
- Your domain name
- Your DNS zone file
- Your IP address
- The IP addresses of visitors to your Deflected website Metadata associated
  with visitors’ browsing activity on your website.
- Session identifiers. While logged in, we keep a temporary session identifier
  on your computer that your software uses to prove your authentication state.
  This is deleted when you log out or the session expires.
- When you are editing your website protected by Deflect, you must authenticate
  yourself as the owner of the account. This will set a 24 hour cookie on your
  computer proving your credentials to the Deflect system and allowing you to
  access the backend.
- Emails, support tickets and associated metadata.


3. How Do We Use Data We Collect?
---------------------------------

We use the Data we collect (including your Personal Information) to provide our
Services, namely Deflect services for your website, administering your account,
and managing our relationship.

Some examples of how your Personal Information is used includes (i) registering
accounts and authenticating your access to the Deflect Dashboard and (ii)
communicating with you about Deflect.

What about metadata? By default, we collect metadata on your website’s visitors.
This is otherwise known as logging, and is used to provide you with statistics.
We also need this data in order to protect your website from malicious activity,
and apply advanced analytics on the metadata associated with your website’s
visitors. Should you opt out of metadata collection (logging) through the
Deflect Dashboard, metadata will only be retained for the time required to
perform the Service. Any metadata associated with malicious activity on our
network or your website we be retained indefinitely.

We do not use any third-party cookies or tracking of any kind.


4. Do We Disclose Your Personal Information to Others?
------------------------------------------------------

**No**, unless we have obtained your prior consent or if we are legally compelled to
do so.


5. Law Enforcement Requests
---------------------------

We may be forced to disclose Data, including your Personal Information, without
your knowledge or consent if we receive an order, subpoena, warrant or other
legal requirement issued by a court, tribunal, appropriate regulatory body or
other person with jurisdiction to compel disclosure of your information,
including your Personal Information. If we receive a request from a law
enforcement agency for access to Data, including Personal Information in the
course of an actual or potential investigation, our policy is to require the law
enforcement agency to obtain an order, subpoena or warrant. It is also our
policy to contest such an order, subpoena or warrant if we believe the order to
be unjustified.

Unless legally prohibited from doing so, we will notify you as soon as possible
of any order, subpoena or warrant to provide information about you and visitors
to your website, including Personal Information.


6. Storage of Data Outside Canada
---------------------------------

Data may be used or stored by us or our service providers and our affiliates
outside of Canada. If your Personal Information is used or stored outside of
Canada it will also be subject to the laws of the country in which it is used or
stored. In all instances, Data we collect will be protected using all reasonable
technical and legal means at our disposal to prevent any third-party access.


7. How Do We Protect Data?
--------------------------

We take administrative, technical and physical measures to safeguard Data,
including your Personal Information against loss, misuse or unauthorized access,
disclosure, alteration and/or destruction. We have put in place physical,
electronic, and managerial policies for managing and safeguarding Data. Wherever
possible, we encrypt Data at rest and in transit. The only access we retain to
encrypted Data is that which is necessary to perform the Services, including
administrative functions.

Deflect uses third-party services for the provision of its caching and logging
infrastructure. We choose carefully our affiliates and third-party service
providers to take comparable steps to ensure the protection of any Data that is
shared with them, but cannot assume responsibility for their treatment of your
personal Data. This is one reason why we strictly encrypt everything at rest.

Although we take precautions against possible breaches of our security systems,
no company can fully eliminate the risks of unauthorized access to Data. We
cannot guarantee that unauthorized access, hacking, data loss or breaches of our
security systems will never occur. Be aware of the risks and consider not
transmitting Personal Information to us if you consider that information to be
highly sensitive.


8. How Can You Access and Modify Your Personal Information or Make a Complaint?
-------------------------------------------------------------------------------

If you have an account, log in to access and modify the Personal Information
stored with your account. If you have questions or concerns about other Personal
Information collected by us and would like assistance accessing that
information, please contact our Privacy Officer at privacy@equalit.ie.

You may also choose to delete your account through the Deflect Dashboard.


9. Notice to Persons Outside of Canada
--------------------------------------

This Privacy Notice is governed by the laws of Canada and applicable provincial
law. By submitting Personal Information to us, you understand that your Personal
Information will be subject to the laws of Canada and applicable provincial
laws.


10. Changes to Our Privacy Notice
---------------------------------
We may need to change certain aspects of the Privacy Notice from time to time.
We will email you and post any substantive changes to this policy on our website
along with the effective date of those changes. 

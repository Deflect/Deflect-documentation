---
layout: default
title: About Deflect
lang: en
ref: About_Deflect
tags:
 - Features
---

About Deflect
=============


Deflect is a
[DDoS-mitigation](https://en.wikipedia.org/wiki/Distributed_denial_of_service#Handling)
service for NGOs, civil society groups, activist bloggers and independent media.
Built by digital security non-profit [eQualit.ie](https://equalit.ie), the
service is a free, open source and effective solution to mitigate
[DDoS](http://en.wikipedia.org/wiki/Denial-of-service_attack#Distributed_attack)
attacks.

Contents
--------

-   [1 Background](#Background)
    -   [1.1 Our approach](#Our_approach)
    -   [1.2 Design](#Design)
    -   [1.3 Protection offered](#Protection_offered)
    -   [1.4 Deflect in Action](#Deflect_in_Action)
-   [2 Details and limitations](#Details_and_limitations)
    -   [2.1 Cached Components](#Cached_Components)
    -   [2.2 Cookies](#Cookies)
    -   [2.3 Is it working?](#Is_it_working)
    -   [2.4 SSL](#SSL)
    -   [2.5 DNS](#DNS)
    -   [2.6 Deflect Customizations](#Deflect_Customizations)


<a name="Background"></a>
## Background

Most human rights and independent media groups do not have the financial
or technical resources to mitigate DDoS attacks, so Deflect was created
to provide this service free of charge. These attacks — undertaken by
infected
['bots'](https://en.wikipedia.org/wiki/Internet_bot#Malicious_purposes)
— can disable the targeted website and prevent access for legitimate
users, intimidating site owners and stifling free speech on the
Internet.

Commercial DDoS mitigation services are expensive and may alter their
terms of service if they believe a particular website is drawing very
heavy traffic. Many of our partners have come to us after a
disappointing experience with a well-known commercial service.

<a name="Our_approach"></a>
### Our approach

We set up distributed [reverse proxy
caches](https://en.wikipedia.org/wiki/Reverse_proxy) on a collection of
geographically distributed, low-cost hosting providers. Each host is
functionally equivalent, though we are learning which are the best
quality providers. Using short [time-to-live
DNS](https://en.wikipedia.org/wiki/Time_to_live#DNS_records),
distributed caching, IP blacklisting and other identified practices,
Deflect services multiple clients simultaneously at a low cost for us
and zero cost to them.

<a name="Design"></a>
### Design

Deflect is designed as a robust, low cost, non-proprietary and easily
reproducible system to provide protection to multiple websites, which we
call "Origins".

The system was created to remain neutral to different web servers, with
some limitations detailed below. It is built using Debian 6 VPSs, which
we call "Edges", and a controlling server we call "Controller". The
caching component is handled by [Apache Traffic
Server](http://trafficserver.apache.org/).

<a name="Protection_offered"></a>
### Protection offered

-   Absorbing 99%+ of traffic destined to your website. Check out our
    [traffic stats](https://www.deflect.ca/stats).
-   Hiding your server's location (IP address).
-   Preventing public access to editorial dashboards (e.g. /admin,
    /login, etc.).
-   Filtering out malicious requests using fail2ban, learn2ban and
    iptables rulesets.

<a name="Deflect_in_Action"></a>
### Deflect in Action

To access a Deflect-protected website:

1.  Enter the website's address in the browser.
2.  The DNS will retrieve an alias pointing to our pool of edges. One of
    these edges is then selected using [round robin
    DNS](https://en.wikipedia.org/wiki/Round-robin_DNS).
3.  If the requested address is permitted and the edge has the content
    of the page in its cache, it will immediately reply to the browser.
    If the content is not cached on the edge, it will be requested from
    the origin and sent to the browser.
4.  If the address is not permitted, a notification page is displayed.

The picture below gives a simple explanation:

[![INFOGRAPHIC](/img/INFOGRAPHIC3-1-pdf.jpg)](/img/INFOGRAPHIC3-1-pdf.jpg)

Details and limitations
-----------------------

<a name="Cached_Components"></a>
### Cached Components

Deflect handles web pages composed of many elements, including CSS,
Javascript, multimedia and large binary files. Page components that are
hosted on different domains ("widgets," traffic trackers, etc) are
handled in the regular manner.

Deflect currently caches responses for 10m, which can be tuned for
individual locations (longer for infrequently changing binary files,
shorter for online forums for example).

<a name="Cookies"></a>
### Cookies

While Deflect currently ignores cookies, returning the same object from
the cache regardless of any cookies present in the client request, it is
configurable on a per-domain and per-path basis. We can enable unique
treatment of different cookies for a site or part of a site, however we
effectively disable our ability to cache that site or part of site.
Nevertheless, the site will still be protected by our firewall analysis.
Query strings are treated as part of the URL - different query strings
will always be considered unique objects and cached as such. Responses
to POST requests are never cached.

<a name="Is_it_working"></a>
### Is it working?

You can tell Deflect is serving a page by looking at the HTTP headers
(using 'Inspect Element' on Chrome or Firefox); you'll see a Via: string
that returns an individual edge serving the requested webpage. It will
look similar to:

    Via:http/1.1 prometeus1.deflect.ca (ApacheTrafficServer/3.2.4 [uIcMsSfWpNeN:t cCMi p sS])

The caching response, in the above case [uIcMsSfWpNeN:t cCMi p sS] can
be interpreted [here](http://trafficserver.apache.org/tools/via.html).

<a name="SSL"></a>
### SSL

Deflect also supports SSL. For further information, see [this
page](/TLS_Support).

<a name="DNS"></a>
### DNS

DNS is configured for short
[TTL](https://en.wikipedia.org/wiki/Time_to_live#DNS_records) ("time to
live") to allow rapid addition/removal of nodes to the edge pool.

If you have any more questions, please see our [FAQ](FAQ) or
drop us an [email](mailto:info@equalit.ie) and we'll do our best to
answer it.

<a name="Deflect_Customizations"></a>
### Deflect Customizations

Over time, we're developing profiles for different Web servers. In the
meantime, we can provide customization for the following:

1.  Domains and their aliases (www.yoursite.org, yoursite.org)
2.  Cache "time to live"
3.  Protected locations (/admin)

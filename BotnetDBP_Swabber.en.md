---
layout: default
title: BotnetDBP - Swabber
lang: en
ref: BotnetDBP_Swabber
tags:
 - Deflect Tech
 - Deflect Labs
 - BotnetDBP
---

# Swabber

[![Swabber](img/GitHub-Mark-120px-plus.png)](https://github.com/equalitie/swabber)

[Swabber](https://github.com/equalitie/swabber) is responsible for managing the
actual banning of IP addresses identified by either Banjax or Learn2ban.

It uses a ZMQ-based pub-sub mechanism for communication and bans the given IP
addresses via IPTables to block identified malicious IP addresses at the TCP
level. Swabber has been implemented with several configurable parameters for
greater flexibility: 

- bantime - describes the amount of time that malicious IP addresses should be
  banned from further HTTP requests.
- bindstrings - provides the list of addresses on which to listen for bans. 
- interface - legitimate ban source to interact with 
- backend - Swabber supports the following methods of banning:
	- raw
        - IPTables commands
	- python-iptables library
	- host.deny

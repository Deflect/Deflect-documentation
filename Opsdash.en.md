---
layout: default
title: Opsdash
lang: en
ref: Opsdash
tags:
 - Deflect Tech
 - Deflect Labs
---

Opsdash
=======

Opsdash provides near realtime storage and querying of the large volume of
traffic reaching the Deflect edge (many millions of events per day).

It is composed of:

- an [Elasticsearch](https://www.elastic.co/products/elasticsearch) cluster for
  data storage and querying;
- a [Kibana](https://www.elastic.co/products/kibana) interface to quickly and
  easily create visualisations of complex data;
- [Logstash](https://www.elastic.co/products/logstash) to enrich and insert logs
  into Elasticsearch;
- [Log-courier](https://github.com/driskell/log-courier) to ship logs securely
  and rapidly from the edge to Logstash;
- [nginx](http://nginx.org/) to restrict access to the powerful Elasticsearch
  API.

---
layout: default
title: Deflect Apache Traffic Server
lang: en
ref: Deflect_Apache_Traffic_Server
tags:
 - Deflect DIY
---


Deflect Apache Traffic Server
=============================

ATS Build Notes
---------------

### Version 6.0.x 
#### Prerequisites 

- Install Debian-7.x (Wheezy) amd64 and upgrade it
- Reboot
- Create user and group (trafserv with UID and GID of 99) -- If not already created from a old version (uid:gid 99:99)
	
	# groupadd -g 99 trafserv
	# useradd -g trafserv  -u 99 -d /usr/local/trafficserver -m trafserv

- Prepare server for deveploment

	# apt-get install build-essential automake autoconf libtool flex bison


#### Compile from Source 

Find the version you will use
[here](http://archive.apache.org/dist/trafficserver/). Add equalitie repo for
apt-get:

	# echo "deb http://users.deflect.ca/repo/ wheezy main" > /etc/apt/source.list.d/deflect.list
	# apt-get update
	# apt-get install libssl-dev libhwloc-dev libcap-dev tcl-dev libpcre3-dev libncurses5-dev libcurl4-openssl-dev libspdylay-dev
	# cd /usr/local/src
	# wget http://archive.apache.org/dist/trafficserver/trafficserver-6.0.x.tar.bz2
	# wget http://archive.apache.org/dist/trafficserver/trafficserver-6.0.x.tar.bz2.md5
	  Pick the latest and replace the 6.0.x
	# md5sum trafficserver-6.0.x.tar.bz2
	  Make sure matches the trafficserver-6.0.x.tar.bz2.md5
	# tar jxvf trafficserver-6.0.x.tar.bz2
	# cd trafficserver-6.0.x
	# ./configure --prefix=/usr/local/trafficserver --enable-layout=Apache --with-user=trafserv --with-group=trafserv --enable-experimental-plugins \
	# --enable-reclaimable-freelist --enable-cppapi --with-ncurses --enable-wccp --with-xml=libxml2 --enable-spdy --with-build-number=<your build number>
	# make
	# make install
	# cd /usr/local/trafficserver
	# mkdir .ssh ; chown trafserv:trafserv .ssh ; chmod 700 .ssh
	# mv conf/ dist-conf


#### Banjax 


	# apt-get install git-core libyaml-cpp-dev libyaml-cpp0.5 libre2-dev libzmq-dev libboost-dev
	# git clone https://github.com/equalitie/banjax.git
	# cd banjax
	# ./autogen.sh
	# ./configure --libdir=/usr/local/trafficserver/modules
	# make
	# make install

The banjax.so is in src/.libs/banjax.so

- mv conf/ dist-conf


#### Additional 

- Use diff to fix your 'conf' directory to your preference.
	
	# diff -rupN orig new > patch_orig-new.patch

- Files to pay attention to when configuring DNET.
	- cache.config
	- records.config
	- remap.config
	- ssl_multicert.config

When happy with your migrated conf directory, launch this command:

	# cp -a new-conf /usr/local/trafficserver/conf

Make sure the permissions and ownership are correct. Do not start the traffic
server if you plan on using for the controller. It will write a lot of files
that are not needed.

We recommend to tar and bzip2 it up:

	# cd /usr/local
	# tar jcvf trafficserver-6.0.x-deflect.tar.bz2 trafficserver/ 


#### Conf Directory Recursive Patch Files 

I created these diffs with method below

	cp -R orig new
	vi new/* ''This is just my way of saying, edit the files.''
	diff -rupN orig new > patch_orig-new.patch

Apply patch with example above. Patch orig

	cd orig
	patch -p1 < ../patch_orig-new.patch
	*remember you can use --dry-run*


ATS SSL
-------

Mostly lifed from
[here](https://docs.trafficserver.apache.org/en/5.3.x/admin/security-options.en.html#using-ssl-termination).

**This is still not tested. My notes only. I will remove this when thoroughly tested and I know it works**

### SSL Termination

All private keys must be hosted on the Edge unless browser will give a warning.

Magic is now done in ssl_multicert.conf. Good information [here](http://trafficserver.apache.org/docs/trunk/admin/configuration-files/ssl_multicert.config.en.html)

	dest_ip=xx.xx.xx.xx ssl_cert_name=cert.pem ssl_key_name=key.pem ssl_ca_name=ca.ca_bundle

*Note: If the private key is located in the certificate file, then you do not
need to specify the name of the private key*

*ssl_ca_name= can be used to specify the location of a Certification Authorithy
change in case that differs from what is specified under records.config's
proxy.config.ssl.CA.cert.filename*

### Client and ATS Only

*Note: This would not be the type of setup we use unless we wanted to do a
encrypted tunnel to origin. This setup would mainly be used if your origin was
behind your firewall on a private network. This must be set up first, before we
go on to Client, ATS and Origin*

|        |                              |      |                          |      |
|--------|------------------------------|------|--------------------------|------|
| Client | ----->----- HTTPS --->------ | Edge | <<------- HTTP ------->> | Origin |
|        | ----<------ HTTPS ---<------ |      |                          |      |

records.conf. All ssl files are going to be in conf/ssl

	#CONFIG proxy.config.ssl.enabled INT 1(deprecated)
	#CONFIG proxy.config.ssl.server_port INT 443 (deprecated)
	CONFIG proxy.config.ssl.client.certification_level INT 1
	CONFIG proxy.config.ssl.server.cert.filename STRING NULL
	CONFIG proxy.config.ssl.server.cert.path STRING conf/ssl
	CONFIG proxy.config.ssl.server.private_key.filename STRING NULL
	CONFIG proxy.config.ssl.server.private_key.path STRING conf/ssl
	CONFIG proxy.config.ssl.CA.cert.filename STRING NULL
	CONFIG proxy.config.ssl.CA.cert.path STRING conf/ssl

See ssl_multicert.conf

See proxy.config.http.server_ports


### Client, ATS and Origin

*This is the setup we will concentrate on.*

|        |                              |      |                          |      |
|--------|------------------------------|------|--------------------------|------|
| Client | ----->----- HTTPS --->------ | Edge | <<------- HTTPS ------->>| Origin |
|        | ----<------ HTTPS ---<------ |      |                          |      |

records.conf 

	#CONFIG proxy.config.ssl.auth.enabled ????? I can't find info on this
	#CONFIG proxy.config.ssl.server_port (deprecated)
	CONFIG proxy.config.ssl.client.verify.server INT 
	CONFIG proxy.config.ssl.client.cert.filename STRING NULL
	CONFIG proxy.config.ssl.client.cert.path STRING conf/ssl
	CONFIG proxy.config.ssl.client.private_key.filename STRING NULL
	CONFIG proxy.config.ssl.client.private_key.path conf/ssl
	CONFIG proxy.config.ssl.client.CA.cert.filename STRING NULL
	CONFIG proxy.config.ssl.client.CA.cert.path conf/ssl


ATS SNI
-------


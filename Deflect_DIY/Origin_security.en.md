---
layout: default
title: Origin security
lang: en
ref: Origin_security
tags:
 - Deflect DIY
---


General origin security
=======================

While your origin is behind Deflect, you will not be immune to all attacks -
either targeted attacks or as part of the day-to-day attacks that any and all
webservers are subject to. The topic of improving webserver security is a very
broad one, and it would not be useful to comprehensively cover this on this
page. There are a few simple things that can be done as a matter of course,
however. 

SYN cookies
-----------

SYN cookies are a safeguard against SYN flood attacks. To temporarily enable
them in Linux, simply issue the following command

	echo 1 > /proc/sys/net/ipv4/tcp_syncookies

To permanently enable them, add the following to `/etc/sysctl.conf`

	net.ipv4.tcp_syncookies = 1


iptables rules
--------------

If you don't have any existing iptables rules set (check with `iptables -L`, you
should set some basic rules. Your mileage may vary, so make sure that the rules
you apply are sane. Misconfiguring your firewall will at best lock you out of
your system. You should only change rules if you know what you're doing!

Amongst other things, rules are set to disallow access to all services running
on the system other than those that are vital for normal operation of the site
and for maintenance (ie web access and SSH).

For a system running only ssh and a webserver running HTTP and HTTPs, the
following rules will block off the use of all other TCP ports on the system.

	iptables -F #delete all rules
	iptables -A INPUT -i eth0 -p tcp –dport 22 -j ACCEPT
	iptables -A INPUT -i eth0 -p tcp –dport 80 -j ACCEPT
	iptables -A INPUT -i eth0 -p tcp –dport 443 -j ACCEPT
	iptables -A INPUT -j DROP

Whenever changing firewall rules, make sure that your system is accessible and
operates normally after the rules have been applied. Some web applications may
malfunction if they cannot access specific ports. The above rules will not break
any local access to ports, so database connections etc will function as normal.

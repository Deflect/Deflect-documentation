---
layout: default
title: Pac file sample
lang: en
ref: Pac_file_sample
tags:
 - Deflect DIY
---

<pre>
function FindProxyForURL(url, host) {
  if (shExpMatch(url, "http://samplesite.com*") ||
    (shExpMatch(url, "http://www.samplesite.com*"))) {
       return "PROXY sampleedge.deflect.ca"
  } else {
    return "DIRECT"
  }
}
</pre>


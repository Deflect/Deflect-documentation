---
layout: default
title: Deflect DIY - Additional Install
lang: en
ref: Additional_Install 
tags:
 - Deflect DIY
---

Deflect DIY
===========

Additional Install
------------------

- Edgemanage
- [Banjax](Banjax)
- [Swabber](Swabber)
- [ENCfs](ENCfs)
- logstash
- awstats

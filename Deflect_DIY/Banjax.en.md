---
layout: default
title: Banjax
lang: en
ref: Banjax
tags:
 - Deflect Tech
 - Deflect Labs
 - BotnetDBP
 - Deflect DIY
---

# Banjax

Banjax is an ATS Banning filter. It was designed as a replacement for
[fail2ban](https://www.fail2ban.org) in processing logs based on regular expressions. Banjax identifies
attackers based on regular expressions and then passes the IP address of the
client matching the rule to [Swabber](Swabber).

It was designed because:

- It is fast: it reads the request on the fly before it is being written in a
  file hence file I/O operation does not slow it down.
- It can prevent a botnet before even serving it once. This was proved to be
  vital in some [previous cases](../News/Vietnam_Case_Study) when the origin is weak.

The Banjax source is available on [Github](https://github.com/equalitie/banjax).
The name is based upon [Dublin slang](http://en.wiktionary.org/wiki/banjax)
terminology for "to ruin".

To download the code and learn how to install and configure, Banjax, go to the
[project on Github](https://github.com/equalitie/banjax).

## Configuration

Banjax.conf is usually located in
/usr/local/trafficserver/conf/banjax/banjax.conf. This configuration is
per-Dnet. 

## Push changes to banjax.conf

	deploy.sh -A network -p ats -H ALL

---
layout: default
title: What is Deflect 
lang: en
ref: DeflectDIY_Intro
tags:
 - Deflect DIY
---


What is Deflect
===============

How Deflect fits together
-------------------------

1. A user enters http://www.somesite.com/page into a web browser or a web
   viewing software (UA) of somekind.
2. The UA then does a name to IP lookup which returns one of many Deflect edges
3. A request is directed to this edge with your request of www.somesite.com/page with

	GET /page HTTP/1.1
	Host: www.somesite.com

4. edge then checks for local copy (cache).
   
If found and fresh, returns to client, end. If no local cache is found, edge requests from origin

	GET /page HTTP/1.1
	Host: www.somesite.com

If found in cache but not fresh, client attempts to revalidate with an IMS request to origin:
    
	GET /page HTTP/1.0
	Host: www.somesite.com
	If-Modified-Since: Sat, 6 Oct 1985 1:24:00 GMT

5. origin webserver gets request, and builds page if dynamic.

If we requested If-Modified-Since, and the page has not been modified since the moment Marty arrived back in 1985, then the server might return "304 Not Modified" - in that case the edge updates time on cached object and serves from cache. Otherwise, whether IMS or not, the server will respond with the appropriate HTTP code and data: probably 200 OK (though it could be any code). If the edge cannot be reached in without origin-timeout, we choose to serve from cache no matter how old.
6. edge returns response to the UA
7. UA optionally caches response locally


Component
---------

1. Operating System
	Linux - Debian 7.x
2. Proxy
	Apache Traffic Server
3. Monitoring
	1. nagios
		nagiosgrapher
        2. logstash
		kibana
4. Log Analysis
	AWStats
5. Domain Name Server
	bind
6. Firewall Tools
	1. iptables
	2. banjax
		swabber
7. File Sync
	rsync
8. File System Encryption
	Fuse
		encFS

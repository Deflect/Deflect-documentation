---
layout: default
title: ENCfs
lang: en
ref: ENCfs
tags:
 - Deflect DIY
---


ENCfs
=====

Deflect uses the [ENCfs](http://www.arg0.net/encfs) FUSE filesystem for
encryption of local file storage. Currently due to the lack of observed
overhead, we encrypt the whole Traffic Server directory on install. Deflect
edges should, wherever possible, store their ATS tree within an ENCfs mount
(unless proper LUKS encryption is available). This allows us to secure logs and
SSL keys in cases of theft of hardware or low level compromises.

Implementation
--------------

### Edge

The edge simply needs to have ENCfs installed (via the `encfs` package)
and the ENCfs directory mounted. Generally the source for the mount is
/usr/local/.trafficserver and the mountpoint is /usr/local/trafficserver. 


### Controller

The controller supports automated creation and mounting of ENCfs filesystems.
The Expect script named `initENCfs.e` in the scripts directory is used to
both create a new, blank ENCfs device if not already present, and also to mount
an existing one. This script is called by deploy.sh when ENCfs is required to be
set up on an edge- ie if the DNET has ENCFS=1. 


Commands
--------

	migrate_to_encfs.sh
	
This script is used to migrate a whole network to ENCfs in bulk. The NETWORK
variable must be defined if you wish to deploy to a new network. 

	mountats

This command simply mounts the ATS ENCfs tree and restarts ATS. This is the
command to use to remount ENCfs when a host comes up after boot.

	initENCfs.e

This is an Expect script that remotely mounts the ENCfs directory. It generally
doesn't need to be called directly but it can be called as follows:
```initENCfs.e $host $ENCFS_PASS /usr/local/.trafficserver/ /usr/local/trafficserver/```.

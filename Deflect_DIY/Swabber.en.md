---
layout: default
title: Swabber
lang: en
ref: Swabber
tags:
 - Deflect Tech
 - Deflect Labs
 - BotnetDBP
 - Deflect DIY
---

# Swabber

Swabber is a daemon for banning and unbanning IP addresses. As simple as that.

## Documentation

The vast majority of documentation and detail is available on
[Github](https://github.com/nosmo/swabber). Swabber can be installed by running
	pip install swabber

## Requirements

Swabber requirements can be installed via pip. The minimum supported version of
python is 2.6. The code uses the "as" keyword and other conventions that are
only present in 2.6 and onwards. 

## Operation

Swabber uses a [pub/sub](https://en.wikipedia.org/wiki/PubSub) based system via
ZMQ, whereby Swabber itself will try to connect to (ie subscribe to) a port
(usually on localhost) where something will publish a ban by starting to listen
(publishing) on the same port. 

Swabber takes a ban as an IP address. If the IP address hasn't been banned, it
will be banned. If it is currently banned, the ban will be extended for the
predefined ban time. This is defined in the configuration file swabber.yaml,
this time defaults to two minutes.

### Manual Execution
You can use the init script to run swabber as a daemon as normal: 

	/etc/init.d/swabberd start

Or if you want to observe more debug info:

	swabberd -v


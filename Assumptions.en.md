---
layout: default
title: What you need to start
lang: en
ref: Assumptions
tags:
 - Get started
---

What you need to start
======================

1. You qualify for protection under the terms of our eligibility criteria.
2. You control the [DNS](https://en.wikipedia.org/wiki/Domain_Name_System) for
   your site(s).
3. You have some knowledge about managing your website.
4. You can provide us with all the [technical
   information](Technical_Information) we need.
5. It is understood that this is a free service provided by
   [eQualit.ie](https://equalit.ie/) without contractual obligations for either
   party. Please read our [manifesto](https://equalit.ie/equalit-ie-manifesto/)
   for further explanations.
6. You understand that the Deflect team will have access to your website's
   traffic data.

### **Next step... [Join Deflect!](Dashboard_Walkthrough/Walkthrough)**

---
layout: default
title: Who should consider Deflect
lang: en
ref: Who_Should_Consider_Deflect
tags:
 - Features
---

Who should consider going behind Deflect and when is the best time to join?
---------------------------------------------------------------------------

You should consider it. As soon as possible!

Too often our security measures are left in the "I'll do it later" pile, and
havoc reigns when the emergency presents itself. Whilst we take on websites
under an ongoing DDoS attack, it is much simpler and more convenient to do so
beforehand. Often the provider will simply switch your website off if it is
attracting an attack and you may not be able to initiate any mitigation measures
after that. So plan in advance.

Also, Deflect is a caching service, meaning that we will deliver your web pages
without having to query your server all the time. This reduces the stress on
your machine and actually makes delivery of content to your readers faster. 

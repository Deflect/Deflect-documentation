---
layout: default
title: eQPress
lang: en
ref: eQPress
tags:
 - Features
 - Support
---


## What is eQPress?

After several years protecting sites against DDoS attacks, we've found that the
weakest link in websites' overall defences are the hosting providers (and
servers) of our clients, which are poorly resourced, unable to handle basic
attacks and easily susceptible to social engineering. Leveraging our team's
experience with infrastructure hardening and secure service provision, we have
therefore decided to close the potential weak spots by setting up and offering
secure hosting along with DDoS mitigation. This is what
[eQPress](https://github.com/equalitie/eqpress) is all about: a secure, stable
and user-friendly hosting infrastructure based on the
[Wordpress](https://wordpress.org) blogging platform and protected by Deflect.

eQPress offers website hosting for single sites or multi-sites with subdomains
(http://sub.example.com) or subdirectories (http://example.com/sub) to anyone
who qualifies for protection under the terms of Deflect's [eligibility
criteria](Eligibility). Users can either create their website from scratch or
migrate their existing Wordpress-based website to eQPress.

To create or migrate a website on eQPress, you need to [sign up with
Deflect](https://dashboard.deflect.ca/signup) first. When the registration is
completed, you can ask Deflect's team to create a new site for you or
[migrate](https://www.equalit.ie/migrating-your-wordpress-site/) your site to
eQPress.

## [eQPress FAQ](eQPressFAQ)

## [Visit eQPress' website](https://equalit.ie/eqpress)

## [Read eQPress documentation](https://www.equalit.ie/eqpress/documentation)


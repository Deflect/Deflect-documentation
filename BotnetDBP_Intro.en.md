---
layout: default
title: BotnetDBP
lang: en
ref: BotnetDBP
tags:
 - Deflect Tech
 - Deflect Labs
 - BotnetDBP
---

# BotnetDBP

Deflect's extensive network of caching servers is built to absorb and mitigate
traffic inflicted by sustained DDoS attacks. There are, however, many scenarios
in which it is more efficient to actively and accurately differentiate between
legitimate and malicious requests in order to further reduce the load on the
network. 

Previously, Deflect had been using
[Fail2ban](http://www.fail2ban.org/wiki/index.php/Main_Page) to perform
detection and prevention duties. As a simple brute force prevention tool,
Fail2ban cannot offer all the complex preventative tasks required by Deflect. We
had planned to extend the tool's capabilities but as the attacks grew in
strength and complexity, the fundamental shortcomings of Fail2Ban became
apparent so we decided to develop our own high-performance, integrated
prevention system called Botnet Detection Banning and Prevention, or BotnetDBP
for short. 

BotnetDBP is comprised of the following core components:

- [Banjax](BotnetDBP_Banjax)
- [Swabber](BotnetDBP_Swabber)
- [Learn2ban](BotnetDBP_Learn2ban)
- [Botbanger](BotnetDBP_Botbanger)


The tools are easily configurable by system administrators and provide extensive
tuning capabilities for specific scenarios. As the project has been built
entirely modularly, it is also possible for the components to be used
independently or integrate into new systems. 

Because our approach is to share information, knowledge and skills as much as
possible, we have documented a number of our attack experiences over time. You
can read about them [here](https://equalit.ie/deflect-labs-reporting) and [here](News).

BotnetDBP is currently deployed in the live Deflect network and has performed
extremely well under heavy DDOS load. 

Code and READMEs for the various tools listed within are available on
[Github](https://github.com/equalitie).

![BotnetDBP scheme](BotnetDBP_scheme.png)

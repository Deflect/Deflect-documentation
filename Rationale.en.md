---
layout: default
title: Rationale
lang: en
ref: Rationale
tags:
 - Features
---

Rationale
=========

Deflect is a robust and innovative website protection service designed
to withstand distributed denial of service (DDoS) attacks. These attacks
— undertaken by infected 'bots' — are intended to disable the targeted
website and prevent access for legitimate users. The attacks also serve
to intimidate the organisation running the site, effectively silencing
online dissent. Most small human rights and independent media
organisations aren't able protect themselves from such attacks - it's
costly, complicated and few system administrators are specialists in
handling this form of cyberattack. Furthermore, commercial DDoS
mitigation services are expensive and may alter their terms of service
if they believe a particular website under their protection is drawing
too many attacks.

Each year, DDoS attacks have become harder to prevent and even tougher
to recover from. On the principle that prevention is better than cure,
Deflect's approach is pro-active: Rather than responding to attacks
after the fact, our project keeps websites under constant protection, in
advance of any security issues. We don't host the sites, we simply cache
them and then deliver the unaltered contents across the eQualit.ie cloud
infrastructure. This is built from a wide network of trusted servers
located around the world and built to absorb a high degree of malicious
bot requests.

We've made Deflect available for free because we believe the important
work our clients do trumps any commercial concerns. In fact when a
website is not under attack, running Deflect reduces the strain on the
client's server and sysadmin resources, ultimately saving them money.

All source code and documentation is freely available, allowing others
to set up their own Deflect network and mitigate DDoS attacks, under a
Creative Commons Licence.


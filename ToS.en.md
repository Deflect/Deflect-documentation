---
layout: default
title: Deflect Terms of Service
lang: en
ref: Terms_of_Service
tags:
 - Features
---


Deflect Terms of Service
========================

1. About us
-----------

eQualit.ie Inc. is a Canadian corporation (eQualit.ie, we, or us) that provides
a distributed denial of service (DDoS) mitigation service called Deflect
(Deflect or, the Service) to individuals, organisations, and businesses engaged
in advocacy efforts that empower civil society and defend human rights and
freedoms (you). We operate the Deflect.ca website and its subdomains. We are
based in Montreal, Canada but our team is located all over the world. Please
refer to the [eQualit.ie Manifesto](https://equalit.ie/equalit-ie-manifesto/) to
learn more about us.

By registering for the Deflect service you agree to be bound by and to comply
with these terms of service and our Privacy Notice (collectively, the Terms).

2. What is Deflect?
-------------------

Deflect is a DDoS mitigation service. Our goal is to create a community-driven
and open-source technical response to DDoS attacks which result in the
censorship of online voices promoting and defending civil liberties and human
rights.

Deflect is comprised of the following services:

-   A distributed reverse proxy caching network (Deflect Network)
-   A control panel to manage your website’s protection (Deflect Dashboard)
-   An optional website hosting service (EQpress)

3. Who Can Use Deflect?
-----------------------

Deflect protection is offered to civil society groups and individuals working
  to defend human rights and create independent media. Our services are also
  available to those promoting democracy, ecology, women’s rights and the
  principles enshrined in the [International Bill of Human
  Rights](https://en.wikipedia.org/wiki/International_Bill_of_Human_Rights).

4. Using Deflect
----------------

Deflect is a free service. There are no fees to use Deflect.

To use the Services you will need to create a user account and provide some
basic information about your website, including the domain name and the server
IP address. Once you have registered you will be able to configure each website
that you want to be “deflected” through the Deflect Dashboard.

5. Your Website Representative
------------------------------

Each organisation that uses the Services must appoint a representative duly
authorised to act on its behalf, who will (i) have access to the Deflect
Dashboard profile configured for their website(s) and who (ii) has access to and
control over the primary email address associated with registration of that
website (the Website Representative).

We will only take instructions from and communicate with the Website
Representative or others who have been authorised by the Website Representative.
Preference will be given to communications made via the Deflect Dashboard, or,
via email, from the primary email address associated with registration of the
website.

Where, for whatever reason, the Website Representative is unable to continue
acting, a new Website Representative must be authenticated by eQualit.ie. You
may contact us using the [Contact Us form](https://equalit.ie/#contact) on the
eQualit.ie website.

6. Your Obligations
-------------------

You grant us a royalty-free, non-exclusive, non-transferable right and licence
to use, copy, store and display the data you provide us solely for the purpose
of enabling us to perform the Services.

### 6.1 Your Responsibilities

In addition to any other obligations contained in these Terms, you shall:

- Represent and warrant that you have the full power and authority to register
  your organisation’s website for Deflect.
- Assign, record and control the issuance of your Website Representative’s
  authority.
- Be responsible for the accuracy, completeness and adequacy of your data.
- Be responsible for ensuring that the content on websites using the Services
  adheres to these Terms and our
  [Manifesto](https://equalit.ie/equalit-ie-manifesto/).
- Be responsible for ensuring that any copyrighted material shall be used with
  the permission of the owner or that you are otherwise permitted to post the
  material.
- Keep your account password and information confidential.
- Treat eQualit.ie staff and its partners with courtesy and respect, remembering
  that this is a free best effort service run by some very dedicated and
  overworked people.

### 6.2_Prohibited_activities
			
You shall not:

- Include, or knowingly allow others to include any Objectionable Content
- Introduce malware or viruses through our Services.

Objectionable Content is content that infringes on the rights of others,
particularly human rights and the right to privacy, and or content which is
discriminatory, threatening, or liable to incite violence, or racial hatred.

If we believe that your content or data contains or includes malware, viruses,
or Objectionable Content then we may remove your data and website from Deflect.
If we receive a complaint about your website’s content we may contact you for
further information and to give you a chance to explain your content. Following
our review we may decide to terminate your account and your use of our Services.
Please report any abusive content to abuse@deflect.ca.

7. eQualit.ie’s	obligations
---------------------------

### 7.1 Our Responsibilities

We shall

- Operate according to our DDoS principles and place the utmost importance on
  data security, your privacy, and respecting your technical and service
  demands.
- Expend best efforts to (i) keep your website online during a DDoS attack and
  (ii) continue providing the Services to you irrespective of attacks against
  your website and or pressure exerted on our organisation.
- Provide our Service in accordance with our Privacy Notice.
- Inform you via email, or, if relevant, over the phone or any other
  communication medium you have provided to us prior to taking advanced
  mitigation measures.
- Endeavour to keep our network and all relevant services current, and install
  the latest software updates and vulnerability patches as and when they become
  available.
- Make our help desk available to support your use of Deflect and aim to respond
  to help tickets within 3 hours between Monday-Friday and within 6 hours on the
  weekend.
- When possible, notify you by email of all interruptions to the Services and
  try to keep such interruptions to a minimum.


8. Copyright Infringement
-------------------------
			 
Notices of legitimate copyright infringement that we receive will be forwarded
to the Website Representative. We may have to suspend the Service to a website
in order to comply with applicable copyright law.


9. Termination
--------------
	
These Terms take effect when you register for Deflect. You are free to stop
using Deflect at any time by pointing your domain’s nameserver records away from
us. If we believe that you are not respecting our Terms we may suspend your use
of Deflect and terminate your account.

In our discretion, but acting reasonably and in good faith, we may cease
providing the Services to you, for example, if our funding sources expire. If
this happens you will be notified at least one month in advance, if possible,
and offered assistance to migrate away from Deflect.

If you violate these Terms we may take escalating steps to rectify the
situation, including suspension of your account and the Service. We will aim to
be fair and proportionate. If we do not take immediate action, we are not
waiving our right to take action later on.


10. Warranties and Limitation of Liability
------------------------------------------

We provide Deflect using a commercially reasonable level of skill and care but
we cannot make any promises about specific functions of the Service, its
reliability, availability or ability to meet your specific needs. We cannot make
any commitments that your website will always be safe from attacks.  We provide
our Services "as is" and exclude all warranties to the extent permitted by law
including implied warranties, conditions of merchantable quality, fitness for a
particular purpose, non-infringement, that the services will meet your needs,
will be available for use at any particular time, or that they will be error
free.

In no event shall we be liable for any direct, consequential, incidental,
exemplary or punitive damages even if advised in advance of the possibility of
such damages. Nor shall we be liable for any lost revenue, lost profit or lost
savings, or the results of your use or misuse of the Services, including any use
contrary to law.


11. Your Indemnity
------------------

You, and where applicable, your organisation will hold harmless and indemnify us
and our affiliates, officers, agents, and employees from any claim, suit or
action arising from or related to the use of the Services or violation of these
Terms, including any liability or expense arising from claims, losses, damages,
suits, judgments, litigation costs and attorney’s fees.


12. General
-----------

We may modify these Terms from time to time and will notify you when this
happens. If you don't agree with our modified Terms then you should stop using
Deflect.

The laws of Canada will apply to any disputes arising out of or relating to
these Terms and will be litigated in the Province of Quebec, Canada.

If any part of these Terms is not enforceable, then the rest of the Terms will
remain in force as though the unenforceable provision had never existed. 

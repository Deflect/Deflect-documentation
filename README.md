# Deflect Documentation

This is the public repository for the documentation of the Deflect project - a
Distributed Denial of Service (DDoS) mitigation service created to neutralize
cyberattacks against independent media and human rights defenders.

Our goal is to create a community-driven, technical response to the censorship
of online voices caused by DDoS attacks.

* Go to the [home page](https://0xacab.org/Deflect/Deflect-documentation/blob/master/index.md) for an index of the most important resources.
* Read the [instructions to contribute](contribute) to this documentation.
* Read the [instructions to translate](translate) this documentation.
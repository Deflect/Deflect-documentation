---
layout: default
title: About Deflect
lang: es
ref: About_Deflect
tags:
 - Features
---

Deflect es un servicio de mitigación
[DDoS](https://es.wikipedia.org/wiki/Ataque_de_denegaci%C3%B3n_de_servicio)
para ONGs, grupos de la sociedad civil, blogueros activistas y grupos mediáticos
independientes. Creado por la asociación sin ánimo de lucro de seguridad digital
[eQualit.ie](https://equalit.ie), el servicio es una solución gratuita de código abierto destinada a
mitigar [los ataques de denegación del servicio (DDoS).

- [1. Sobre Deflect](#Sobre_Deflect)
	- [1.1 Antecedentes](#Antecedentes)
	- [1.2 Nuestro método](#Nuestro_metodo)
	- [1.3 Diseño](#Diseno)
	- [1.4 Protección ofrecida](#Proteccion_ofrecida)
	- [1.5 Deflect en acción](#Deflect_en_accion)
	- [1.6 Detalles y limitaciones](#Detalles_y_limitaciones)
	    - [1.6.1 Componentes almacenados](#Componentes_almacenados)
	    - [1.6.2 Cookies](#Cookies)
	    - [1.6.3 ¿Funciona?](#Funciona)
	    - [1.6.4 Capa de conexión segura (SSL)](#Capa_de_conexion_segura_SSL)
	    - [1.6.5 Sistema de nombres de dominio (DNS)](#Sistema_de_nombres_de_dominio_DNS)
	    - [1.6.6 Personalizaciones de Deflect](#Personalizaciones_de_Deflect)

<a name="Antecedentes"></a>
Antecedentes
------------

La mayoría de los grupos defensores de los derechos humanos y de los grupos
mediáticos independientes no disponen de los recursos económicos o técnicos para
mitigar los ataques de denegación de servicio (DDoS), por ello se creó Deflect
para proporcionar este servicio de forma gratuita. Estos ataques — iniciados
mediante bots infectados ['bots'](https://es.wikipedia.org/wiki/Botnet) — pueden
inhabilitar la página web meta e impedir el acceso a los usuarios legítimos,
intimidar a los dueños de la página y vulnerar la libertad de expresión en
internet. Los servicios comerciales de mitigación de los ataques de denegación
de servicio (DDoS) son caros y puede que cambien los términos del servicio si
consideran que una determinada página web tiene muchas visitas. Muchos de
nuestros clientes acuden a nosotros después de haber tenido una mala experiencia
con algún conocido servicio comercial.

<a name="Nuestro_metodo"></a>
Nuestro método
--------------

Instalamos cachés [proxys
inversos](https://es.wikipedia.org/wiki/Proxy#Proxy_inverso_.28Reverse_Proxy.29)
repartidos en una colección de proveedores de hosting de bajo coste distribuidos
geográficamente. Cada uno de los anfitriones (host) tiene una funcionalidad
equivalente aunque estamos averiguando cuales son los proveedores de mayor
calidad. Usando el tiempo de vida del sistema de nombres de dominio (DNS),
cachés distribuidos, poniendo en la lista negra algunas IP, y otras buenas
prácticas identificadas, los servicios Deflect multiplican clientes, a la vez, a
bajo coste para nosotros y de forma gratuita para ellos.

<a name="Diseno"></a>
Diseño
------

Deflect está diseñando como un sistema fácilmente reproducible, robusto, de bajo
coste, y no propietario que proporciona protección a múltiples páginas web, a
las que llamamos "origins". El sistema fue creado para mantenerse neutral ante
diferentes servidores web, con algunas limitaciones que se explican a
continuación. Se ha construido usando Debian 6 VPSs, a los que llamamos "edges",
y un servidor controlador al que llamamos "controller". El componente de caché
se encuentra bajo el [servidor de tráfico
Apache](http://trafficserver.apache.org/).

<a name="Proteccion_ofrecida"></a>
Protección ofrecida
-------------------


- Absorbe el 99% del tráfico destinado a tu página web Comprueba algunas estadísticas de tráfico [aquí](https://www.deflect.ca/stats).
- Oculta la ubicación de tu servidor (dirección IP)
- Impide el acceso público a los paneles de redacción (p. ej. /admin, /login, etc.)
- Filtra las peticiones malignas a través de fail2ban, learn2ban, y conjuntos de reglas iptables

<a name="Deflect_en_accion"></a>
Deflect en acción
-----------------

Para acceder a la página protegida de Deflect:

1. Introduce la dirección de la página web en el buscador.
2. El sistema de nombres de dominio (DNS) recuperará un alias señalando a nuestro conjunto de servidores de almacenamiento. Uno de estos servidores es seleccionado mediante la técnica [Round Robin DNS](https://en.wikipedia.org/wiki/Round-robin_DNS).
3. Si se permite el acceso a la página solicitada y el servidor de almacenamiento tiene el contenido de la página en su caché, responderá inmediatamente al buscador. Si el contenido no está guardado en el servidor de almacenamiento, se solicitará a la "origin" y se mandará al buscador.
4. Si la dirección no está autorizada, se muestra una página de	notificación.

La imagen que se muestra abajo ofrece una sencilla explicación:

[![INFOGRAPHIC](/img/INFOGRAPHIC4_SPANISH.jpg)](/img/INFOGRAPHIC4_SPANISH.jpg)

<a name="Detalles_y_limitaciones"></a>
Detalles y limitaciones
-----------------------

<a name="Componentes_almacenados"></a>
### Componentes almacenados

Deflect se encarga de páginas web compuestas por muchos elementos, incluyendo
hojas de estilo en cascada (CSS), Javascript, archivos multimedia y grandes
ficheros binarios. Los componentes de las páginas alojados en diferentes
dominios (“widgets", rastreadores de tráfico etc.), se gestionan de manera
regular. Actualmente, Deflect almacena respuestas de 10m, que se pueden ajustar
para localizaciones individuales (largas para archivos binarios que rara vez
cambian, más cortas para foros online por ejemplo). 

<a name="Cookies"></a>
### Cookies

Aunque Deflect actualmente ignora las cookies, devolviendo el mismo objeto del
caché independientemente de la presencia de cookies en la petición del cliente,
se puede configurar sobre la bases de dominio y de ruta. Podemos habilitar un
tratamiento único para las diferentes cookies para una página o para parte de
una página, pero de esta forma inhabilitamos eficazmente nuestra capacidad de
almacenar esa página o parte de esa página. No obstante, la página seguirá
estando protegida por nuestro análisis firewall. Las cadenas de búsqueda se
tratan como parte de la URL- diferentes cadenas de búsqueda serán siempre
consideradas como objetos únicos y almacenadas como tal. Las respuestas a las
peticiones POST nunca se almacenan. 

<a name="Funciona"></a>
### ¿Funciona?

Puedes saber si Deflect está prestando sus servicios a una página mirando a los
encabezados HTTP (usando “Inspeccionar elemento” en Chrome o la extensión
[Firebug](http://getfirebug.com/) en Firefox). Verás una Vía: cadena que devuelve un servidor de
almacenamiento individual que presta servicio a la página web solicitada. Será
parecido a esto:

Via:http/1.1 prometeus1.deflect.ca (ApacheTrafficServer/3.2.4
[uIcMsSfWpNeN:tcCMi p sS])

La respuesta de almacenamiento, en el caso de arriba [uIcMsSfWpNeN:tcCMi p sS],
se puede interpretar aquí. 

<a name="Capa_de_conexion_segura_SSL"></a>
### Capa de conexión segura (SSL/TLS)

Deflect también soporta SSL. Para más información, vea el
[soporte](TLS_Support "TLS Support") de capa de
conexión segura (SSL). 

<a name="Sistema_de_nombres_de_dominio_DNS"></a>
### Sistema de nombres de dominio (DNS)

DNS está configurado para tener un tiempo de vida corto para permitir la rápida
adición o la eliminación de nódulos al conjunto de servidores de almacenamiento.
Cualquier otra pregunta, por favor visita nuestra sección de preguntas más
frecuentes o mándanos un correo y haremos todo lo posible para contestarla. 

<a name="Personalizaciones_de_Deflect"></a>
### Personalizaciones de Deflect

Con el tiempo, iremos desarrollando perfiles para diferentes servidores web.
Entretanto, podemos proporcionarle personalización para:

1. Dominios y sus alias (www.supágina.org, supágina.org)
2. Caché “tiempo de vida” (TTL)
3. Localizaciones protegidas (/admin)

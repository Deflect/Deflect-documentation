---
title: Deflect Overview in Farsi/fa
permalink: wiki/Deflect_Overview_in_Farsi/fa/
layout: wiki
---

<div class="ltr">
"دیفلکت" (Deflect) نام سرویسی برای کاهش "حملات توزیع شده منجر به عدم
دسترسی" (Distributed Denial of Service - DDoS) می‌باشد که از سوی
eQualit.ie ارائه می‌گردد. ما گروهی از متخصصان رشته فن‌آوری هستیم که با
رسانه‌های مستقل، فعالان اجتماعی وبلاگ‌نویس و همچنین مدافعان حقوق بشر در
کشورهایی که دارای محدودیت‌های شدید اینترنتی هستند همکاری می‌کنیم. ما به
این گروه‌ها و وبسایت آنها کمک می‌کنیم تا در برابر حملات سایبری مقاومت
کنند. ما یک نهاد غیرانتفاعی هستیم و خدمات ما برای سازمان‌های واجد شرایط،
رایگان است. بودجه ما از سوی برخی موسسات بین‌المللی تامین می‌شود که هویت
آنها نزد ما محفوظ مانده و برای هیچ‌کس آشکار نخواهد شد.

شیوه عمل ما رویکردی فعالانه است: هنگام بروز مشکلات امنیتی، به جای پاسخ
دادن به حملات، وبسایت را تحت پوشش حفاظت دائمی قرار می‌دهیم. ما از وبسایت
شما میزبانی نمی‌کنیم، بلکه آن را ذخیره کرده و سپس محتوای دست نخورده آن
را از طریق زیرساخت‌های شبکه خود ارائه می‌کنیم. در واقع، بازدیدکنندگان و
کاربران در هیچ شرایطی به سرور شما مراجعه نخواهند کرد و اگر وبسایت شما
مورد حمله DDoS قرار بگیرد، ما جلوی درخواست‌های مخرب را می‌گیریم، تا سرور
شما همچنان به کار طبیعی خود ادامه ‌دهد. شبکه ما بر روی یک مجموعه توزیع
شده از سرورهای قابل اعتماد بنا شده که بطور اختصاصی، برای جذب ترافیک زیاد
در صورت بروز حملات، طراحی شده است.

روش کار در این شکل نمایش داده شده:

برای شروع حفاظت از وبسایت شما، به این جزئیات نیاز داریم:

-   فهرستی از همه دامنه‌هایی (Domain) که باید توسط سرویس Deflect مراقبت
    شود، بهمراه کپی Zone File هر دامنه.
-   آمار ترافیک متوسط ماهیانه، ورودی به وبسایت شما.
-   شرح فنی سروری که وبسایت شما بر روی آن قرار گرفته.
-   شرح کاملی از خصوصیات تعاملی و پویا که بر روی وبسایت خود دارید
    (فایل‌های فلش، انجمن‌ها و فورم‌ها، بنرها و تصاویر، آگهی‌های
    تبلیغاتی، ویجت‌ها، اپلیکیشن‌ها و غیره).

سپس برای پیوستن به سیستم حفاظتی Deflect، تنها چهار مرحله ساده باقی
می‌ماند:

۱- به ما اطلاع دهید! می‌توانید از طریق ایمیل signup@deflect.ca با ما
مکاتبه کنید.

۲- ما یک مخزن آزمایشی برای وبسایت شما راه‌اندازی می‌کنیم.

۳- شما کارکرد مخزن آزمایشی را بررسی می‌کنید تا از عملکرد صحیح وبسایت خود
اطمینان حاصل کنید.

۴- DNS خود را به سوی سیستم Deflect هدایت می‌کنید.

-   هرگاه بخواهید، می‌توانید خدمات Deflect را با برگرداندن رکورد DNS به
    سوی سرور خودتان، لغو کنید.
-   آدرس وبسایت شما در شبکه اینترنت تغییر نمی‌کند. دبیران وبسایت شما از
    طریق یک آدرس مخفی که با جستجوی عادی یافت نمی‌شود، وارد سیستم
    می‌شوند. توصیه می‌کنیم که به محض پیوستن به Deflect، آدرس IP خود را
    عوض کنید.
-   Deflect دارای پشتیبانی SSL برای بازدیدکنندگان وبسایت شماست. اگر
    دارای گواهینامه SSL هستید و مایلید آن را در Deflect فعال کنید، به ما
    بگویید تا آن را بر روی سیستم پیکربندی کنیم.
-   اگر برای پیوستن به شبکه ما، نیاز به انجام تغییراتی بر روی وبسایت شما
    باشد، به اطلاع شما خواهیم رساند. با این کار از بسته بودن همه
    روزنه‌های معمول برای نفوذ حمله کنندگان، اطمینان حاصل می‌کنیم و نیز
    قابلیت هک شدن و مورد حمله DDoS قرار گرفتن سایت شما را کاهش می‌دهیم.
-   حتی اگر وبسایت شما مورد حمله قرار نگرفته باشد، باز هم استفاده از
    خدمات Deflect می‌تواند موجب کاهش بار سرور و منابع مدیریت سیستم در
    وبسایت شما ‌شود.
-   تمام کدهای منابع (Sources) و دستورالعمل‌ها، تحت یک Creative Commons
    Licence، بطور آزادانه در دسترس هستند، بنابراین شما می‌توانید، درصورت
    تمایل، شبکه Deflect شخصی خود را راه‌اندازی کنید.

برای مطالعه بیشتر در زمینه Deflect، به هر دو زبان فارسی و انگلیسی، به
وبگاه قابل ویرایش ما به آدرس <http://deflect.ca> بروید. اگر نیاز به
اطلاعات بیشتری داشتید، به ما به آدرس info@deflect.ca ایمیل بزنید.

</div>


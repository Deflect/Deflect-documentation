---
layout: default
title: Procedimiento
lang: es
ref: Procedure
tags:
 - Get started
---


Procedimiento
=============

Estos son los pocos pasos sencillos a seguir para unirse a Deflect:

1. Regístrese en
   [https://dashboard.deflect.ca/signup](https://dashboard.deflect.ca/signup)
2. Le enviaremos un correo electrónico con datos de acceso temporal
3. Inicie sesión y siga las instrucciones para configurar el DNS, los ajustes de
   seguridad, y los certificados SSL de su sitio si son necesarios. Más
   información en [Registrarse](Dashboard_Walkthrough/Step0_Register)
4. Recibimos su solicitud, la aprobamos de acuerdo a nuestras directrices, y
   establecemos su sitio web en Deflect
5. Recibe una notificación para cambiar sus servidores de nombres (NS) hacia
   Deflect
6. Apunta sus NS hacia Deflect

¡Y ya está!

* * *

Los asociados potenciales al proyecto deben considerar los siguientes detalles
técnicos:

- La protección de Deflect está pensada para ser pro-activa y estar implementada
  con anterioridad a un ataque DDoS. Puede cambiar al uso de Deflect durante un
  ataque DDoS, pero nos llevará más tiempo establecerle. Una vez haya cambiado a
  Deflect, su sitio web será almacenado como caché por toda nuestra
  infraestructura, y el tráfico de Internet será dirigido hacia esta.
- Tendrá que apuntar los registros de servidores de nombres (NS) de su DNS a un
  servidor de Deflect. En cualquier momento puede revertir el uso de Deflect
  cambiando los registros del DNS para que vuelvan a apuntar a los servidores de
  nombres originales.
- La dirección de su sitio web permanecerá inalterada hacia Internet. Los
  miembros de su equipo editorial tendrán que autentificarse a si mismos en el
  área editorial de su sitio con una contraseña que usted defina en el Panel de
  Instrumentos de Deflect (Dashboard).
- Se recomienda que cambie la dirección IP de su servidor una vez esté detrás de
  Deflect.
- Deflect tiene [Soporte SSL](TLS_Support) para su sitio web. Tendrá que
  compartir sus certificados con Deflect, mediante el Panel de Instrumentos.
-  Puede que tenga que implementar ciertas actualizaciones en su plataforma
   existente del sitio web antes de que sea aceptada por el proyecto Deflect.
   Esto asegurará que se cierran los resquicios comunes para ataques, haciéndola
   menos vulnerable a hacking y ataques DDoS.
- A causa de que Deflect es un servicio gratuito, no garantizamos soporte 24/7.
  Sin embargo, con miembros radicados en 3 continentes y trabajando de acuerdo a
  una monitorización planificada, normalmente hay alguien disponible para
  abordar cualquier problema que pudiera surgir.

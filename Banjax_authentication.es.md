---
layout: default
title: Autentificación Banjax
lang: es
ref: Banjax_authentication
tags:
 - Deflect Tech
 - Security
---

Autentificación Banjax
======================

Cuando su sitio web está detrás de Deflect, las peticiones de páginas
adicionales provendrán de nuestros servidores de almacenamiento en caché. Esto
significa que pueden estar varios minutos anticuadas y pueden no tener las
actualizaciones más recientes. Esto no es lo ideal cuando está editando el sitio
web y necesita ver las actualizaciones inmediatamente. Deflect proporciona una
forma especial de que se autentifique a usted mismo en el sistema y acceda a su
sitio web **sin usar la caché**. Lo llamamos autentificación Banjax. Después
de que haya creado la contraseña, en el [panel de instrumentos de
Deflect](Dashboard_Walkthrough/Step2_Admin_Credentials), la página de inicio de sesión de
administración a su sitio web (ej /wp-admin, /login, /administrator, etc.) se
mostrará de este modo:

![Autentificación Banjax](img/Banjax_authentication.png)

Sólo aquellos en posesión de la contraseña de autentificación podrán continuar.
Esto tiene un efecto secundario extra de proteger la sección editorial de su
sitio web de ataques de fuerza bruta contra la contraseña.

---
layout: default
title: Cookies
lang: en
ref: Cookies
tags:
 - Deflect Tech
---


Cookies in Deflect
------------------

Deflect widely supports cookies, although they are sometimes controversial: for
example, cookies are subject to legal regulation in certain countries. In the
interests of privacy, they may be disabled by users or system administrators. 

The Cookie request header and Set-Cookie response header are end-to-end
HTTP headers, and therefore normally propagated intact by HTTP proxies.
However, the propagation of Set-Cookie headers as-is may be insufficient
in a reverse proxy. The Set-Cookie header may contain Path and Domain
information that are valid for the origin server, but are incorrect or
meaningless when a reverse proxy has remapped the URL space.

Cookies may also affect cacheability, and hence performance.

The issue of rewriting URLs in a reverse proxy is discussed at
[http://www.apachetutor.org/admin/reverseproxies](http://www.apachetutor.org/admin/reverseproxies).
Cookie path and domain information are one element of that rewriting. It is
implemented in Apache HTTPD's mod\_proxy through the ProxyPassReverseCookiePath
and ProxyPassReverseCookieDomain directives.

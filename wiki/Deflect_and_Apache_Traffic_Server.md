---
title: Deflect and Apache Traffic Server
permalink: wiki/Deflect_and_Apache_Traffic_Server/
layout: wiki
tags:
 - Deflect
 - Public
---

Design
------

### Apache Traffic Server (ATS)

For manageability, trafficserver config (like all our configs) is edited
centrally and deployed to edge nodes via a script. We use several
deviation from stock config, among which are

-   reverse proxy mode
-   pristine host headers

For more information on the meaning of these terms, consult the
trafficserver documentation. For more accurate and up-to-date
information on all our deviations from stock configuration, simply diff
a given file against the original (which can be found in
/usr/local/trafficserver/config/<file>.config

Update an Apache Traffic Server plugin
--------------------------------------

1.  build the plugin using plugin build method ; e.g. axps, gcc, cc
2.  copy into /usr/local/deflect/trafficserver/modules

`deploy.sh -p ats -H edgeN`

Build new Apache Traffic Server (ATS) version
---------------------------------------------

##### Version 3.2.0

*Prerequisites: Debian-Squeeze, Deflect, user trafserv:trafserv already
created (UID:99, GID:99)*<br\> Download source to /usr/local/src/

`# cd /usr/local/src`  
`... find your mirror at `[`http://www.apache.org/dyn/closer.cgi/trafficserver/trafficserver-3.2.0.tar.bz2`](http://www.apache.org/dyn/closer.cgi/trafficserver/trafficserver-3.2.0.tar.bz2)  
`# wget `[`http://download.nextag.com/apache/trafficserver/trafficserver-3.2.0.tar.bz2`](http://download.nextag.com/apache/trafficserver/trafficserver-3.2.0.tar.bz2)<br\>`''Note: Debian does not come with bzip2 by default :/ ''`  
`# apt-get install bzip2`  
`# tar jxvf trafficserver-3.2.0.tar.bz2`<br\>`....`<br\>`trafficserver-3.2.0/`  
`# cd trafficserver-3.2.0/`<br\>*`Note:` `Box` `probably` `does` `not`
`have` `development` `tools` `installed.` `Below` `install` `should`
`get` `you` `by` `on` `a` `deflect` `box.`*  
`# apt-get install build-essential`  
`# ./configure --with-user=trafserv --with-group=trafserv --enable-layout=Apache --prefix=/usr/local/trafficserver`  
*`Note:` `I` `add` `the` `--prefix=` `just` `in` `case` `the` `default`
`changes.` `Also` `note` `the` `uppercase` `Apache.` `It` `matters.`
<br\>`Changed` `the` `default` `user` `and` `group` `for` `Apache`
`layout` `since` `we` `already` `have` `them` `created` `for` `the`
`old` `3.1.3` `install.`*  
`# make`<br\>`... wait approximately 10 minutes`  
`# service trafficserver stop ; mv /usr/local/trafficserver /usr/local/deflect/archive/trafficserver-3.1.3 ; make install`

##### Config File Changes

Not going to do a step-by-step here. Explain differences below. Config
changes a pretty manual process  
- diff the stock config from previous trafficserver version

`diff -uwr /usr/local/deflect/archive/trafficserver-previous/conf /usr/local/trafficserver/conf`

If there are no differences between config files, then we can carry on
using the same ones. If there are differences, you'll need to be careful
to see whether we need new options, and you'll need to do the following:
list all our variation from stock config for the previous version, then
merge into the new config

`diff -uwr /usr/local/deflect/archive/trafficserver-previous/conf/ /usr/local/deflect/tsconf`

Put the new config files into place Apply all the differences from stock
(as listed from the previous version) to the new config files Nominate a
test edge, then

`deploy.sh -p ats -H testedgeN`

Test with edge\_check.sh if all is well, then

`deploy.sh -p ats -H ALL`

###### What has Changed 3.2.x

-   Upgrade conf to 3.2.x. most lifted from [
    <https://cwiki.apache.org/confluence/display/TS/Upgrading+to+3.2>
    ATS Upgrade to 3.2]
    -   Removed the use of records.config to define SSL certificates.
        Certificate file names must now be in ssl\_multicert.config.
        -   The proxy.config.ssl.server.cert.filename and
            proxy.config.ssl.server.private\_key.filename configuration
            parameters have been removed. The ssl\_multicert.config has
            examples of a typical configuration,eg:

<!-- -->

    dest_ip=192.168.9.9 ssl_cert_name=cert.pem ssl_key_name=key.pem

:\*Removed proxy.config.http.quick\_filter.mask from records.config.
This functionality has been moved to ip\_allow.config. See the example
that comes with your source.

:\*Changed the way ports are configured for HTTP. The following
configuration values are now deprecated

    proxy.config.http.server_port
    proxy.config.http.server_port_attr
    proxy.config.http.server_other_ports
    proxy.config.http.ssl_ports

  
  
All of these are replaced by a single new configuration value. The value
is a STRING consisting of a sequence of port specificiations separated
by spaces or commas. Each specification configures a listening port for
ATS. Each specification is a set of keywords separated by colons. Some
keywords can also have values which may be separated from the keyword by
an optional '=' character. The case of keywords is ignored. The keyword
order is irrelevant unless keywords conflict (eg tr-full and ssl) in
which case the right most keyword dominates..

<!-- -->

    proxy.config.http.server_ports

Eg: *Note: ipv4 is default so the 2 examples below are the same*

    80 80:ipv6 443:ssl
    80:ipv4 80:ipv6 443:ipv4:ssl

|String|Description|
|------|-----------|
|*number*|IP port, required.|
|ipv6|Use IPv6|
|ipv4|Use IPv4. Default|
|tr-in|Use inbound transparency (to client)|
|tr-out|Use outbound transparency (to server)|
|tr-full|Full transparency, both inbound and outbound|
|ssl|Use SSL termination. *Note: For SSL you must still configure the certificates, this option handles only the port configuration*|
|blind|Use as a blind tunnel (for CONNECT)|
|ip-in|Use the keyword value as the local inbound (listening) address. This will also set the address family if not explicitly specified. If the IP address family is specified by ipv4 or ipv6 it must agree with this address|
|ip-out|Use the value as the local address when connecting to a server. This may be specified twice, once for IPv4 and once for IPv6. The actual address used will be determined by the family of the origin server address|

#### See also

-   [Logging](/wiki/Logging "wikilink")


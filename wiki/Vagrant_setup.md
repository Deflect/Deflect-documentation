---
title: Vagrant setup
permalink: wiki/Vagrant_setup/
layout: wiki
---

1.  Install virtualbox
2.  Install vagrant
3.  create and change to a directory, download and install a Vagrant
    base Debian 6 VM with networking
4.  vagrant box add vagrant-image /path/to/vagrant-image.box
5.  vagrant up
6.  vagrant ssh controller; verify resolv.conf

The following [Vagrant](http://www.vagrantup.com/) config file will
allow quickly setting up a virtual controller and edge, given you've
installed a Debian 6 x64 Vagrant box as debian6x64.

    Vagrant::Config.run do |config|
      config.vm.define :controller do |controller_config|
        controller_config.vm.box = "debian6x64-deflect"
        controller_config.vm.host_name = "controller"
        controller_config.vm.network :hostonly, "192.168.33.10"
      end

      config.vm.define :edge do |edge_config|
        edge_config.vm.box = "debian6x64-deflect"
        edge_config.vm.host_name = "edge"
        edge_config.vm.network :hostonly, "192.168.33.11"
      end
    end

use this for /etc/hosts, which sets up DNS for vcontroller.deflect.ca
and vedge.deflect.ca:

    domain deflect.ca
    search deflect.ca
    nameserver 8.8.8.8

Note you'll probably need to re-edit this file after VM reboots,
including during Deflect installs.

We used this page to create our own Vagrant box:
<http://henrysmith.org/blog/debian-squeeze-vagrant-base-box/>


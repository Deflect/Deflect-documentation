---
title: Deflect clients README
permalink: wiki/Deflect_clients_README/
layout: wiki
tags:
 -  Deflect
 - Public
---

Deflect cooperates with organisations that promote international human
rights and freedom of expression. [Summary::The Deflect project aims to
mitigate the effectiveness of targeted DDoS attacks and ensure stability
and online presence for its partners'
websites.](/wiki/Summary::The Deflect project aims to mitigate the effectiveness of targeted DDoS attacks and ensure stability and online presence for its partners' websites. "wikilink")
In essence, Deflect will protect vulnerable websites by distributing a
DDoS attack away from its intended target across multiple servers on the
Internet that will absorb the shock. Deflect will bundle several other
features into its website protection scheme that will identify friendly
visitors and give them access to the site during an attack.

Potential partners in the project need to consider the following
technical details:

-   Deflect protection is meant to be pro-active and implemented prior
    to a DDoS attack. Once you switch to Deflect, your website will be
    cached across our infrastructure and Internet traffic will be
    directed towards it.
-   You will need to either point your DNS name servers to a host
    prescribed by Deflect (very easy) or allow us to become your DNS
    registrar (not valid for all country specific domains)
-   Your website address will remain unchanged to the Internet. Your
    editorial staff will login via a secret address, unknown to the
    Internet. It is recommended that you change your server's IP address
    once on Deflect.
-   You may need to implement certain updates on your existing website
    platform prior to being accepted by the Deflect project. This will
    ensure that common attack loopholes are closed, making it less
    vulnerable to malicious hacking.
-   At any time you can opt out of Deflect by changing the DNS records
    to point back to your server.

If you want to become a partner of eQualit.ie and use Deflect, contact
us after having answered yes to all of the following criteria:

-   You do independent media work and/or defend human rights
-   Your website has in the past been targeted by DDoS attacks OR you
    are expecting such an attack because of the work you are undertaking
-   You are prepared to work with the Deflect team and understand that
    this service is being offered free of charge and cannot guarantee
    round-the-clock monitoring and response mechanisms
-   You control DNS records for your website/s

Once selected, your websites will be assessed based on the
particularities of your online presence. eQualit.ie will analyse your
online services and may recommend certain updates to your website prior
to its integration into the Deflect system. You will be accompanied by
eQualit.ie team members throughout. The accompaniment includes initial
set-up meetings with your website's editors and technical personnel (if
any), as well as ongoing informal trainings to ensure a transfer of
technical knowledge to your organization's technical staff. An
evaluation meeting will be held at the end of the project. To speed up
this process please provide answers to the following questions:

-   A list of all domains you wish hosted on Deflect
-   Average monthly traffic to your website/s
-   Technical description of the server your website/s is located on
-   Detailed description of your website's content management system
    (including versions, plug-ins, etc)
-   Detailed description of all dynamic, interactive features on your
    website (flash, forums, banners, advertising, widgets, APIs, etc.)

**Please send your application to signup @ deflect . ca**


---
layout: default
title: Bundler
lang: en
ref: Bundler
tags:
 - Projects
---

Bundler
=======

Introduction 
------------

Distributed Deflect aims to allow volunteers to become part of the network and
help protect online content. However, DDeflect must have a way to prevent
volunteers from being able to monitor which content users are accessing, or to
modify the content they are caching. Bundler is a core internal component of
Distributed Deflect that aims to address these issues.

[![Bundler](../img/GitHub-Mark-120px-plus.png)](https://github.com/equalitie/bundler)
Bundler's task is to retrieve websites and produce an encrypted, authenticated
bundle of the website content that can then be sent to the volunteer edges for
caching. This way, volunteer edges can cache and deliver content without being
able to break the content's confidentiality or integrity. When a user connects
to a protected website, a trusted edge delivers an authentication key and a link
to the bundle, which the user then downloads from the volunteer edge and
authenticates with the key provided by the trusted edge. The bundle then
automatically debundles and displays the content inside the user's browser.

First, Bundler fetches a webpage and then converts all of its resources into
Data URIs which are then integrated into the main page. This is done so that an
entire webpage, including its resources, can be represented as a single file.
Then, Bundler encrypts the page using AES and produces an authentication key
using HMAC-SHA512. The encryption and authentication keys are kept by the
trusted edges, but the encrypted bundle is then sent to the volunteer edge for
storage.

When a user requests a webpage that is protected by Distributed Deflect, they
first connect to a trusted edge which sends them the encryption keys, the code
for decrypting and displaying bundles, and instructs them to fetch a bundle from
a volunteer edge. Once the bundle is fetched from the volunteer edge, it can be
decrypted by the user.

While Bundler does provide integrity and confidentiality, its confidentiality
properties currently do not defend against side-channel issues such as
identifying encrypted content by its file size. This is fine because Bundler's
main priority is maintaining the integrity of bundled resources.

Bundler's behaviour as a library is explained in depth on its [Github
page](https://github.com/equalitie/bundler). Many aspects of the request process
can be customised to account for edge cases where pages have elements loading in
unusual manners. 


### Workflow 

![Bundler scheme](../img/Bundler.png)


### Video Demonstration 

<iframe src="https://player.vimeo.com/video/102259940" width="640" height="195"
frameborder="0" webkitallowfullscreen mozallowfullscreen
allowfullscreen></iframe>
<p><a href="https://vimeo.com/102259940">Bundler Demo</a> on <a
href="https://vimeo.com">Vimeo</a>.</p>


### Bundler Specification 

The Deflect Bundler is a server-side application programmed in the Node
JavaScript framework. Its purpose is to:

1. Retrieve content from a website that a user requests,
2. Package it as a compressed, encrypted, authenticated "bundle",
3. Deliver the bundle for caching by the volunteer edge,
4. Deliver the decryption and authentications keys to the trusted edge.

A client-side complement to Bundler, called "Debundler" (see below), is then
responsible with dealing with the bundle's contents, namely by decrypting it,
verifying it and preparing it to be displayed to the user in their browser.

When connecting to a protected website, the trusted edge then sends the user the
debundling code, the encryption keys, and a link to download the bundle from the
volunteer edge.

Bundler is designed to run on Trusted Edges only.


#### Cryptographic primitives 

- AES-CBC-256: Used as the standard encryption block cipher.
- HMAC-SHA256: Used to generate authentication codes for bundles.

#### Step 1. Receiving a request for data to be bundled 

A client requests a website and ends up contacting a Trusted Edge. If the
requested url is, for example: http://website.com/page.html, Bundler is expected
to create a bundle that contains the content specific to that page.

Contact between the client and the trusted edge should always occur only via
SSL.

#### Step 2. Creating a bundle 

1. Bundler downloads the requested website data in its entirety, including
   dependencies such as images.
2. Bundler compresses this data into a single resource by converting every
   resource into Data URIs and embedding them all into the main DOM.
3. Bundler generates two random 256-bit values: an encryption key and an
   authentication key.
4. Bundler encrypts the bundle with AES-256-CBC using the encryption key. 
5. Bundler generates an HMAC-SHA256 of the encrypted bundle using the
   authentication key.
6. The resulting HMAC is used as the bundle's filename when it is stored on the
   volunteer edge.

#### Step 3. Delivering a bundle 

Once the bundle has been created, the Trusted Edge must serve the code for
Debundler and the encryption/authentication keys to the client in an HTML file
that links to the volunteer edge. The debundling code will also include
functions to detect invalid bundles, notify the Trusted Edge and receive a new
Volunteer Edge address, as well as a final Trusted Edge callback to provide
metrics.

#### Possible issues 

Generated bundles may be susceptible to situations where two bundles containing
the same content will always have the same ciphertext, same file size and same
filename (HMAC). This could allow the fingerprinting of bundle requests.


### Debundler Specification 

The Deflect Debundler is a client-side application programmed in JavaScript. Its
purpose is:

1. Authenticating and decrypting a bundle,
2. Displaying the decrypted content to the user.

A server-side complement to Debundler, called "**Bundler**", is responsible for
creating and encrypting content bundles, and sending them to the Debundler for
client-side processing.

#### Cryptographic primitives 

- AES-CBC-256: Used as the standard encryption block cipher.
- HMAC-SHA256: Used to generate authentication codes for bundles.

#### Step 1. Decrypting a bundle 

1. Debundler downloads the encrypted bundle from the URL provided by the trusted
   edge.
2. Debundler uses the authentication key provided by bundler to authenticate the
   bundle using HMAC-SHA256.
3. Debundler decrypts the bundle using the encryption key.

#### Step 2. Displaying a bundle's contents 

Once the bundle contents have been authenticated and decrypted, they may now be
displayed to the user's browser.

#### Possible issues 

Nothing here yet.

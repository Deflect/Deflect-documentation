---
layout: default
title: DDeflect
lang: en
ref: DDeflect
tags:
 - Projects
 - DDeflect
---


DDeflect
========

**Please note that the DDeflect project is [currently not in
production](https://equalit.ie/deflect-reflect/).**


Concept
-------

Distributed Deflect allows volunteers to expand the Deflect infrastructure,
creating the capacity to protect a potentially unlimited number of websites.

#### Technical Assumptions

Distributed Deflect's software and network design begins from the following
assumptions:
- A non-profit, free software initiative will need volunteer-run Deflect servers
  in order to expand.
- A volunteer-run distributed model can be leveraged to handle highly resource
  intensive operations, such as serving cached responses.
- Volunteer proxies will remain under the owner's control, just like the
  decision to join or leave the DDeflect infrastructure at any time. The package
  should be installable on most webserver configurations and run in parallel to
  their existing services.
- Some voluntary proxies will be set up incorrectly or with malicious intent.
  The Deflect team will not be able to control the volunteer proxy's routing or
  content delivery mechanisms, but can practice content authentication while
  monitoring suspect proxies.

![DDeflect scheme](DDeflect.png)

### Goals

In this section we discuss the proposed solution which realizes the main goals
of DDeflect, namely:

1. To ensure the authenticity of the content delivered to the client.
2. To identify malicious untrusted edges (v-edge).
3. To ensure the security of the origin.
4. To provide an accessible solution in a sense that requires no installation of
   an extra third-party software on the client.
5. To share the load of DDoS attack with volunteer edges.

#### Basic Idea 

In the proposed DDeflect model the edges are divided in two groups of trusted
edges and v-edges. Any request from the client is served in two steps:

1. Small authentication code snippet that is served by a trusted edge.
2. The actual requested page that is served by the v-edge.

When the content is completely loaded, the authentication code will check the
integrity of the content served by the v-edge and in case of discrepancy, it
requests it from another volunteer source.

#### Detail 

Suppose that a user using the client machine would like to surf
http://activist.org/webpage.html.

1. After a user requests http://activist.org/webpage.html in their browser, a
   DNS request resolves activist.org to the address to a trusted edge
2. The browser requests webpage.html from trusted.edge.deflect.ca
3. The trusted edge receiving the request retrieves the content of webpage.html
   from activist.org (if it does not already have it in cache)
4. The trusted edge encrypts the retrieved contents of webpage.html and its
   assets, bundles them, and replies to the browser with JavaScript that
   contains a unique index of the encrypted bundle, instructions for decrypting
   the bundle and a referral to fetch the requested content from a v-edge
5. The browser requests the encrypted bundle from the v-edge
6. The v-edge returns the encrypted bundle to the browser
	- If the v-edge did not have the requested bundle in cache, it firsts
	  retrieves it from the trusted edge
7. The browser decrypts the bundle and renders the page
	- If there is an encryption error, the browser reports to the trusted
	  edge and re-requests the content



##### Sequence 

![DDeflect Sequence](DDeflect_sequence.png)


### Achieving the Goals

In this section we argue that the above approach will ensure that the desirable
conditions of DDeflect apply.

1. To ensure the authenticity of the content delivered to the client. The
   JavaScript runs a one way message authentication function on the content to
   make it virtually impossible for the v-edge to fake content with the same
   check sum.
2. To identify malicious v-edges. Every time the JavaScript disapproves the
   content served by the v-edge, the trusted edge raises a red flag in the
   Deflect central database on the entry of the v-edge. When the number of red
   flags exceeds a certain limit, DDeflect automatically blacklists the v-edge
   and will not refer further requests to it.
3. To ensure the security of the origin. The volunteers either retrieve the
   contents from trusted edges and cache them for the future or use an anonymity
   network such as Tor to reach the origin (in the case that the trusted edge is
   under DDoS attack and not responsive). In this way the origin address is
   never shared with the v-edges.
4. To offer an accessible solution in a sense that requires no installation of
   extra third-party software on the client. The only requirement for the
   client's browser is to support iframe and JavaScript. These are basic
   features that are supported by all modern popular browsers.
5. To share the load of DDoS attack with volunteers. The actual content of the
   web pages are served by the v-edges, and trusted edges only need to serve a
   small JavaScript and few checksum values which only require few kilobytes of
   bandwidth.


Components
----------

The Distributed Deflect project is in development. Herein a collection of ideas,
prototypes and pages describing progress. Please join the discussion.

- [Bundler](Bundler)
- [DDeflect Network Components](DDeflect_Network_Components)


Community
---------

Project success will come from a mixture of innovative programming that fits
into the Internet's design, changes in the sector's perception and policies
around DDoS mitigation, community outreach and support. Distributed Deflect will
seek in-kind contributions of network resources from websites receiving DDeflect
protection. Voluntary server donations will be sought from civil society
organisations, international funders, academic and non-profit institutions and
technology companies. The project's innovative, open source and non-profit
nature distinguishes it from similar commercial initiatives and attracts
like-minded organisations and individuals to support the project with easy,
low-cost and immediately understandable contributions. DDeflect will provide
up-to-date traffic statistics on the entire infrastructure and individual
domains as a way to stimulate discussion and interest around DDoS mitigation and
provide extra incentive for those wishing to help websites stay online.

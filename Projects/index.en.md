---
layout: default
title: Projects
lang: en
ref: Projects
tags:
 - Projects
---

We are a growing collective involved in many aspects of Internet Freedom and
digital security. We collaborate with academics, tech NGOs, tech-activists and
anyone with the right skills that wants to deepen their knowledge and ours. If
you're interested and think you can help, please get in touch and tell us what
you could do (info @ deflect . ca). We are currently focused on the following
projects:

- [Bundler](Bundler)
- [DDeflect](DDeflect)
- [Deflect Labs](../Deflect_Labs)
- [eQPress](../eQPress)

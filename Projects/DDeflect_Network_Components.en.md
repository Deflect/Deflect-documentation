---
layout: default
title: DDeflect Network Components
lang: en
ref: DDeflect_Network_Components
tags:
 - Projects
 - DDeflect
---


DDeflect Network Components
===========================
<a name="Trusted_edges"></a>
## Trusted Edges

A trusted edge is currently quite similar to a Deflect edge. 

### Caching

A trusted edge (or t-edge) is used to retrieve pages from origin servers. The
trusted edge is queried by [volunteer edges](#Volunteer_Edges) (or v-edges)
for bundled content, which is then cached. The trusted edge itself caches the
same content retrieved from the origin server.

### Banning

The trusted edge detects attacks placed against it, banning attackers and
preventing them from having an impact on the Deflect network. This information
is used between trusted edges to build knowledge bases of malicious attackers
and will be combined with information passed from the V-edges.

### Monitoring

T-edges will monitor the v-edges for which they are currently responsible. In
turn, either individual t-edges or controller systems can use the
[edgemanage](../Edgemanage) system to analyse the health of v-edges and can
exclude v-edges where installed components are out of date.

T-edges are responsible for chunks of the v-edge pool at any one time. This
information is shared between t-edges using key value stores, storing v-edge
performance history and also managing the exclusion of any potentially malicious
underperforming v-edges.

### Requirements

- A trusted edge must be a physical server
- A trusted edge should preferably use full disk encryption
- Current OS and infrastructure is Debian Wheezy - 64 bit
- Server rental plans should include at least 5TB of monthly traffic

### Setup
#### Bundler

[Bundler](Bundler) is installed via [NPM](https://npmjs.org).

#### Encryption

[ENCfs](../Deflect_DIY/ENCfs) is used to encrypt the ATS configuration
directory, any SSL certificates and origin information stored on the system.

#### Monitoring

The trusted edge runs the latest stable package of Deflect NRPE scripts for
monitoring and alerting.

#### Caching server

Trusted Edges use the eQualit.ie build of Apache Traffic Server, installed from
packages.

#### Attack detection

The servers run the latest stable version of [Banjax](../Deflect_DIY/Banjax).

#### Edgemanage

No server-side configuration is required for [Edgemanage](../Edgemanage).

#### rsyslog Elastic Search logging

Ansible recipes are used to install rsyslog and configure logging to deliver ATS
log information to the Deflect Elastic Search cluster.

### Attack banning

Banjax communicates malicious IP addresses to [Swabber](../BotnetDBP_Swabber).

### SSH server

Trusted edges only allow access via SSH, using keys for authentication from
trusted IP addresses (pseudo-controller machines and other trusted hosts). The
trusted IP address list will be managed by the deployment mechanism.

This is done via the following snippet in /etc/ssh/sshd_config: 

	RSAAuthentication yes
	PubkeyAuthentication yes
	PasswordAuthentication no


### Management

Systems are managed by the Brainiac system, a set of Ansible recipes fed with a
configuration file populated by information from the Deflect
[Dashboard](https://dashboard.deflect.ca). These recipes are used to configure
all aspects of the system, from SSH configuration, to ATS configuration, to
logging.

<a name="Volunteer_Edges"></a>
## Volunteer Edges

Volunteer edges (or v-edges) are network resources contributed by the community.
Apart from a caching server, many aspects of v-edge configuration are left as
suggestions to the administrator, as there are many aspects that are out of the
hands of the Distributed Deflect administrators. 

### Caching

Volunteer edges run a caching server in the same configuration as the
[trusted edges](#Trusted_edges), merely caching different content.

### Banning

Volunteer edges can run a full suite of [BotnetDBP](../BotnetDBP_Intro) components:
currently [Banjax](../BotnetDBP_Banjax) and [Swabber](../BotnetDBP_Swabber).

### Monitoring

A set of Deflect NRPE plugins is provided with the volunteer edge setup. The use
of these plugins is optional for the v-edge operator. 

Volunteer edges MUST, in addition to the t-edge remap rules, implement a remap
rule (with caching) for https://deflect.ca/edgemanage.object.

### Reporting

A volunteer edge reports information about traffic seen and number of requests
to the t-edge network. This interface also offers information about versions of
software installed. This interface is currently a simple web service that can
only be queried by trusted edge servers.

### Limiting

We are developing a rate-limiting service that will offer fine-grained control
over how much bandwidth Deflect can use on a host.

### Update

Volunteer edges poll trusted edges for configurations on a regular basis. This
is currently set to be hourly. The configuration will contain new ban
information, expected software versions and other health information vital to
the network.


User Agent
----------

User agent is a term used to refer to the client browser.

### Browsing

The user agent browses and views pages on DDeflect as normal. If the user agent
is using any plugins such as [RequestPolicy](https://requestpolicy.com), they
will need to whitelist v-edges as content is loaded, but the audience of this
plugin will be familiar with this process.

### Decryption

The user agent must be able to decrypt the bundled content. This is a
restriction that can exclude some browsers. Currently the browsers that are
excluded from use of Distributed Deflect are also browsers that have difficulty
with features such as
[SNI](https://en.wikipedia.org/wiki/Server_Name_Indication), so the concerns are
somewhat minimal. 

---
layout: default
title: Devopsjs
lang: en
ref: Devopsjs
tags:
 - Projects
---


Devopsjs
========

What
----

From [Wikipedia](http://en.wikipedia.org/w/index.php?title=DevOps&oldid=543075822):

	*DevOps (a portmanteau of development and operations) is a software
	development method that stresses communication, collaboration and
	integration between software developers and information technology
	professionals. DevOps is a response to the interdependence of software
	development and IT operations. It aims to help an organization rapidly
	produce software products and services.*


***Devopsjs (working name) - a minimum invention system that tries to make development and operations (devops) better, via a comprehensive system enabling more people to be at least casually involved in every process. Using a semantic wiki and web-based data store, information is highly structured and reusable for consistency and access.***

![](../img/Squarepeg.jpg)

<div style="font-size: 150%">**https://github.com/equalitie/devopsjs**</div>



Why
---

![Facilities](../img/facilities.png)

- People develop and participate in operations across many projects
- Try to flatten development process and participants
    - Fewer special-purpose systems
    - Decrease separation
- Make it more possible for everyone on the project to participate (if they will accept wikis)
    - More "eyes on" systems
- Hybrid of rigid and lightweight practices for hopefullly more manageable development and operations
- Consolidated, up to date, reusable information based on current state
    - manage systems in SMW


![](../img/squarepeg.jpg)


How
---

### Design 

- minimum of (lower level) 'invention'
- "best of breed" widely used components

<table class="wikitable" style="width: 100%">
<tr><th>SMW</th><th>ElasticSearch</th><th>Javascript</th><th>nodejs</th><th>NRPE</th><th>Tests</th></tr>
<tr>
<td><!-- SMW -->
* Front end
    * Authentication
* Configuration
    * Version control
    * Forms & Views
* General documentation, etc
</td>
<td><!-- ElasticSearch -->
* NoSQL database
* Queries
    * Faceted browsing
    * Graphical views
    * Command line
* Reusable data
</td>
<td><!-- Javascript -->
* Widgets
    * Hosts
    * Test results
</td>
<td><!-- nodejs -->
* Component interaction
* Test executor
</td>
<td><!-- NRPE -->
* Nagios Remote Plugin Executor
* Lightweight, well supported with many tests
</td>
<td><!-- tests -->
* Behaviour
* Unit tests
</td>
</tr>
</table>

![Devopsjs: How](../img/Devopsjs_How.png)


Enabling involvement
--------------------

- Via software systems using HTTP/JSON or Logstash transformer
- Command line

<pre>
$ node src/node/edgemanage.js -A staging -gv
…
remove candidate; time: Wednesday, October 30th 2013, 12:27:02 pm
removeActive eval host1.staging.deflect.ca  worry: 0 2013-10-30T13:57:01.765Z vs 2013-10-30T12:27:02.004Z: 540
removeActive eval host2.staging.deflect.ca  worry: 0 2013-10-30T12:57:02.227Z vs 2013-10-30T12:27:02.004Z: 180
addInactive eval host3.staging.deflect.ca worry: 0 initial
add candidate; time:  Wednesday, October 30th 2013, 1:27:01 pm
addInactive eval host4.staging.deflect.ca worry: 0 2013-10-30T12:27:02.004Z vs 2013-10-30T13:27:01.709Z: -360
add candidate; time:  Wednesday, October 30th 2013, 12:27:02 pm
addInactive eval host5.staging.deflect.ca worry: 0 2013-10-30T12:57:02.227Z vs 2013-10-30T12:27:02.004Z: 180
addInactive eval host6.staging.deflect.ca worry: 0 2013-10-30T11:27:02.064Z vs 2013-10-30T12:27:02.004Z: -360
add candidate; time:  Wednesday, October 30th 2013, 11:27:02 am
addInactive eval host7.staging.deflect.ca worry: 0 2013-10-30T13:57:01.765Z vs 2013-10-30T11:27:02.064Z: 900
addInactive eval host8.staging.deflect.ca worry: 0 2013-10-30T11:57:01.466Z vs 2013-10-30T11:27:02.064Z: 180
addInactive eval host9.staging.deflect.ca worry: 0 2013-10-28T15:50:40.491Z vs 2013-10-30T11:27:02.064Z: -15698
add candidate; time:  Monday, October 28th 2013, 3:50:40 pm
/home/devopsjs/dev/devopsjs/src/node/edgemanage.js --activate host8.staging.deflect.ca --deactivate host2.staging.deflect.ca -c "replace host8.staging.deflect.ca 7 days ago [w host2.staging.deflect.ca 4 hours ago [time](time])"
</pre>
 
- https://…/#dashboard/temp/0CKDYxKATk6XsCSUFDJXyw


Behaviour tests
---------------

- [Dan North introduction](http://dannorth.net/introducing-bdd/)

	*… I decided it must be possible to present TDD in a way that gets
	straight to the good stuff and avoids all the pitfalls.<br />My response
	is behaviour-driven development (BDD). It has evolved out of established
	agile practices and is designed to make them more accessible and
	effective for teams new to agile software delivery. Over time, BDD has
	grown to encompass the wider picture of agile analysis and automated
	acceptance testing.*
- Java, Ruby, Javascript, PHP, etc
- Higher level than unit tests
- Should be understandable by most people
- in devops, created through revisions
- constantly create new tests through the lifetime of a project

[CS169.1x: Software as a Service (Berkeley) 16m video on agile workflow using BDD tests](https://www.youtube.com/watch?feature=player_embedded&v=bAu9j0nHOYw)

	Cucumber [BDD implementation](a) meets halfway between customer and developer
		- User stories don't look like code, to clear to customer and can be used to reach agreement
		- Also aren't completely freeform, so can connect to real tests
		- Anyone from developer to end user can better understand operations and problems
			- Define tests


### 1. Define tests on wiki 

<code>
<span style="color:blue">Scenario: </span>Login to site<br />
<span style="color:orange">Given</span> I have access to "deflect.ca"<br />
<span style="color:orange">And</span> there are login credentials<br />
<span style="color:orange">Then</span> I should be able to login
</code>

### 2. Stub code generated on host 


	Given(/^I can access to (\s+)$/, function(callback) {
	  // your great code to validate this condition goes here, working with steps and a "world"
	  callback();
	});

	And(/^there are login credentials$/, function(callback) {
	  callback();
	});

	Then(/^I should be able to login$/, function(callback) {
	  callback();
	});

<span style="color: yellow">Tests fail!</span>


### 3. Refine tests 

<span style="color: green">Tests pass!</span>


Tests end to end
----------------


- Cumulatively add to development and operations test suite
    - One test can apply to many sites, or distinct cases
- Users suggest more tests based on their situations
    - Involvement in process

### Unit test 

<code>
  $ grunt mochaTest:devUnitTest<br />
  Running "mochaTest:devUnitTest" (mochaTest) task<br />
  <br />
  Test http helper functions<br />
    <span style="color: green">✓</span> should find the deflect logo for equalit.ie/user <br />
    <span style="color: green">✓</span> should retrieve headers <br />
    <span style="color: green">✓</span> should check a header to see if it came from the cache <br />
    <span style="color: green">✓</span> should check a response for an expected term <br />
  <br />
  File writing with promises<br />
    <span style="color: green">✓</span> should set up the source and destination files correctly <br />
    <span style="color: green">✓</span> should return a promise for a file's contents <br />
    <span style="color: green">✓</span> should return a promise to a written file <br />
    <span style="color: green">✓</span> should read a file as a template and write it out <br />
  <br />
  8 passing (25ms)<br />
  Done, without errors.

</code>

### Behaviour test 

<code>
  Test wiki.deflect.ca<br />
    <span style="color: green">✓</span> should perform DNS lookup<br />
    <span style="color: green">✓</span> aliases should match<br />
    <span style="color: green">✓</span> exclude locations<br />
    <span style="color: green">✓</span> login to site<br />
  <br />
  4 passing (200ms)<br />
  Done, without errors.
</code>


### Visualization 



Automation
----------

Automation makes expanding systems supportable, and provides data that enables
both humans and programs to make better decisions. In Deflect specifically, this
is be used for edge optimization and traffic analysis, and will be used for
machine learning algorithms, attack comprehension, and projects such as sharing
attack data and ISP quality ratings. We are also providing a live public view of
Deflect's operations, and client specific views.

### Edge rotation 

A component of devopsjs supports command line and automated management of edges
in the Deflect network using the result of automated checks. It automatically
rotates edges based on time or error conditions.

What it supports now
--------------------

- Configuration of hosts via SMW
- Widget displaying current status with graphs¹
- Definition of BDD tests via SMW¹
    - Functional, systems, backup, etc¹
- Widget displaying results¹
- Execution of tests
- Search tests
- Edge rotation

1 - being updated for ElasticSearch backend.


Demo
----

*Being updated*


### Components 

- elasticsearch edge:edge.deflect.ca AND aCheck:check_tcptraffic


Installation
------------

- ~1h

### Host 

- ~30m

- Set up base host
- install:
- git
- java (openJDK 7)
- ElasticSearch
- Apache
- [MediaWiki](http://www.mediawiki.org) + [Semantic
  Mediawiki](http://www.semantic-mediawiki.org) (usually via Semantic Bundle;
  version 1.8 recommended)
- nodejs
	- https://github.com/creationix/nvm

# Devopsjs 

- ~30m

- git clone https://github.com/equalitie/devopsjs.git
- cd devopsjs
- edit config/localConfig.js
- edit config/hosts
- generate config/hosts.json:
- configure wiki
    - MediaWiki:Common.js
- Templates, forms, properties
- nagios-nrpe on hosts


Next steps
----------

While focused on practical operations, we anticipate development in the following areas:

- Refine forms & templates
- Integration with ticket system
    - Automated ticket validation & regression testing
- Ad hoc tests, run at specific times
- Query NRPE directly (without using shell / check_nrpe)
- Simpler installation
- Get more people to participate!

### How to participate 

- https://github.com/equalitie/devopsjs

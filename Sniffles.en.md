---
layout: default
title: Sniffles
lang: en
ref: Sniffles
tags:
 - Deflect Tech
 - Deflect Labs
---

Sniffles
========

Traditionally, Deflect [edges](About_Deflect#Design) are low-cost Linux VPSs
or dedicated servers existing in heavily filtered colocation facilities.
Sniffles is a pair of Linux systems with a dedicated unfiltered Internet uplink
and a large pool of IP addresses available.

One machine is in practical terms a typical Deflect edge.

The other is a Linux router sitting between the edge and the unfiltered Internet
connection, able to capture all traffic (including UDP- or ICMP-based attack
traffic, which is typically heavily filtered before it reaches traditional
edges) and then forward legitimate traffic to the edge server, where it is
handled as normal.

Each Deflect-protected site is assigned a dedicated unique IP address by
[Edgemanage](Edgemanage) from
the pool available to the router, giving a reliable indication of which site is
under attack at any given time (frequently missing from attack traffic against a
shared IP address with name-based virtual hosting).

Sniffles primary outputs are:

- reliable identification of attack target; and
- raw pcap or flow data regarding an attack.

Each of these outputs can be considered an input into both
[Opsdash](Opsdash) and [BotHound](BotHound).

---
layout: default
title: Can I get the code?
lang: fa
ref: Can I get the code
tags:
 - FAQ
 - FAQ_technical
---



شما می‌توانید به کد Deflect [اینجا](../Deflect_DIY/Installing_Deflect) دسترسی پیدا کنید. 
|آیا_امکان_دسترسی_به_کد_را_دارم

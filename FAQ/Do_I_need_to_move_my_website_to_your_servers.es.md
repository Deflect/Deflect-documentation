---
layout: default
title: ¿Tengo que mover mi sitio web a sus servidores?
lang: es
ref: Do I need to move my website to your servers
tags:
 - FAQ
 - FAQ_overview
---


¿Tengo que mover mi sitio web a sus servidores?
===============================================

- No. Deflect es una infraestructura de mitigación de ataques DDoS, no un
  alojamiento de sitios web. Para unirse a Deflect simplemente necesita cambiar
  sus registros DNS. Su sitio web continúa en su proveedor de alojamiento
  original. Podemos aconsejarle proveedores de precio y fiabilidad más
  razonables si no está contento con su actual alojamiento.

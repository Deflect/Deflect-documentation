---
layout: default
title: Ya estoy pagando por alojamiento, ¿de verdad necesito esto?
lang: es
ref: Already paying for hosting
tags:
 - FAQ
 - FAQ_overview
---


Ya estoy pagando por alojamiento, ¿de verdad necesito esto?
===========================================================

- Muy probablemente. Proporcionamos nuestro servicio por encima de su
  alojamiento de forma gratuita. Puede reducir el coste de su alojamiento al
  reducir enormente la cantidad de tráfico con la que su servidor tiene que
  tratar.
- Hay algunos servidores dedicados que anuncian su preparación contra ataques
  DDoS, pero puede ser un negocio caro y restrictivo.

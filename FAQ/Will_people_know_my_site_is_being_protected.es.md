---
layout: default
title: ¿Sabrá la gente que mi sitio está siendo protegido?
lang: es
ref: Will people know my site is being protected
tags:
 - FAQ
 - FAQ_tech
---


¿Sabrá la gente que mi sitio está siendo protegido?
===================================================

- La protección de Deflect no es visible inmediatamente al ojo no entrenado.
- Un usuario web (o atacante) experimentado puede averiguarlo mirando
  detenidamente el código entregado con la página web.
- Además, cuando alguien trate de iniciar sesión en la sección editorial de su
  sitio web, ¡será repelido (deflected)!

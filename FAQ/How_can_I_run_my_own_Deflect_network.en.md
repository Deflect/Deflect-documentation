---
layout: default
title: How can I run my own Deflect network?
lang: en
ref: How can I run my own Deflect network
tags:
 - FAQ
 - FAQ_tech
---


How can I run my own Deflect network?
=====================================

Detailed instructions on how to run a Deflect network can be found in the
[Deflect DIY](../Deflect_DIY) section.

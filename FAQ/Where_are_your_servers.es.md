---
layout: default
title: ¿Dónde están sus servidores?
lang: es
ref: Where are your servers?
tags:
 - FAQ
 - FAQ_tech
---


¿Dónde están sus servidores?
============================

Nuestros VPSs (servidores privados virtuales) están dispersos por tres
continentes en ubicaciones seguras.

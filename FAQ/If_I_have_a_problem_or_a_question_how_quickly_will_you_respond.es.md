---
layout: default
title: Si tengo un problema o una pregunta, ¿cómo de rápido responderán?
lang: es
ref: How quickly will you respond
tags:
 - FAQ
 - FAQ_overview
---


Si tengo un problema o una pregunta, ¿cómo de rápido responderán?
=================================================================

- Tratamos de responder con prontitud y habitualmente en 24 horas.
- Somos un grupo sin ánimo de lucro de activistas tecnológicos comprometidos en
  distintas zonas horarias, así que habitualmente alguien está en línea para
  pasar a la acción cuando se da un problema.
- Va en nuestro interés proporcionar un servicio eficiente si un cliente está
  sufriendo un ataque DDoS, afecta a toda nuestra infraestructura.   

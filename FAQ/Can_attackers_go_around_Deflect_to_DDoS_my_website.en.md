---
layout: default
title: Can attackers go around Deflect to DDoS my website?
lang: en
ref: Can attackers go around Deflect to DDoS my website? 
tags:
 - FAQ
 - FAQ_technical
---


Can attackers go around Deflect to DDoS my website?
===================================================

If attackers know your webserver's IP address, they can.

Deflect protects you from this by hiding the IP address of the server where your
site is hosted. Once behind Deflect, your website's name will resolve, with DNS,
to Deflect's IP addresses. Only Deflect then knows what the real IP address of
your website's server is, while public visitors will only see Deflect's IP
addresses associated to your website.

Once you are behind Deflect, there are two ways attackers can locate your web
server:

- Through other services that aren't behind Deflect, like an email server.  It's
  important to make sure other services have their own IP addresses.
- Historical DNS records. We can advise you on whether your web server IP can be
  found on the internet, and how to get a new one - if you need it.

---
layout: default
title: Who are you?
lang: fa
ref: Who are you
tags:
 - FAQ
 - FAQ_about
---


-  Deflect توسط شرکت کانادایی غیر انتفاعی [eQualit.ie](http://equalit.ie)  اداره
   می‌شود. ما مجموعه‌ای کوچک و متعهد از فعالان تکنولوژی (Techtivist )
   هستیم که روی حفاظت از مدافعان حقوق بشر و روزنامه‌نگاران مستقل تمرکز
   کرده‌ایم.
   مقر اصلی eQuali.ie در مونترال است ، گرچه تک تک ما در سرتاسر جهان زندگی
   می‌کنیم.

- تیم Deflect از برنامه‌نویسان، متخصصان رمزگذاری، مسوولان سیستم، مهندسان
  سیستم و مشاوران تکنولوژی تشکیل شده است. در مجموع ما ده‌ها سال تجربه در
  زمینه‌های مختلف و زبان‌های مختلف برنامه‌نویسی داریم و در
  پروژه‌های عمومی، خصوصی، تجاری و دانشگاهی کار کرده‌ایم. بیوگرافی کامل
  اعضای تیم را می‌توانید در [ﺍیﻦﺟﺍ](https://equalit.ie/#team)
  بخوانید. 
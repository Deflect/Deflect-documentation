---
layout: default
title: What is Deflect?
lang: en
ref: FAQ What is Deflect
tags:
 - FAQ
 - FAQ_overview
---


What is Deflect?
================

* Deflect is a reverse caching service for websites vulnerable to Distributed
  Denial of Service (DDoS) attacks. Though the websites don't change their IP
  addresses, Deflect ensures their home servers don't have to deal with a sudden
  influx of artificial visitors or 'bots' trying to drag the sites offline.
  Instead, these requests all get redirected to Deflect, a network of servers
  built specifically to handle them.
- Meanwhile, legitimate traffic to the websites still gets full access to all
  published content. Unlike commercial mitigation services, we do not charge and
  we will not change the [Terms of Service](../ToS). There is no contract nor
  minimum length of time in which to stay on the network.

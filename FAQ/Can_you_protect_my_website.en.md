---
layout: default
title: Can you protect my website?
lang: en
ref: Can you protect my website
tags:
 - FAQ
 - FAQ_overview
---


Can you protect my website?
===========================

- Yes. We can protect your website from being overwhelmed by too much traffic,
  whether that traffic is malicious in origin or is the result of great
  popularity. 
- We also hide the origin server address of your website, which helps protect
  against other types of attack, such as password hacking. If a hacker can't
  find your host, they can't launch an attack.
- Deflect is specifically designed for DDoS-mitigation. It is not a catch-all
  web security suite nor a web hosting provider.   

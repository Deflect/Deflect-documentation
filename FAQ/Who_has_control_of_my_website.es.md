---
layout: default
title: ¿Quién tiene el control de mi sitio web?
lang: es
ref: Who has control of my website
tags:
 - FAQ
 - FAQ_overview
---


¿Quién tiene el control de mi sitio web?
========================================

Lo tiene usted. Nosotros simplemente ayudamos a entregar el contenido, pero no
lo alteramos. Podría decirse que controlamos la copia de seguridad, ya que
conmutar su DNS hacia nuestros servidores significa que se efectúa una copia de
seguridad automática de su sitio web cada vez que consigue un visitante.

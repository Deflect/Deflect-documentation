---
layout: default
title: ¿Mi sitio web necesita Deflect?
lang: es
ref: Does my website need Deflect
tags:
 - FAQ
 - FAQ_overview
---


¿Mi sitio web necesita Deflect?
===============================

- Probablemente. No necesita estar bajo ataque constante para necesitar Deflect,
  tan solo tener una sospecha razonable de que su sitio puede haber sido marcado
  como objetivo por hackers y para ataques DDoS.
- Algunos de los sitios bajo nuestra protección están sujetos a ataques DDoS
  casi-continuos, aunque con otros pueden pasar meses sin ningún tráfico
  sospechoso.
- Es posible esperar un ataque y cambiar entonces a Deflect (vea [¿Puedo cambiar
  a Deflect durante un ataque
  DDoS?](Can_I_switch_to_Deflect_during_a_DDoS_attack)) pero es [mucho más
  efectivo](../Who_Should_Consider_Deflect) cambiar anticipadamente.

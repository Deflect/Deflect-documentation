---
layout: default
title: How much does it cost?
lang: en
ref: How much does it cost
tags:
 - FAQ
 - FAQ_overview
---


How much does it cost?
======================

- The Deflect service is free to any website or network of websites that
  represent human rights organizations, activists, dissident bloggers or
  independent media. There is no contract and you may use the service when you
  please.
- Thankfully it costs us a remarkably small amount to maintain our network of
  edges.	

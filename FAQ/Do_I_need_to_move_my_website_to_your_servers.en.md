---
layout: default
title: Do I need to move my website to your servers?
lang: en
ref: Do I need to move my website to your servers
tags:
 - FAQ
 - FAQ_overview
---


Do I need to move my website to your servers?
=============================================

- No. Deflect is a DDoS mitigation infrastructure, not a website host. To join
  Deflect you simply need to change your DNS records. Your website remains on
  its original hosting provider. We can advise you on more reasonably priced and
  reliable providers if you are unhappy with your current host.
- If you need free secure hosting and your website is based on WordPress, we can
  offer you this option with [eQPress](https://equalit.ie/eqpress).

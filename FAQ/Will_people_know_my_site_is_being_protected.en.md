---
layout: default
title: Will people know my site is being protected?
lang: en
ref: Will people know my site is being protected
tags:
 - FAQ
 - FAQ_tech
---


Will people know my site is being protected?
============================================

- Deflect protection is not immediately visible to the untrained eye.
- An experienced web user (or attacker) can find out by looking closely at the
  code delivered with the web page. 
- Also, when someone tries to log into your website's editorial, they will be
  Deflected!

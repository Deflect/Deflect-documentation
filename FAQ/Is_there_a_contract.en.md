---
layout: default
title: Is there a contract?
lang: en
ref: Is there a contract
tags:
 - FAQ
 - FAQ_overview
---

## Is there a contract?

- No. You are free to join or leave the Deflect network any time.
- When you subscribe, we ask you to approve our [Terms of Service](../ToS) and
  [Privacy Notice](../Privacy_Notice).

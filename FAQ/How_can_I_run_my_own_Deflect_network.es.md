---
layout: default
title: ¿Cómo puedo ejecutar mi propia red de Deflect?
lang: es
ref: How can I run my own Deflect network
tags:
 - FAQ
 - FAQ_tech
---


¿Cómo puedo ejecutar mi propia red de Deflect?
==============================================

Puede encontrar instrucciones detalladas sobre cómo ejecutar una red Deflect en
la sección [Deflect DIY](../Deflect_DIY).

---
layout: default
title: ¿Afecta a mis ingresos por publicidad?
lang: es
ref: Does it affect my ad revenue
tags:
 - FAQ
 - FAQ_overview
---


¿Afecta a mis ingresos por publicidad?
======================================

Deflect no afectará a sus ingresos por publicidad o a las estadísticas de
tráfico de sistemas que usan contenido externo, que es como funcionan la mayoría
de sistemas como Google Ads y Google Analytics. Las páginas web pueden incluir
contenido adicional de su sitio origen (origin), o de sitios externos; sólo
almacenamos en caché el contenido alojado en el sitio origen. Si reproduce
anuncios publicitarios para Google o la mayoría del resto de servicios de
publicidad, o si usa un rastreador analítico, los navegadores excepcionalmente
obtendrán de sitios externos anuncios publicitarios, enlaces analíticos de
impactos, u otro contenido.

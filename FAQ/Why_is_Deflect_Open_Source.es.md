---
layout: default
title: ¿Por qué Deflect es de código abierto?
lang: es
ref: Why is Deflect Open Source
tags:
 - FAQ
 - FAQ_tech
---


¿Por qué Deflect es de código abierto?
======================================

Deflect ha sido erigido sobre algunos de los mejores principios y software de
código abierto. Deseamos adherirnos a los principios y contribuir a la comunidad
de código abierto - hacer el código abierto y la documentación del sistema
públicamente disponible y gratuita. Creemos que Internet debe continuar siendo
un foro de libre expresión y pensamiento, y de transformación social.
Compartiendo lo que conocemos, queremos que la gente aprenda de ello, lo
repliquen para si mismos, y eventualmente seremos nosotros los que también
aprenderemos de ellos. También creemos que una infraestructura de seguridad debe
erigirse sobre buenos principios, no secretos. Si no tenemos nada que ocultar,
no somos vulnerables a la exposición de ese secreto. Esto hace incluso más
difícil el trabajo del atacante.

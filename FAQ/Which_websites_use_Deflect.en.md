---
layout: default
title: Which websites use Deflect?
lang: en
ref: Which websites use Deflect
tags:
 - FAQ
 - FAQ_overview
---


Which websites use Deflect?
===========================

We do not disclose the identity of our partners (nor will we disclose
yours) to the public, other partners nor project donors. All of websites
protected by Deflect are built and maintained by not-for-profit human
rights and independent media organisations that work for and according
to the principles enshrined in the
[UDHR](http://www.un.org/en/documents/udhr).

Herein a geo-mapping catchment of cultural content carried by the
websites' of our current partners.

![Partner map](../img/Partnermap.png)

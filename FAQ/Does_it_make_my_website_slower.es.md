---
layout: default
title: ¿Ralentiza mi sitio web?
lang: es
ref: Does it make my website slower
tags:
 - FAQ
 - FAQ_overview
---


¿Ralentiza mi sitio web?
========================

No, de hecho debería hacer que las páginas carguen más rápido para sus lectores.
Esa es la belleza de los servidores de almacenamiento en caché - responden
rápidamente con el contenido de la página. Al absorver la mayoría del tráfico
destinado a su sitio web, se reduce el esfuerzo de su servidor. Este es el caso
durante la actividad normal, no sólo durante un ataque DDoS.

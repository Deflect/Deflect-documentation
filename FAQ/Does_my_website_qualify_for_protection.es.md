---
layout: default
title: ¿Mi sitio web reúne requisitos para recibir protección?
lang: es
ref: Does my website qualify for protection
tags:
 - FAQ
 - FAQ_overview
---


¿Mi sitio web reúne requisitos para recibir protección?
=======================================================

Evaluamos los sitios web por dos criterios clave:

- ¿Su trabajo no tiene ánimo de lucro y tiene que ver con medios de comunicación
  independientes y/o defesores de los derechos humanos?

- ¿Tiene razones para creer que su sitio web puede ser objeto de un ataque DDoS
  a causa del trabajo que lleva a cabo? ¿O ya ha sido elegido como objetivo?

Lea más en la página [Elegibilidad](Eligibility).

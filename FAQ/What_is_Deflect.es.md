---
layout: default
title: ¿Qué es Deflect?
lang: es
ref: FAQ What is Deflect
tags:
 - FAQ
 - FAQ_overview
---


¿Qué es Deflect?
================

- Deflect es un servicio de almacenamiento en caché inverso (transparente) para
  sitios web vulnerables a ataques Distribuidos de Denegación de Servicio
  (DDoS). Aunque los sitios web no cambian sus direcciones IP, Deflect garantiza
  que sus servidores principales no tienen que afrontar repentinos flujos
  entrantes de visitantes artificiales o 'bots' que traten de arrastrar a los
  sitios a su desconexión efectiva. En su lugar, estas peticiones son
  redirigidas a Deflect, una red de servidores construida específicamente para
  afrontarlos.
- Entre tanto, el tráfico legítimo hacia los sitios web todavía obtiene acceso
  completo a todo el contenido publicado. A diferencia de servicios de
  mitigación comerciales, no realizamos cargos y no cambiaremos los [términos del
  servicio](../ToS). No hay contrato ni periodo mínimo de tiempo de permanencia en la
  red.

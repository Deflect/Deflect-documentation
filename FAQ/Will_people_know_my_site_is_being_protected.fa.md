---
layout: default
title: Will people know my site is being protected?
lang: fa
ref: Will people know my site is being protected
tags:
 - FAQ
 - FAQ_tech
---



- فورا قابل تشخیص توسط کسانی که آموزش ندیده باشند نیست.
- یک کاربر با تجربه وب (یا حمله‌کننده) می‌تواند با نگاه کردن دقیق به
  کدی که با صفحه وب سایت می‌آید این موضوع را تشخیص دهد.
- به همچنین وقتی کسی بخواهد به صفحه ویرایش وب‌سایت شما وارد شود، (login
  کند)، Deflect خواهد شد.  | آیا مردم می‌فهمند که سایت من تحت محافظت است

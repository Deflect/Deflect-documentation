---
layout: default
title: Who has control of my website?
lang: fa
ref: Who has control of my website
tags:
 - FAQ
 - FAQ_overview
---


شما. ما فقط کمک می‌کنیم که محتوا به مخاطب تحویل داده شود ولی هیچ تغییری در
آن نمی‌دهیم. می‌شود گفت ما بر پشتیبان وب سایت شما کنترل داریم، زیرا با
عوض کردن DNSتان به سرورهای ما هربار کسی  وب‌سایت شما رو می‌بیند یک
پشتیبان اتوماتیک از آن گرفته می‌شود.

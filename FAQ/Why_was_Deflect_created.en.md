---
layout: default
title: Why was Deflect created?
lang: en
ref: Why was Deflect created
tags:
 - FAQ
 - FAQ_about
---


Why was Deflect created?
========================

- The fundamental reason for Deflect's creation came about because of this
  necessity: there is an urgent need for this service amongst individuals and
  groups who are under threat of cyberattacks yet are unable to afford
  commercial mitigation services. 

- Most DDoS attacks are rudimentary and can be mitigated by good technology.
  Many website administrators doing good work in potentially hazardous
  circumstances simply don’t have the time or resources to allocate for a
  dedicated technical team. 

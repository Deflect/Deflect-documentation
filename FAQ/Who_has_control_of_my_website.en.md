---
layout: default
title: Who has control of my website?
lang: en
ref: Who has control of my website
tags:
 - FAQ
 - FAQ_overview
---


Who has control of my website?
==============================

You do. We simply help deliver the content but we make no changes to it. You
could say we control the backup, since switching your DNS to our servers means
there is an automatic backup of your site every time you get a visitor.

---
layout: default
title: Can I use Deflect while my website is under construction?
lang: en
ref: Can I use Deflect while my website is under construction
tags:
 - FAQ
 - FAQ_overview
---


Can I use Deflect while my website is under construction?
=========================================================

Yes. In fact, we recommend it.

The earlier you protect your website behind Deflect, the fewer people will know
its IP address, and this will reduce the chances of an attack reaching your
server.

Register your website as early as possible on Deflect. You will still be fully
able to access any web administration panel through Deflect, or connect directly
to the IP address of your server should the need arise. 

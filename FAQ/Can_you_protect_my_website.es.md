---
layout: default
title: ¿Pueden proteger mi sitio web?
lang: es
ref: Can you protect my website
tags:
 - FAQ
 - FAQ_overview
---


¿Pueden proteger mi sitio web?
==============================

- Sí. Tenemos capacidad de proteger su sitio web de ser apabullado por tráfico
  excesivo, tanto si ese tráfico es malicioso desde su germen o es el resultado
  de una gran popularidad. 
- También podemos ocultar la dirección del servidor de procedencia de su sitio
  web, lo que ayuda a proteger contra otros tipos de ataque, como un hackeo de
  contraseña. Si un hacker no puede encontrar su servidor, no podrá lanzar un
  ataque.
- Deflect está específicamente diseñado para la mitigación de ataques DDoS.  No
  es un conjunto de seguridad web que lo cubra todo ni un proveedor de
  alojamiento web. 

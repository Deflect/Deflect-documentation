---
layout: default
title: Can I get the code?
lang: en
ref: Can I get the code
tags:
 - FAQ
 - FAQ_technical
---

## Can I get the code?

You can get the Deflect code [right here](../Deflect_DIY/Installing_Deflect). 

---
layout: default
title: ¿Puedo cambiar a Deflect durante un ataque DDoS?
lang: es
ref: Can I switch to Deflect during a DDoS attack
tags:
 - FAQ
 - FAQ_overview
---


¿Puedo cambiar a Deflect durante un ataque DDoS?
================================================

- Puede cambiar a la red de Deflect en cualquier momento pero tiene más sentido
  hacerlo anticipándose a un ataque. Si espera hasta que su sitio esté bajo
  ataque todavía podemos ayudarle, pero el DNS puede tardar hasta 24 horas en
  propagarse. Entre tanto, los bots que tengan su dirección continuarán
  bombardeando su sitio web con peticiones, así que nuestro almacenado en caché
  no será efectivo inmediatamente. Como reza el dicho, prevenir es mejor que
  curar, así que intercambiar su sitio web por adelantado puede ahorrar muchos
  dolores de cabeza en el futuro. En general, necesitamos evaluar su sitio antes
  de conmutarlo, y esto puede llevar tiempo, pero si es una emergencia, entonces
  por supuesto póngase en contacto y haremos lo que podamos.

---
layout: default
title: Why is Deflect Open Source?
lang: en
ref: Why is Deflect Open Source
tags:
 - FAQ
 - FAQ_tech
---


Why is Deflect Open Source?
===========================

Deflect has been built using some of the best open source software and
principles. We would like to abide and contribute to the open source community -
making the source code and system documentation publicly available and free. We
believe the Internet should remain a forum for free speech, independent thought
and social change. In sharing what we know, we want people to learn from it,
replicate it for themselves and eventually we will learn from them. We also
believe that a security infrastructure should be built on good principles, not
secrets. If we don't have anything to hide, we are not vulnerable to the
exposure of this secret. This makes the attacker's job even harder.


---
layout: default
title: Does it make my website slower?
lang: en
ref: Does it make my website slower
tags:
 - FAQ
 - FAQ_overview
---


Does it make my website slower?
===============================

No, in fact it should make pages appear faster for your readers. That's the
beauty of caching servers - they quickly reply with the page's content. By
absorbing the majority of traffic destined for your website, we reduce the
strain on your server. This is the case on a day-to-day basis, not just during a
DDoS attack.

---
layout: default
title: ¿Qué sitios web usan Deflect?
lang: es
ref: Which websites use Deflect
tags:
 - FAQ
 - FAQ_overview
---


¿Qué sitios web usan Deflect?
=============================

No desvelamos la identidad de nuestros partícipes (ni desvelaremos la suya) al
público, ni a otros partícipes o donantes del proyecto. Todos los sitios web
protegidos por Deflect están construidos y mantenidos por organizaciones sin
ánimo de lucro defensoras de los derechos humanos y medios independientes que
trabajan para, y de acuerdo a, los principios consagrados en la [Declaración
Universal de los Derechos
Humanos](http://www.ohchr.org/EN/UDHR/Pages/Language.aspx?LangID=spn)

A continuación una instantánea geo-localizada del contenido cultural
transportado por los sitios web de nuestros partícipes.

![Partner map](../img/Partnermap.png)

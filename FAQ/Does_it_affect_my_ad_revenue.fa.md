---
layout: default
title: Does it affect my ad revenue?
lang: fa
ref: Does it affect my ad revenue
tags:
 - FAQ
 - FAQ_overview
---



نه. ما فقط محتوایی از سایت شما را که روی سرور مبدا هست را cache می‌کنیم.
اگر شما تبلیغاتی از گوگل یا سایر شرکت‌های تبلیغاتی دارید، آن‌ها به
جدیدترین صفحه‌های cache شده منتقل می‌شود به طوری که انگار از مبدا
آمده‌اند.
|آیا درآمد حاصل از تبلیغات وب‌سایت تحت تاثیر قرار می‌گیرد

---
layout: default
title: Where are your servers?
lang: en
ref: Where are your servers?
tags:
 - FAQ
 - FAQ_tech
---


Where are your servers?
=======================

Our VPSs are spread across three continents in secure locations.    

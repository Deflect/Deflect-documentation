---
layout: default
title: ¿Cómo cambio a Deflect?
lang: en
ref: How do I change to Deflect
tags:
 - FAQ
 - FAQ_overview
---


¿Cómo cambio a Deflect?
=======================


1. Compruebe si cumple nuestros [criterios de elegibilidad](../Eligibility)
para acceder a protección y si se satisfacen todos los demás
[requerimientos](../Assumptions).
2. Vaya a
   [https://dashboard.deflect.ca/signup](https://dashboard.deflect.ca/signup) y
   siga estas [instrucciones](../Dashboard_Walkthrough/Walkthrough).
3. Cambie sus registros DNS para que se dirijan hacia Deflect.

Si se dan esas condiciones, eso es todo lo que se necesita.

También recomendamos a los sitios web que cambien su dirección IP al alternar a
Deflect, porque si no hay registros DNS apuntando directamente a su servidor,
una nueva IP nunca será revelada y su verdadera dirección puede ser ocultada de
Internet y de los potenciales atacantes.

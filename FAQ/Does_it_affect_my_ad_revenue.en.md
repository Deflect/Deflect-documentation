---
layout: default
title: Does it affect my ad revenue?
lang: en
ref: Does it affect my ad revenue
tags:
 - FAQ
 - FAQ_overview
---


Does it affect my ad revenue?
=============================

Deflect won't affect your ad revenue or traffic statistics from systems that use
external content, which is how most systems like Google Ads and Analytics work.
Web pages can include additional content from your site (the origin), or
external sites. We cache only content hosted on the origin site. If you run ads
from Google or most other ad services, or use an analytics tracker, browsers
will uniquely retrieve ads, analytics hit links, or other content from external
sites.

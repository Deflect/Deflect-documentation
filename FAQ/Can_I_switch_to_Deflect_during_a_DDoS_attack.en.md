---
layout: default
title: Can I switch to Deflect during a DDoS attack?
lang: en
ref: Can I switch to Deflect during a DDoS attack
tags:
 - FAQ
 - FAQ_overview
---


Can I switch to Deflect during a DDoS attack?
=============================================

You can switch over to the Deflect network any time, but it makes more sense to
do so in advance of an attack. If you wait until your site is under attack, we
can still help you but the DNS may take up to 24 hours to propagate. Meanwhile, 
the bots that have your IP address will continue to bombard your website with 
requests, so our caching won't be effective right away. As they say, prevention
is better than cure, so switching your website in advance may save a lot of
headache and downtime in the future. In general, we need to evaluate your site
before switching, and this can take time, but if it's an emergency, then of
course please get in touch and we will do what we can.
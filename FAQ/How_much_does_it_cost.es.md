---
layout: default
title: ¿Cuánto cuesta?
lang: es
ref: How much does it cost
tags:
 - FAQ
 - FAQ_overview
---


¿Cuánto cuesta?
===============

- El servicio de Deflect es gratuito para cualquier sitio web o red de sitios
  que represente organizaciones de defensa de los derechos humanos, activistas,
  bloqueros disidentes o medios independientes. No hay contrato y puede usar el
  servicio cuando le plazca.
- Afortunadamente nos cuesta una cantidad remarcablemente pequeña mantener
  nuestra red de servidores perímetro (edge)

---
layout: default
title: Who are you?
lang: en
ref: Who are you
tags:
 - FAQ
 - FAQ_about
---

Who are you?
============

- Deflect is created and maintained by the Canadian not-for-profit tech
collective [eQualit.ie](http://equalit.ie). We are a small, dedicated collection
of Techtivists focused on protecting human rights defenders and independent
journalists. eQualit.ie is based in Montreal, though individually we are spread
across the globe.

- The Deflect team is comprised of professional developers, cryptographers,
system admins, system engineers and tech consultants. Between us we have decades
of experience across a wide range of disciplines and computer languages and have
worked on public, private, commercial and academic projects. Our full bios can
be found [here](https://equalit.ie/#team).

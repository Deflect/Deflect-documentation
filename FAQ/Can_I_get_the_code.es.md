---
layout: default
title: ¿Puedo conseguir el código?
lang: en
ref: Can I get the code
tags:
 - FAQ
 - FAQ_technical
---


¿Puedo conseguir el código?
===========================

Puede obtener el código de Deflect [aquí
mismo](../Deflect_DIY/Installing_Deflect).

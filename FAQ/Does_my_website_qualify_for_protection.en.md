---
layout: default
title: Does my website qualify for protection?
lang: en
ref: Does my website qualify for protection
tags:
 - FAQ
 - FAQ_overview
---


Does my website qualify for protection?
=======================================

We evaluate websites by two key criteria:

- Is your work not-for-profit and concerned with independent media and/or
  defending human rights? 

- Do you have reason to believe your website may be subject to a DDoS attack
    because of the work you undertake? Or have you already been targeted? 

Read more on the [Eligibility](../Eligibility) page.

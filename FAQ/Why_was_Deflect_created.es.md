---
layout: default
title: ¿Por qué fue creado Deflect?
lang: es
ref: Why was Deflect created
tags:
 - FAQ
 - FAQ_about
---


¿Por qué fue creado Deflect?
============================

- La razón fundamental para la creación de Deflect se suscitó a causa de esta
  demanda: Hay una necesidad urgente de este servicio entre individuos y grupos
  que están bajo amenaza de ciberataques al tiempo que no pueden permitirse
  servicios comerciales de mitigación de tales ataques.

- La mayoría de los ataques DDoS son rudimentarios y se pueden mitigar con buena
  tecnología. Muchos administradores de sitios web que realizan un gran trabajo
  en circunstancias potencialmente peligrosas simplemente no tienen el tiempo o
  los recursos a asignar para un equipo técnico dedicado.

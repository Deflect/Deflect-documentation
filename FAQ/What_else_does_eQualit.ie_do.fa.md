---
layout: default
title: What else does eQualit.ie do?
lang: fa
ref: What else does eQualit.ie do
tags:
 - FAQ
 - FAQ_about
---


- eQualit.ie در بیش از ۳۰ کشور جلسات آموزشی
  [Digital Security](http://equalit.ie/content/digital-security-training)  برای
  فعالان از سرتاسر جهان برگزار می‌کند و یک مدرسه امنیت دیجیتال در کشور
  لیتوانی تاسیس کرده است که این تواناهایی بتوانند دست به دست شوند.
- به علاوه، ما [case management system](http://equalit.ie/content/rightscase)  را
  درست کرده‌ایم تا به فعالان کمک کنیم که موارد نقض حقوق بشر را ثبت و تحیل
  کنند.  | eQualit.ie چه کارهای دیگری انجام می‌دهد

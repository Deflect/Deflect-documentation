---
layout: default
title: ¿Hay un contrato?
lang: es
ref: Is there a contract
tags:
 - FAQ
 - FAQ_overview
---


¿Hay un contrato?
=================

No. Es libre de unirse o abandonar la red de Deflect en cualquier momento.

---
layout: default
title: Does my website need Deflect?
lang: en
ref: Does my website need Deflect
tags:
 - FAQ
 - FAQ_overview
---


Does my website need Deflect?
=============================

- Probably. You do not need to be under constant attack to need Deflect, just to
  have a reasonable suspicion that your site may be targeted by hackers and DDoS
  attacks.
- Some of the sites under our protection are subject to near-continuous DDoS
  attacks, while others may go months without any suspicious traffic. 
- It is possible to wait for an attack and switch over then (see [Can I switch
  to Deflect during a DDoS
  attack?](Can_I_switch_to_Deflect_during_a_DDoS_attack)) but it is [much more
  effective](../Who_Should_Consider_Deflect) to switch in advance.

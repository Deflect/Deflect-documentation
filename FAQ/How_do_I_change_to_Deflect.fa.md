---
layout: default
title: How do I change to Deflect?
lang: fa
ref: How do I change to Deflect
tags:
 - FAQ
 - FAQ_overview
---



۱. به اطلاع دهید که می‌خواهید شروع به استفاده کنید.
۲. ما سایت شما را بررسی می‌کنیم که  آیا واجد شرایط هست یا نه. صفحه [آیا وب‌سایت من واجد شرایط برای حفاظت
هست؟](Does_my_website_qualify_for_protection) را ببینید.
۳. ما یک محیط آزمایشی برای شما برپا می‌کنیم.
۴. شما DNS خود راتغییر می‌دهید تا به Deflect اشاره کند.

اگر همه چیز خوب پیش برود، همه کار همین است. ما توصیه می‌کنیم که آدرس IP خود
را بعد از پیوستن به Deflect تغییر دهید.
| چگونه می‌توانم به Deflect بپیوندم

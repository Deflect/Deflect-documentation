---
layout: default
title: ¿Quiénes son ustedes?
lang: es
ref: Who are you
tags:
 - FAQ
 - FAQ_about
---

¿Quiénes son ustedes?
=====================

- Deflect está creado y mantenido por el colectivo tecnológico canadiense sin
  ánimo de lucro [eQualit.ie](http://equalit.ie). Somos un pequeño y dedicado
  grupo de activistas tecnológicos centrados en la protección de los defensores
  de derechos humanos y los periodistas independientes. eQualit.ie tiene su base
  en Montreal, aunque individualmente estamos dispersos por todo el globo.

- El equipo de Deflect está compuesto por desarrolladores profesionales,
  criptógrafos, administradores de sistemas, ingenieros de sistemas y
  consultores tecnológicos. Entre nosotros hay décadas de experiencia en un
  amplio rango de disciplinas y lengujes de computadora, y hemos trabajado en
  proyectos públicos, privados, comerciales y académicos. Puede encontrar
  nuestras biografías completas [aquí](https://equalit.ie/#team).

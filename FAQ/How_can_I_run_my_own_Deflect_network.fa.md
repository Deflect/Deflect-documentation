---
layout: default
title: How can I run my own Deflect network?
lang: fa
ref: How can I run my own Deflect network
tags:
 - FAQ
 - FAQ_tech
---



بله. دستورالعمل دقیق چگونگی راه‌اندازی یک شبکه Deflect را می‌توانید در
صفحه [نصب Deflect](../Deflect_DIY) ببینید.
|چگونه می‌توانم شبکه ‌‌Deflect خودم را اجرا کنم

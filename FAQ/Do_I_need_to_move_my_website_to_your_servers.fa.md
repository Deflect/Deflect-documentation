---
layout: default
title: Do I need to move my website to your servers?
lang: fa
ref: Do I need to move my website to your servers
tags:
 - FAQ
 - FAQ_overview
---

- نه. Deflect یک ساختار زیربنایی تخفیف DDoS است نه یک میزبان وب‌سایت. برای
پیوستن به Deflect شما فقط کافی است رکوردهای DNS خود را تغییر دهید. وب سایت شما
روی میزبان اصلی خود باقی می‌ماند. اگر شما از میزبان فعلی خود راضی نیستید ما
می‌توانیم در مورد ارائه‌دهنده ‌های قابل اطمینان و با قیمت مناسب
به شما مشاوره بدهیم.
|آیا باید وب‌سایت خود را به سرورهای شما منتقل کنم

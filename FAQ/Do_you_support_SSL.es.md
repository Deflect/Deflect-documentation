---
layout: default
title: ¿Soportan SSL?
lang: es
ref: Do you support SSL
tags:
 - FAQ
 - FAQ_tech
---


¿Soportan SSL?
==============

Sí. Tendrá que compartir su certificado SSL y su fichero de clave existentes con
el equipo de Deflect, o puede comprar otro certificado SSL para compartir con
nosotros. Para información adicional, vea la página [Soporte
SSL](../TLS_Support).

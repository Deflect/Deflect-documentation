---
layout: default
title: I'm already paying for hosting, do I really need this?
lang: en
ref: Already paying for hosting
tags:
 - FAQ
 - FAQ_overview
---


I'm already paying for hosting, do I really need this?
======================================================

- Very likely. We provide our service on top of your hosting for free. It can
  reduce the cost of your hosting by greatly reducing the amount of traffic your
  host server has to deal with.  
- There are some dedicated hosts that advertise their DDoS preparedness but it
  can be an expensive and restrictive business.
- If your website runs on WordPress, we can also offer you free and secure
  hosting with eQPress. [Learn more about this
  option](https://equalit.ie/eqpress Learn more about this option).

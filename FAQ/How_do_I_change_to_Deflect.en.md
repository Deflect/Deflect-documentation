---
layout: default
title: How do I change to Deflect?
lang: en
ref: How do I change to Deflect
tags:
 - FAQ
 - FAQ_overview
---


How do I change to Deflect?
===========================

1. Check if you qualify for protection according to our [eligibility
   criteria](../Eligibility) and all other [requirements](../Assumptions) are
fulfilled.
2. Go to [the sign-up form](https://dashboard.deflect.ca/signup) and follow these [instructions](../Dashboard_Walkthrough/Walkthrough).
3. Change your DNS records to point to Deflect.

All things being equal, that's all it takes.

We also recommend websites to change their IP address after switching to
Deflect, because if there are no DNS records pointing directly to your server,
your new IP will never be revealed and its true location can be hidden from the
Internet and from potential attackers.

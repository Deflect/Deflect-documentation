---
layout: default
title: Do you support SSL?
lang: fa
ref: Do you support SSL
tags:
 - FAQ
 - FAQ_tech
---


بله. شما می‌توانید گواهی‌نامه SSL فعلی خود را به کار ببرید یا
می‌توانید گواهی‌نامه SLL جدیدی بخرید که با ما به اشتراک بگذارید. برای
اطلاعات بیشتر صفحه [پشتیبانی SSL](../TLS_Support) را ببینید. 
|آیا از SSL پشتیبانی می‌کنید

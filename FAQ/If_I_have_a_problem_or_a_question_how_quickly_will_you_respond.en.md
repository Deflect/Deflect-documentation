---
layout: default
title: If I have a problem or a question how quickly will you respond?
lang: en
ref: How quickly will you respond
tags:
 - FAQ
 - FAQ_overview
---


If I have a problem or a question how quickly will you respond?
===============================================================

- We treat support request and notices of incident with the highest priority in
  our daily work. Sometimes, serious incidents or a series of other support
  requests may make it difficult to process a request immediately. Our goal is
  to take not more than three hours to respond to a request.
- We are a non-profit group of committed techtivists living in different time
  zones, so chances are good that someone will be online to investigate your
  request more quickly than that. 
- It’s in our interests to provide a fast and efficient service because if one
  client is being DDoSed, it affects our whole infrastructure. 

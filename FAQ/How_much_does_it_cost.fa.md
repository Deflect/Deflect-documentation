---
layout: default
title: How much does it cost?
lang: fa
ref: How much does it cost
tags:
 - FAQ
 - FAQ_overview
---


-   ،سرویس Deflect برای تمامی وب‌سایت‌ها یا شبکه‌ء وب‌سایت‌های مربوط به
    سازمان‌های حقوق بشری، فعالان، وب‌لاگ نویسان منتقد یا مدیای مستقل
    مجانی می‌باشد.
-   خوش‌بختانه، هزینه‌های نگهداری از شبکه‌ها ؟؟ میزان کمی می‌باشد

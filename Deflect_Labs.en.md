---
layout: default
title: Deflect Labs
lang: en
ref: Deflect_Labs
tags:
 - Features
---

Deflect Labs
============

Deflect Labs is Deflect's project that leverages and enriches the large amount
of data available to the Deflect DDoS mitigation system to:

- provide sophisticated intelligence about the nature of DDoS attacks;
- profile attacks and create a taxonomy of attacks;
- inform efforts to mitigate, prevent and discuss attacks;
- develop visualization tools for data analytics to profile botnet activity
  history, DDoS attack metrics and forensics.


![Deflect Labs - Canaries](img/Canaries.png)

Deflect Labs publishes regular
[reports](https://equalit.ie/category/deflect-labs) on the most interesting
cases we observe:

- [**Deflect Labs Report #3**](https://equalit.ie/deflect-labs-report-3/) - On
  DDoS attacks launched against [Black Lives Matter's official
  website](http://blacklivesmatter.com/) between April 29th and October 15th,
  2016.
- [**Deflect Labs Report #2**](https://equalit.ie/deflect-labs-report-2/) - On
  DDoS attacks launched against the [BDS Movement's
  website](https://bdsmovement.net/) between February 1st and March 31st, 2016.
- [**Deflect Labs Report #1***](https://equalit.ie/deflect-labs-report-1/) - On
  DDoS attacks launched against Ukrainian media website
  [Kotsubynske](http://www.kotsubynske.com.ua/) in February 2016.

Deflect Labs' goal is to raise the costs for launching DDoS attacks by keeping
the Internet Freedom community informed about new and incoming cyber-threats and
by exposing adversary methods and identities, thus discouraging future attacks.

Deflect provides an excellent resource to study the characteristics and nature
of DDoS and other attacks on civil society websites. The network can be seen a
honeypot of sorts for collecting attack data, since a large number of
Deflect-protected sites are subject to frequent DDoS attack. The Deflect Labs
project aims to use this information to strip away some of the impunity
currently enjoyed by botnet operators and their benefactors, so as to raise the
costs for launching attacks against our clients and civil society in general.
While complete revelation of the identity of attackers is an impossible
technical challenge for us to undertake, Deflect Labs allows for better
attribution of attacks to specific hosts, botnets and causes. We believe this
research may help attack victims or third parties form opinions about the
provenance, or perhaps even the objectives, of attacks.

If your site is protected by Deflect and you would like to work with us on
researching attacks you have experienced, please contact us via the support form
in Deflect Dashboard.

### [Technical Details](Deflect_Labs_Tech)

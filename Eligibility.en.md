---
layout: default
title: Eligibility
lang: en
ref: Eligibility
tags:
 - Get started
---

Eligibility criteria
====================

Individuals and groups requiring Deflect protection should meet the
following criteria:

- You defend Human Rights; run a Civil Society Organisation; produce Independent
  Media; or work with people who do one of these.
- Your work does not contravene the [principles set out in the Universal
  Declaration of Human
  Rights](http://www.ohchr.org/EN/UDHR/Pages/Language.aspx?LangID=eng).
- Your work does not promote hate speech or encourage discrimination.
- You agree with our [Terms of Service](ToS).

---
layout: default
title: BotnetDBP - Banjax
lang: en
ref: BotnetDBP_Banjax
tags:
 - Deflect Tech
 - Deflect Labs
 - BotnetDBP
---

# Banjax


[![Banjax](img/GitHub-Mark-120px-plus.png)](https://github.com/equalitie/banjax)

[Banjax](https://github.com/equalitie/banjax) is responsible for early stage
filtering, challenging and banning of bots, identified via regular expression
(regex) matching, in conjunction with the Swabber module. 

Banjax allows for per site configuration of either SHA-inverse proof-of-work or
Captcha challenges. These challenges are served as a simple method to detect bot
requests that have not been intercepted by one of the system configured regexes.

The [Banjax](Banjax) module is integrated into the ATS proxy server working as a
filter which intercepts and analyses HTTP requests before any content is served.
This tool has a number of functional operations. It allows for the use of
regular expression (regex) banning, SHA-inverse challenging and generation of
Captchas. Additionally, it provides whitelisting capabilities to prevent banning
of allowed bots as well as legitimate automated requests. The three Banjax
functionalities are enabled as filters in Banjax.conf. Additionally Banjax can
gather and submit the detailed information on each request to Botbanger for
further analysis of the requester behavior.

## Regex detection

In the majority of large-scale attacks, clear and distinct patterns can be found
in each attacker's requests, allowing BotnetDBP to be fed with regular
expressions to find these patterns and ban any bot whose requests show up with
them at a given rate. The regex filter is basically imitating Fail2ban's
capabilities but with a greater level of efficiency needed for DDoS defence.
This is done before serving any such requests and has proven crucial when
particularly weak origins are under attack by a significantly large network of
bots.

The regex filters are defined as a series of rules indicating the circumstance
within which a given request should be banned. The parameters supported are: 

- Rule - is the human readable name for a given regex to be banned.
- Regex - contains the regular expression string that a request will be tested
  against
- Interval -  is the window of time between requests that Banjax should
  consider.
- Hits_per_interval - is the number of hits for a given time window that Banjax
  should consider before banning a given IP address. The actual banning is
  calculated as 1/1000 x hits_per_interval. This means that for a given
  interval, Banjax considers the number of requests per millisecond and when it
  crosses the allowed threshold, a ban is implemented. 

## Challenging the request

The caching proxy does not offer an effective measure to prevent cache-busting
attacks. In this type of attack, each bot requests a new, unique resource from
the network. Because each request is different from the previous one, there is
no copy of such a resource in the caching proxy so all the requests reach the
origin, effectively amplifying the DDoS attack by the number of proxy servers.
This strategy essentially turns the Deflect network against the very people we
are trying to help.

To prevent this, BotnetDBP can be configured to serve challenges to each
computer requesting the content, allowing only those which solve the challenge
successfully to proceed (automatic detection of cache-busting has been
implemented and will be deployed shortly). The Challenger filter supports two
methods for HTTP requests: SHA-inverse challenge and Captcha challenge. These
functions are primarily intended to mitigate cache-busting attacks but also
serve to ensure the legitimacy of the request and to provide a mechanism to slow
traffic during a heavy load.

- The SHA-inverse challenge asks the user browser for an inverse image of
  partial SHA256 value via Javascript. This ensures that traffic is originating
  from a legitimate browser rather than from a pre-programmed Bot. This
  challenge is seamless for the user as it occurs as part of the interaction
  between their browser and the server.
- By contrast, the Captcha is presented directly to the user, who must correctly
  solve the visual challenge and submit their answer before gaining access to
  the site's content.

Banjax users can configure Challenger by the following parameters:

- the number of times a requester can fail a challenge before they are banned
- the difficulty level (the time taken for the browser to solve the SHA256
  inverse problem)
- the length of time that a solved challenge can grant access to the requester

Additionally, there is a key which ensures that the cookies passed to and from
the user have not been tampered with or manipulated and the tool can also be
configured for multiple hostnames, allowing for different challenges to be set
per host.

## Automatic detection of bot behaviours 

Banjax gathers details of each request and submits them to Botbanger.  Botbanger
then computes statistical features from the browsing behaviour of each client
and compares the clients' behaviours against known botnet behaviour patterns.
Depending on the results of this comparison, either the client is immediately
banned or a challenge is presented to test their authenticity.

## White Listing 

The Banjax tool can also be configured to allow HTTP requests for specific IP
addresses via the White Lister in order to interact with the server without
interference.

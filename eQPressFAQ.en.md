---
layout: default
title: eQPress FAQ
lang: en
ref: eQPressFAQ
tags:
 - Features
 - Support
---


# eQPress FAQ

## What is eQPress?

eQPress is a secure, stable and user-friendly hosting infrastructure based on
the Wordpress blogging platform and protected by Deflect. 

## Who can have their website hosted on eQPress?

eQPress offers website hosting to anyone who already uses or wants to use
Wordpress for their website and qualifies for protection under the terms of
Deflect's [eligibility criteria](Eligibility).

## How can I create my site on eQPress?

If your existing website or the new website you want to create is
[eligible](Eligibility) under Deflect, you can ask Deflect's team for an eQPress
account.

To do so, you first need to register a domain (if you don't have one already)
and then [sign up with Deflect](https://dashboard.deflect.ca/signup). When the
registration is completed, you can ask Deflect's team to create a new site for
you or to migrate your site to eQPress.

## How can I migrate my Wordpress-based website to eQPress?

After you have [signed up with Deflect](https://dashboard.deflect.ca/signup),
you can migrate your site to eQPress by following [these
instructions](https://www.equalit.ie/migrating-your-wordpress-site/), or we can
do it for you. We will just need a MySQL database dump and the complete backup
of your Wordpress site. This will allow us to do any tuning of the site that may
be needed from the beginning.

## Can I get my own IP?

No. To mitigate the risk of DDoS attacks, eQPress has been created ad hoc to
protect your Wordpress-based website under the [Deflect
network](About_Deflect#Our_approach), so you cannot get your own IP.

## Is multisite supported?

Yes it is. eQPress supports both subdomains (sub.example.com) and subdirectories
(example.com/sub).

## What do you mean by secure?

Websites hosted by eQPress are secure because they are protected against DDoS by
[Deflect](About_Deflect), which also protects any possible backdoors from
attacks. Furthermore, the websites are hosted in hardened servers that are run
by an experienced team.

## Will my site be accessible on HTTPS?

HTTPS certificates can be added via the [Deflect
Dashboard](Dashboard_Walkthrough/Step3_TLS). By
following the procedure to install your TLS certificate, your website will be
accessible on HTTPS.

## Will my site be protected from DDoS attacks?

Yes, definitely! eQPress has been developed as a hosting platform protected by
Deflect, a distributed denial-of-service (DDoS) mitigation service created to
neutralize cyberattacks against independent media and human rights defenders.

## What about other attacks?

Deflect doesn't only mitigate the risk of DDoS, but also protects any backdoors
from possible attacks because attackers cannot reach your actual website and
will only have access to Deflect's caching servers.

## How often is my site backed up?

Websites hosted on eQPress are backed up every day, and the backup is encrypted
and stored on Deflect's servers. The system also has a mirror backup for
failover. This gives our team the ability to switch to this mirror server if we
have problems with the master eQPress server in the cluster. 

## What version of Wordpress will my website run on?

eQPress is based on the latest version of Wordpress and updates are carried out
regularly.

## When will my WordPress core get updated?

WordPress periodically releases maintenance updates. These are typically for
significant bug fixes or security issues. Since these upgrades might have
security implications, and because WordPress’ popularity makes it susceptible to
an exploit being quickly released, we attempt to apply these upgrades as quickly
as possible.

Our goal is to upgrade all websites within 6 hours from the release of a version
addressing security issues. All sites will be upgraded no later than 24 hours
from the time when the official announcement is made on the WordPress Project’s
News blog.

Major releases provided by WordPress can significantly affect its compatibility
with plugins and themes. Typically there are no security patches applied,
therefore the urgency to upgrade is lower. We will provide guidance via our
announce mailing list on how the upgrades will eventually be applied to all
websites hosted on the platform.

## Can I install any theme I like?

Yes, but if we find the theme to have a security vulnerability, we may decide to
block it.

## Can I install any plugin I like?

Yes, but if we find the plugin to have a security vulnerability or large
performance flaw, we may decide to block it.

## How do I get support?

The best way to submit a support issue is through the
[Deflect Dashboard](Dashboard_Walkthrough/Step5_Control_Panel#My_account). 


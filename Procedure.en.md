---
layout: default
title: Procedure
lang: en
ref: Procedure
tags:
 - Get started
---


Herein the few simple steps to join Deflect:

1. Sign-up on
   [https://dashboard.deflect.ca/signup](https://dashboard.deflect.ca/signup)
2. We will send you an email with temporary login details
3. Log-in and follow the prompts to set-up your site's DNS, security settings
   and SSL/TLS certificates if necessary. More info in the [Registration
   walkthrough](Dashboard_Walkthrough/Step0_Register)
4. We receive your application, approve it according to our guidelines and
   set-up your website on Deflect
5. You receive notification to change your name servers (NS) to Deflect
6. Point your NS at Deflect

That's it!

* * *

Potential partners in the project need to consider the following
technical details:

- Deflect protection is meant to be pro-active and implemented prior to a DDoS
  attack. You can switch during a DDoS attack but it will take us longer to set
  you up. Once you have switched to Deflect, your website will be cached across
  our infrastructure and Internet traffic will be directed towards it.
- You will need to point your DNS name server records to a Deflect host.  At any
  time you can opt out of Deflect by changing the DNS records to point back to
  the original name servers.
- Your website address will remain unchanged to the Internet. Your editorial
  staff will need to authenticate themselves with a password you define in the
  Deflect Dashboard.
- It is recommended that you change your server's IP address once behind
  Deflect.
- Deflect has [SSL/TLS Support](TLS_Support) for your website. You can ask us to
  create TLS certificates for you or you can share yours through the Dashboard.
- You may need to implement certain updates on your existing website platform
  prior to being accepted by the Deflect project. This will ensure that common
  attack loopholes are closed, making it less vulnerable to malicious hacking
  and DDoS.
- Because Deflect is a free service, we do not guarantee 24/7 support. However,
  with members based on 3 continents and working according to a monitoring
  schedule, someone is usually available to address any issues that may arise.

---
layout: default
title: Translate Deflect documentation
lang: en
ref: Translate
tags:
 - Community
---

Translate Deflect documentation
===============================

- [How this website works](#How)
- [How to translate Deflect documentation](#translate)
	- [Edit translations](#edit)
	- [Add translations](#add)
	- [Links](#links)


We welcome anybody with the time and talent to lend a hand translating
this documentation into other languages. Write to info @ deflect . ca
if you'd like to help out.

If you wish to contribute to this website by editing or adding to its
resources, please read [these instructions](contribute).

<a name="How"></a>
## How this website works

The Deflect documentation website is generated automatically from a
[git](https://en.wikipedia.org/wiki/Git) repository hosted on
[Riseup](https://riseup.net)'s [Gitlab
instance](https://0xacab.org/),
[here](https://0xacab.org/Deflect/Deflect-documentation).

All text files are formatted in
[markdown](https://en.wikipedia.org/wiki/Markdown), a lightweight markup
language that you can use to format the text. The syntax of markdown is
easy to use and you can find the references you need
[here](https://daringfireball.net/projects/markdown/).

To translate Deflect documentation, the first thing you need to do
is [create an account](https://0xacab.org/users/sign_in).

Once you have created an account, you can:

* [Edit the repository in your own branch](contribute#merge_requests) and submit a
  merge request (the safest solution - read more about merge requests
  [here](https://docs.gitlab.com/ee/user/project/merge_requests/index.html)).
* [Become a member of the project and edit the master repository
  directly](contribute#edit_directly). In this case all your edits will be
  deployed to the website within few minutes.

Please, read the documentation on how to add or edit files
[here](contribute#Add_Edit).

<a name="translate"></a>
## How to translate Deflect documentation

Once you have created an account and created your own branch or become a
member of the Deflect documentation project, you can edit or add
translated files to the repository.

<a name="edit"></a>
### Edit translations

The repository containing all Deflect documentation includes some
Spanish, Farsi, and French translations. You can identify these
translated files from the language code in the extension of the file
names - "**.es**.md" for Spanish, "**.fa**.md" for Farsi, and
"**.fr**.md" for French. Over time, other languages may be added.
Please, go to this [list of standard language
codes](https://www.w3schools.com/tags/ref_language_codes.asp) to
interpret each code in the file name extension.

If you would like to change one of the existing translations, you can
follow [these instructions for editing files](contribute#Edit_files) in
Deflect documentation website.

<a name="add"></a>
### Add translations

If you want to add a new translation to Deflect documentation, you will
need to [create a new file](contribute#Add_New_File) that has the same
name as the original text file, but an extension that contains the
[standard code for the target
language](https://www.w3schools.com/tags/ref_language_codes.asp). If,
for example, you want to translate this file ("translate.en.md") into
Chinese, you will have to create a new file called "translate.zh.md".

The new file should include the original [YAML Front
matter](contribute#Front_matter) (which you can't see in the website, but will
find at the beginning of each file within the repository). This Front matter 
includes meta tags identifying each document, and you will need to edit just 2 
of them:

- **title**: in your translation, you will have to change the "title"
  value (in this file: "title: Translate Deflect documentation") into a
  localized title (if translating, e.g., into French, you would have:
  "title: Traduisez la documentation de Deflect").
- **lang**: this meta tag corresponds to the language used in your
  translation. This field should contain the same [standard language
  code](https://www.w3schools.com/tags/ref_language_codes.asp) you'll
  use in the file name extension.

Finally, please note that all text files of Deflect documentation are
written in markdown, a very easy to use markup language that allows you
to format your text adding \*\***bold**\*\* or \**italics*\* fonts,
\[hyperlinks\]\(URL\) or \!\[\]\(images\). You can find a basic introduction to
markdown [here](https://daringfireball.net/projects/markdown/basics).

<a name="links"></a>
### Links

If the text you're translating contains a link to an external page - in
raw text format the structure will be \[text\]\(http://example.com\) -
such as a Wikipedia page, please find the equivalent in your language
and replace the URL accordingly. If the text links to a resource
included in this website, you don't need to replace it: if the
translated file exists, it will open automatically in browsers where
that language is set as default. If the translated file does not exist, we
would appreciate it if you could add it of course! :)

---
layout: default
title: Sur Deflect
lang: fr
ref: About_Deflect
tags:
 - Features
---

Sur Deflect
=============

Deflect est un service [atténuation de
DDoS](http://fr.wikipedia.org/wiki/DDoS#Les_protections_contre_les_attaques_de_d.C3.A9ni_de_service)
utilisé par les ONG, les groupes de
la société civile, les blogueurs activistes et les médias indépendants pour
limiter les attaques par déni de service (DDos). Élaboré par l’organisation à but
non lucratif pour la sécurité numérique [eQualit.ie](https://equalit.ie), le service
est gratuit, code source ouvert, et apporte une solution libre et efficace pour neutraliser
les attaques [par déni de
service](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service).


Historique
----------

La plupart des groupes luttant pour les droits de l’homme et des groupes de
médias indépendants n’ont pas les ressources financières et techniques
nécessaires pour limiter les attaques DDoS. C’est pourquoi Deflect a été créé
pour fournir ce service gratuitement. Ces attaques, entreprises par des [« bots »
malveillants](https://fr.wikipedia.org/wiki/Bot_informatique#Utilisation_malveillante),
peuvent désactiver le site Web ciblé et empêcher l’accès aux
utilisateurs légitimes, les propriétaires des sites sont alors intimidés et la
liberté d’expression sur Internet étouffée. Les services commerciaux utilisés
pour limiter les DDoS sont coûteux et leurs conditions d’utilisation peuvent
être modifiées si un site Web particulier attire un trafic très dense. Beaucoup
de nos partenaires se sont adressés à nous après avoir connu une expérience
décevante auprès d’un service commercial connu.


Notre approche
--------------

Nous mettons en place des [caches de proxy
inverse](http://fr.wikipedia.org/wiki/Proxy_inverse) sur un ensemble de
fournisseurs d’hébergement à bas prix répartis géographiquement. Bien que tous
les hébergeurs se valent d’un point de vue fonctionnel, nous cherchons à savoir
quels fournisseurs assurent la meilleure qualité. Les services Deflect utilisent
un serveur DNS [à courte durée de
vie](http://fr.wikipedia.org/wiki/Time_to_Live#Le_Time_to_Live_dans_le_DNS),
la mise en cache distribuée, la liste
noire des adresses IP et d’autres méthodes identifiées afin de satisfaire de
nombreux clients simultanément, à bas coût pour nous et gratuitement pour eux.


Création
--------

Deflect a été pensé comme un système solide à faible coût, commun et facilement
reproductible pour assurer la protection de plusieurs sites Web, que nous
appelons également « Origins ».

Le système a été créé pour rester neutre face aux différents serveurs Web, avec
quelques restrictions détaillées ci-dessous. Il est construit à partir des
serveurs VPS Debian 6, aussi appelés « Edges » (« bords »), et d’un serveur de
contrôle que nous appelons « Controller ». Le composant de mise en cache est
géré par [Apache Traffic Server](http://trafficserver.apache.org/).


Protection offerte
------------------


* Absorption de plus de 99 % du trafic destiné à votre site Web. Découvrez quelques statistiques [ici](https://www.deflect.ca/stats/).
* Adresse du serveur (adresse IP) masquée
* Accès interdit au public pour les tableaux d’édition (par exemple, /admin, /login, etc.)
* Requêtes malveillantes filtrées grâce à Fail2ban, Learn2ban et l’ensemble des règles Iptables


Deflect en action
-----------------

Pour avoir accès à un site Web protégé par Deflect :

1. Entrez l’adresse du site Web dans le navigateur.
2. Le serveur DNS récupère un nom d’emprunt enregistré dans notre base de bords. Un de ces bords est ensuite sélectionné en utilisant une pétition DNS.
3. Si l’adresse demandée est autorisée et que le bord affiche le contenu de la page dans son cache, le serveur répond immédiatement au navigateur. Si le contenu n’est pas répertorié dans le cache du bord, il est demandé à la source puis envoyé au navigateur.
4. Si l’adresse n’est pas autorisée, une page d’avertissement s’affiche.

L’image ci-dessous en donne une explication simple :

[![](/img/Deflect-fr.png)](/img/Deflect-fr.png)


Détails et restrictions
-----------------------

### Composants cachés

Deflect gère des pages Web constituées de nombreux éléments, y compris le CSS,
le Javascript, les fichiers multimédias et les fichiers binaires importants. Les
composants de la page qui sont hébergés sur des domaines différents (les «
widgets », les traqueurs de trafic, etc.) sont traités de manière normale.

Deflect met actuellement en cache les réponses pour 10 m, qui peuvent être
réglées pour des emplacements spécifiques (par exemple, plus long pour les
fichiers binaires qui changent rarement, plus court pour les forums en ligne).


### Cookies

Bien que Deflect ignore actuellement les cookies, renvoyant le même objet du
cache indépendamment de la présence de cookies à la demande du client, il peut
être configuré sur la base d’un domaine ou d’un chemin d’accès. Nous pouvons
permettre un traitement unique des différents cookies, cependant nous
désactivons la mise en cached’un site ou une partie de ce dernier. Néanmoins, le
site sera toujours protégé par notre analyse de pare-feu. Les chaînes de requête
sont traitées comme des parties de l’URL, cela signifie que les différentes
chaînes de requête sont toujours considérées comme des objets uniques et ainsi
mises en cache.Les réponses aux requêtes POST ne sont jamais mises en cache.


### Est-ce que cela fonctionne ?

On peut voir que Deflect analyse une page en regardant les en-têtes HTTP (en
utilisant la fonction « Inspecter l’élément » sur Chrome ou l’extension Firebug
sur Firefox).Vous verrez un message Via : chaîne qui renvoie un bord spécifique
au service de la page Web demandée. Ce message ressemble à celui-ci :
Via:http/1.1 prometeus1.deflect.ca (ApacheTrafficServer/3.2.4 [uIcMsSfWpNeN:t
cCMi p sS]) La réponse de mise en cache, dans le cas ci-dessus [uIcMsSfWpNeN:t
cCMi p sS] peut être interprétée
[ici](http://traficserver.apache.org/tools/via.html).


### SSL/TLS

Deflect supporte également le protocole SSL. Pour plus d’informations, voir
[SSL/TLS Support](TLS_Support).


### DNS

Le serveur DNS est configuré pour une courte durée de vie pour permettre un
ajout/une suppression rapide des nœuds dans la base des bords.

Si vous avez d’autres questions, veuillez consulter notre
[FAQ](FAQ) ou envoyez-nous un [email](mailto:info@equalit.ie).
Nous ferons de notre mieux pour y répondre.


### Personnalisation du service Deflect

Au fil du temps, nous développons des profils pour les différents serveurs Web.
En attendant, nous proposons la personnalisation pour les cas suivants :

1. Les domaines et leurs noms d’emprunt (www.yoursite.org, yoursite.org)
2. Les caches à « durée de vie »
3. Les emplacements protégés (/admin)


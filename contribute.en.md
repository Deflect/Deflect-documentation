---
layout: default
title: Contribute to Deflect documentation
lang: en
ref: Contribute
tags:
 - Community
---

Contribute to Deflect documentation
===================================

- [How to contribute](#How)
    - [Edit your own branch (safe mode)](#merge_requests)
    - [Contribute directly to the documentation](#edit_directly)
- [Add or edit files](#Add_Edit)
    - [Add a new file or directory](#Add_New_File)
    - [Edit files](#Edit_files)
    - [Add or edit files by command line](#Command_line)
- [Important notes](#Important)
    - [Markdown](#Markdown)
    - [Front matter](#Front_matter)
    - [File extension](#File_extension)


This website aims to share collected experience and provide directions to
Deflect users and to sys admins who wish to install their own Deflect
instances on their server.

It is a work in progress and we welcome contributions. Please, read this
guide to learn how to contribute to this website.

If you wish to translate this website into a different language, read
[these instructions](translate).

<a name="How"></a>
## How to contribute

The Deflect documentation website is generated automatically from a
[git](https://en.wikipedia.org/wiki/Git) repository hosted on
[Riseup](https://riseup.net)'s [Gitlab
instance](https://0xacab.org/),
[here](https://0xacab.org/Deflect/Deflect-documentation).

All text files are formatted in
[markdown](https://en.wikipedia.org/wiki/Markdown), a lightweight markup
language that you can use to format the text. The syntax of markdown is
easy to use and you can find the references you need
[here](https://daringfireball.net/projects/markdown/).

To contribute to Deflect documentation, the first thing you need to do
is [create an account](https://0xacab.org/users/sign_in).

Once you have created an account, you can:

* [Edit the repository in your own branch](#merge_requests) and submit a
  merge request (the safest solution - read more about merge requests
  [here](https://docs.gitlab.com/ee/user/project/merge_requests/index.html)).
* [Become a member of the project and edit the master repository
  directly](#edit_directly). In this case all your edits will be
  deployed to the website within few minutes.

<a name="merge_requests"></a>
### Edit your own branch (safe mode)

If you wish us to check your contributions before they go live on the
Deflect documentation website, the best solution is to edit the
repository in your own branch.

To do so, you just need to go to the [master
repository](https://0xacab.org/Deflect/Deflect-documentation), click
on the "+" icon next to the name of the project, and then click on "New
branch" in the dropdown menu.

![Add menu](img/contribute3.png)

A form will appear to add your new branch: enter the name you prefer
(for example `my-deflect-branch`) and then click on the "Create branch"
button.

![Add new branch](img/contribute5.png)

When your branch has been created, you can edit it as you wish. When
you're done, submit your merge request to the master repository by going
to the main project or to the "Merge requests" menu and clicking on the
"Create Merge Request" button.

![New merge request](img/contribute4.png)

You can always switch to your branch from the dropdown menu above the
list of files in the project, next to the title.

![Branch dropdown menu](img/contribute2.png)

<a name="edit_directly"></a>
### Contribute directly to the documentation

If you are sure of what you are doing and wish to contribute directly to
the documentation, you can ask us to become a member of the
documentation project by clicking on the "Request access" button at the
right end of the Project menu.

![Request access](img/contribute1.png)

Since what we can see is only the nickname with which you registered,
before you request access it's a good idea to write to us at
deflect @ equalit . ie to explain who you are and what you would like to
contribute with.

Once we have accepted your access request, you will be able to edit the
repository directly, and all your changes will be deployed to the
Deflect documentation website within few minutes.

<a name="Add_Edit"></a>
## Add or edit files
<a name="Add_New_File"></a>
### Add a new file or directory

To add a new file or directory to the project,  go to the [master
repository](https://0xacab.org/Deflect/Deflect-documentation) and click
on the "+" icon next to the name of the project to open a dropdown menu
where you can choose among three options:

- **New file** - By selecting this option, you will be able to add a new
  file by editing it in the web interface.
- **Upload file** - This option allows you to upload a file that you
  have already created in your local computer. This is the option you need to
  select, for example, if you want to add images or PDF files
- **New directory** - By clicking on this option, you can add a new
  directory to the repository.

![Add menu](img/contribute3.png)

<a name="Edit_files"></a>
### Edit files

To edit an existing file, just select it in the file list to view the
text and then click on the "Edit" button in the right-top corner. An
editing interface will open where you can change both the title of the
file and the text.

![Edit file](img/contribute6.png)

When you're done, click on the "Commit changes" button in the bottom
left corner of the window.

<a name="Command_line"></a>
### Add or edit files by command line

If you know how to use git from a [command
line](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html),
you can just clone the repository to your local computer and then commit
your changes by submitting a [pull
request](https://git-scm.com/docs/git-request-pull) or by
[pushing](https://git-scm.com/docs/git-push) directly your changes to
the master repository if you're a member of the project.

<a name="Important"></a>
## Important notes
<a name="Markdown"></a>
### Markdown

All text files of Deflect's documentation are written in markdown. Please
read the basics of
[markdown](https://daringfireball.net/projects/markdown/basics) before
you start contributing to this project.

<a name="Front_matter"></a>
### Front matter

In each document, there is a [YAML front
matter](https://jekyllrb.com/docs/frontmatter/) before the body of the
text. This includes meta tags that are useful for the deployment of the
repository to the website.

The used meta tags, included within 2 lines with 3 dashes, are:

- **layout** - this should remain unchanged unless you want to change
  the template of your document
- **title** - this title will appear in the frame of your browser and
  should be as clear as possible. This may be the only meta tag you will need
  to change.
- **lang** - this meta tag corresponds to the language used in the body
  of the text. Please, use a [standard language
  code](https://www.w3schools.com/tags/ref_language_codes.asp), like
  "en" for English or "ar" for Arabic.
- **date** - only used in the News folder to indicate the date when the
  piece of news was released.
- **ref** - the unique name of a document across languages: it should
  remain unchanged in the localized versions of the same document. 
- **tags** - tags identifying the topic of the text, for example
  "Features" or "Deflect News".

*Important: Please, leave the lines with the dashes unchanged, as they are
necessary for the site generator to do its job correctly.*

<a name="File_extension"></a>
### File extension

Every text file in the repository ends with a language code followed by
the `.md` extension, which is used for markdown files. Please add the
corresponding [standard language
code](https://www.w3schools.com/tags/ref_language_codes.asp) to your
file (for example "filename**.en**.md" if your file is written in
English).
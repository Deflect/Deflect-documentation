---
title: Deflect Resources 
layout: default
lang: en
ref: Home
---

![Deflect logo](img/Deflectmw.png)

- Get started
	- [What you need to start](Assumptions)
	- [How it works](Procedure)
	- [Terms of Service](ToS)
- Get help
	- [Step-by-Step Walkthrough](Dashboard_Walkthrough/Walkthrough)
	- [Hosting with eQPress](eQPress)
	- [Secure connections](TLS_Support)
- [Features]() #this should be a link to the "Why Deflect" section of
  this page
- [FAQ](FAQ) #This should be an index generated from the "FAQ" tag
  contained in the Front Matter of the relevant files
- Search #This should be the search engine Donncha will install

* * *

## [Terms of Service](ToS)
## [Deflect DIY](Deflect_DIY) 
 #This should also be an index generated from metatags

<iframe frameborder="0" height="300" src="https://player.vimeo.com/video/93531470?title=0&amp;byline=0&amp;portrait=0" webkitallowfullscreen="" width="400"></iframe>

## [Testimonials](Testimonials)
## [In the media](Deflect_in_the_Media)

* * * 

## Why Deflect?

 # this should be identical in the front page
- [Hosting](eQPress)
- [Analytics](https://equalit.ie/deflect-labs-reporting/)
- [Encrypted connections](TLS_Support)
- [Human support](Support)
- [Advanced mitigation](About_Deflect)
- [Control panel](Dashboard_Walkthrough/Walkthrough)

* * *

## How we protect you

 # this should be identical in the front page, just as the excerpt from
 Deflect Labs report, the "Get protected, stay connected" section and
 the footer.

## Get protected, stay connected

1. [Sign up](https://dashboard.deflect.ca/signup)
2. [Set up](Dashboard_Walkthrough/Step01_Start_Setup)
3. [Change DNS and go!](Dashboard_Walkthrough/Step4_Finalize)

 # links in the footer:

- Documentation #link to this index
- [Source code](Deflect_DIY/Installing_Deflect)
- [Terms of Service](ToS)
- [Contact us](info@deflect.ca)
- [Translations](translate)
- [Contribute to Deflect's documentation](contribute)

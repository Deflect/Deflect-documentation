---
layout: default
title: Social Engineering attacks on Deflect
lang: en
date: 2013-02-28
ref: Social_Engineering_attacks_on_Deflect
tags:
 - Deflect News
external_URL: http://forum.tntvillage.scambioetico.org/index.php?showtopic=257654
---



Social Engineering attacks on Deflect
=====================================

Summary
-------

We received two different yet pretty much simultaneous take-down notices from three of our server providers on Friday. One was a spoofed DMCA notice that we quickly managed to resolve (notes attached). The other was a complaint submitted to a European CERT claiming that we were operating a botnet (fast-fluxing like techniques were used as evidence of this). This latter request caused a Spanish CERT to issue some of our edge server providers with takedown notices. Afraid of an up-stream reaction, two of the three providers shut-down our servers before notifying us of the lodged complaint. Albeit there was no immediate technical connection between the DMCA and fast-flux complaints, they happened at exactly the same time and were both referencing one of the highly targeted Chinese human rights websites currently on Deflect. All issues were quickly resolved with CERT and the hosting providers, albeit one of them decided to discontinue our service nonetheless. 


Hackers target Deflect hosts
----------------------------

\*\*\* Sent via Email - DMCA Notice of Copyright Infringement \*\*\*

Dear Sir/Madam,

I certify under penalty of perjury, that I am an agent authorized to
act on behalf of the owner of the intellectual property rights and
that the information contained in this notice is accurate.

I have a good faith belief that the page or material listed below is
not authorized by law for use by the individual(s) associated with
the identified page listed below or their agents and therefore
infringes the copyright owner's rights.

THE INFRINGING PAGE/MATERIAL IS INDEXED AND PRESENT IN YOUR SEARCH
ENGINE AND I HEREBY DEMAND THAT YOU ACT EXPEDITIOUSLY TO REMOVE THE
PAGE FROM YOUR INDEX.

This notice is sent pursuant to the Digital Millennium Copyright Act
(DMCA), the European Union's Directive on the Harmonisation of
Certain Aspects of Copyright and Related Rights in the Information
Society (2001/29/EC), and/or other laws and regulations relevant in
European Union member states or other jurisdictions.

My contact information is as follows:

Organization name: Attributor Corporation as agent for Rights Holders
listed below
Email: counter-notice@attributor.com
Phone: 650-340-9601
Mailing address:
119 South B Street
Suite A,
San Mateo, CA 94401

My electronic signature follows:
Sincerely,
/Eraj Siddiqui/
Eraj Siddiqui
Attributor, Inc.


\*\*\* INFRINGING PAGE OR MATERIAL \*\*\*

Infringing page/material that I demand be disabled or removed in
consideration of the above:

Rights Holder: RCS

Original Work: I promessi sposi
Infringing URL:
http://forum.tntvillage.scambioetico.org/index.php?showtopic=257654

----- Forwarded Message -----
From: "INTECO-CERT [fastflux]" <incidencias@cert.inteco.es>
To: \*\*\*
Cc: sitic-reports@sitic.se
Sent: Saturday, February 23, 2013 9:02:07 AM
Subject: \[INTECO-CERT \#\#\#\] [Fast-Flux] INCIDENT REPORT 

	**********************************************************************
The INTECO-CERT (Computer Emergency Response Team ICT Security), supports the
development of national business network and offers the current services of an
Incident Response Centre, providing reactive solutions to computer incidents,
prevention services against possible threats and services of information
awareness and training in computer security matters to Spanish companies and
citizens.
	**********************************************************************

Dear Team,

INTECO-CERT has detected some domain names using Fast-FLux techniques
(http://en.wikipedia.org/wiki/Fast_flux) and we detect some IP addresses under
your constituency, likely to be members of a botnet. This information is
collected by DNS resolution.

Some of those domains may not resolve, because the files are generated daily
with information gathered in last 24 hours.

File Format:

2013-02-22 19:32:51	5.34.241.230	5.34.241.0/24	website.org	EU
Ripe	57858	FIBERGRID Fiber Grid OU

	**Timestamp format is dd/mm/yyyy hh:mm:ss CET/CEST**

As this information is collected from public services, you can share it with
other involved entities (like ISPs, other enterprises or CERTs).

Thank you for your cooperation to prevent and terminate this kind of activities.

You can contact us if you detect any fraudulent activity under a .es domain or
related with spanish resources, and we would try to help you to solve it.

Please include the string [INTECO-CERT ####] in the subject line of all future
correspondence about this issue. To do so, you may reply to this message.

Best Regards,

	-------------------------------------------------------------------------

INTECO-CERT
incidencias@cert.inteco.es - http://cert.inteco.es
PGP Key: http://cert.inteco.es/About/PGP_Public_keys/
National Institute of Communication Technologies

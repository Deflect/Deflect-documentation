---
layout: default
title: October - Ten Days that Shook the World of Botnets
lang: en
date: 2013-01-11
ref: October_Ten_Days_that_Shook_the_World_of_Botnets
tags:
 - Deflect News
---


October: Ten Days that Shook the World (of Botnets)
===================================================

On October 21st, Deflect started protecting a website which reports on
corruption and criminality at the top levels in some of the world's most famous
transnational bodies. The site, the work of a very dedicated British
investigative journalist, had been knocked offline several times since August
and on one occasion had also brought down its ISP.

When we put it onto the network, the first malicious traffic we received was a
volley of POST attacks(1) sent at an incredibly fast rate. The curious
thing was that all the requests made were for a particular page on the site,
perhaps sending a message to the site owners that this particular news story was
the reason for the trouble.
 
When we started blocking the POSTs, they redirected the attacks to an address on
the website that doesn't actually exist, another way of tying up server
resources. However, due to our caching strategy, we were able to serve the
redundant requests without any trouble. Concurrently, the attackers also sent a
large number of GET(2) requests to the root of the site and to some of
its sub-pages. 

The attack began on the 22nd of October, peaked on the 24th and ended more or
less by the 30th of October. Because of the tremendous scale of the attack, we
deployed Banjax and Swabber, two key elements from our new banning tool,
[DBP](../BotnetDBP_Intro). This was done late on the 22nd and then
further optimised on the 23rd. In so doing, the software quickly and capably
blocked the vast majority of attackers. The graph below represents the traffic
on just one server on the Deflect network (Week 41 corresponds to the dates
above).

![October13 attack](../img/Selection_003.png)

Throughout the attack, high loads were noticed on all proxy hosts. This was
later discovered to be the result of a SlowLoris(3)-style slow socket
attack, at first on HTTP and once this was somewhat mitigated, on HTTPS.
Initially these attacks were actually halted by the sheer volume of GET
requests, so in an attempt to evade detection a lot of the bots cycled user
agents(4) many times a second. However, many of these agents were
easily detected and so could be pinpointed by their fraudulent identifiers. Many
of these identifiers were themselves erroneous, unusual or completely unknown. 

For example, we received requests from these browsers:

	"Mozilla/5.0 (compatible; AhrefsBot/4.0; +http://ahrefs.com/robot/)"
	
	"Opera/9.80 (Windows NT 5.1; WOW64; U; Edition Ukraine Local; ru) Presto/2.10.289 Version/11.09"

	"Opera/9.63 (X11; FreeBSD 7.1-RELEASE i386; U; en) Presto/2.1.1"

Many of these user agents are very close to being valid but each of them have
some fundamental error. For example, the very common Opera 9.80 pattern
displayed above seems valid, but Opera have never released a version with
?Edition $country Local? in the string.

Despite the large volume of traffic, the site remained available almost entirely
throughout the attack. Because data from the whole network is too large to
process, the statistics below come from the first ten days of operation on just
one of our hosts and show the number of requests blocked by our anti-bot
systems. These do not include the number of requests from IP addresses we had
already blocked.

Requests blocked by anti-bot systems(5): 5307248

Overall bot count: 10259

Unique bot counts:

- 21/10 - 312
- 22/10 - 2130
- 23/10 - 664
- 24/10 - 4208
- 25/10 - 1848
- 26/10 - 1039
- 27/10 - 541
- 28/10 - 619
- 29/10 - 786
- 30/10 - 262

The data requested over the same period was in the region of 300GB, peaking on
the 22nd with 99GB in a single day. 

* * *

(1) *This has a similar effect as putting a tractor on a one-line highway:
traffic slows way down as everyone gets stuck behind it*

(2) *These are the most common requests made to a webserver, such as requests
for pages, images and so on*

(3) *A Slow Loris attack is the equivalent of telling someone to call you
because you have important information and then not picking up the phone when
they ring. You can keep asking and they'll keep calling, unable to move on to
other tasks*

(4) *The User Agent in this case is the browser. Cycling through user agents is
like trying to pay for something with dozens of credit cards – some genuine,
some fake - in the hope that security controls are lax. However in this case,
the attackers pretending to use a wide variety of different browsers and browser
editions in order to trick the server into thinking they are different visitors
for each request*

(5) *not counting requests not loaded due to IP-level blocking*

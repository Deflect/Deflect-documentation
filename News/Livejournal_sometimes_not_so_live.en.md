---
layout: default
title: Livejournal sometimes not so live
lang: en
date: 2013-01-11
ref: Livejournal_sometimes_not_so_live
tags:
 - Deflect News
external_URL: https://en.wikipedia.org/wiki/LiveJournal#Distributed_Denial_of_Service_Attacks
summary: Livejournal was down on NYE. This is far from the first suspected attack
---


Livejournal sometimes not so live
=================================

*LiveJournal Status*

*LiveJournal administrators are aware of current site access issues, and
are working to resolve them as quickly as possible. We appreciate your
patience while we work to restore access to the service*

*Status Updated: 3:26 pm GMT (Monday, December 31)*

They've not confirmed the nature of the fault but 3 times last year the
Russian-owned site was widely believed to have been DDOS'd by the
Russian Government, leading Medvedev to condemn the attacks:

"As an active user of LiveJournal I consider these actions outrageous
and illegal. What has occurred should be examined by LiveJournal's
administration and law enforcement agencies."

Perhaps he should have asked his Prime Minister?


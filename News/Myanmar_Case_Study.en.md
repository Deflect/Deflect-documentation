---
layout: default
title: Myanmar Case Study
lang: en
date: 2013-04-23
ref: Myanmar_Case_Study
tags:
 - Deflect News
---


Myanmar Case Study
==================

As part of its ongoing research into DDoS mitigation, the Deflect team is
sharing some of its findings and recommendations from a recent investigation
into independent media websites in Myanmar. The effort was motivated in part by
the wide disparity between official and independent news sources in the country,
particularly at a time of sectarian violence in Rakhine and heightened conflict
in Kachin, as well as the team's ongoing interest in studying cyber-attacks and
improving the project's mitigation strategies.

Our initial research discovered a number of websites and facebook groups
advertising them, containing a PHP script or embedded Javascript, called by the
/ddos.php URL after the domain name. The session would then initiate a DoS
attack targeting a particular Burmese independent news service. Four of the
websites in question (who may have been victim of script injections) carry
Myanmar relevant content and are registered, built and hosted by
[WebDesignDev](http://www.webdesigndev.com) on
[Softlayer](http://www.softlayer.com) servers.

- [Marvellous Memory Tour](http://www.marvellousmemorytour.com/) 69.93.224.39
  targeting [Irrawaddy](http://irrawaddy.org) 
- [Myanmar Exotic Travel](http://www.myanmarexotictravel.com/) 74.52.200.163
  targeting [first-11.com](http://first-11.com)
- [Myanmar’s Embassy in Australia](http://www.mecanberra.org/) 174.120.23.67
  targeting [first-11.com](http://first-11.com)
- [Maihsoongtravel](http://www.maihsoongtravel.com/) 174.120.23.92 targeting
  [first-11.com](http://first-11.com), also previously defaced

Other websites had a re-worked implementation of the Low Orbit Ion Cannon
allowing users to orchestrate an attack on a chosen target. An Italian
[auto-repair consortium
website](http://www.autoriparatori.org/news/immagini/1/ddos.php) has been one of
the victims (be careful opening that link, it will begin a DDoS attack). 

Sites targeted by these scripts are the leading independent news services in
Burma: [Eleven Media Group](http://elevenmyanmar.com/), [the Voice
Weekly](http://thevoicemyanmar.com/), [Irrawaddy](http://www.irrawaddy.org/),
[Mizzima](http://www.mizzima.com/), [Kachin News](http://www.kachinnews.com/),
[Kachinnet](http://www.kachinnet.net/), [Kachin Land
News](http://kachinlandnews.com/) (down) and
[Phophtaw](http://www.phophtaw.org/) (down). All hacked websites and their
current targets have been notified by eQualit.ie

The wider issue here is that vulnerable websites with unpatched software and
unmaintained systems are the low-hanging fruit for these attackers to host
malicious code on. Is the Myanmar embassy in Australia aware of their site and
network resources being used in this manner? Although it may be quite
complicated and costly to maintain a well managed online presence, complacency
is no longer an option. On the other side are the websites under attack who need
to take extra care to mitigate this new type of online oppression (or
expression). During this transitional phase in Myanmar’s political, media and
internet environments, it’s clear that independent news groups need to protect
themselves with DDoS–mitigation, while webmasters everywhere should regularly
check sites for invasive scripts.

The strategy of participatory DDoS originated in the late 90’s with the
[Zapatista Floodnet](http://www.thing.net/~rdom/ecd/ZapTact.html) and the WTO
protests, although in this case the [hacker
group](http://www.blinkhackergroup.org/) claiming responsibility for these
attacks seems to be motivated by pro-government, nationalist, anti-Rohingya
sentiments, despite their stated apolitical stance and the equally strong
anti-Rohingya position taken by some of their targets.

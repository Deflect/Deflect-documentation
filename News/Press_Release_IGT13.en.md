---
layout: default
title: Press Release IGT 2013
lang: en
date: 2013-10-15
ref: Press_Release_IGT_2013
tags:
 - Deflect News
---


eQualit.ie, the innovative digital security non-profit from Montreal, will be attending the annual Internet Governance Forum in Indonesia this week and is proud to be invited by the Association for Progressive Communications (APC) and Tactical Technology Collective (TTC) to present a brief introduction to Deflect, the open source DDoS-mitigation project created for Human Rights Defenders, civil society groups and independent journalists.  
# Press Release IGT 2013

**15/10/13**

***For Immediate Release:***

[eQualit.ie](https://equalit.ie), the innovative digital security non-profit from
Montreal, will be attending the annual [Internet
Governance Forum](http://www.intgovforum.org/) in Indonesia this week and is proud to be invited by the
[Association for Progressive Communications (APC)](http://www.apc.org/) and
[Tactical Technology Collective (TTC)](https://www.tacticaltech.org/) to present
a brief introduction to [Deflect](https://deflect.ca), the open source
DDoS-mitigation project created for Human Rights Defenders, civil society groups
and independent journalists. 

DDoS attacks on these groups have increased year on year and are predicted to
rise again in 2014, while a related surge in attacks on banks, businesses,
governments and gamers has caused commercial mitigation options to rise in cost
accordingly. Deflect was created to redress this balance, ensuring that HRDs,
civil society groups and journalists without financial or technical resources
can protect and maintain their websites' presence online. 

Meanwhile, in light of the world's recent awakening to global internet
surveillance, the 2013 IGF will be a particularly key event for discussing the
intersection of digital security with human rights such as freedom of
expression, association and the right to privacy.

On the night before the global IGF in Bali, 21 October 2013, APC and TTC are
hosting a peer-learning event on human rights to privacy and anonymity and what
solutions exist to protect those rights.

“We're calling it the Disco-tech,” said TTC organiser Gillo Cutrupi, “because
the format of the event will be very unique. Participants can learn about
technological solutions in an inspiring and relaxed yet high-energy atmosphere.”
Disco-tech is designed to raise awareness of online security (and insecurity) in
a comfortable and stimulating peer-learning environment. A series of five to six
short "lightning talks" will be presented by experts and activists covering
tools and experiences with online surveillance, safety, security from the
perspective of the rights to online privacy and anonymity. Participants will
have the opportunity to set up stations to share technological tools,
information or discuss specific issues. A “key signing party” and an
introduction to encryption will be held in parallel.

The evening's discussions will encourage cross-regional networking among
techies, human rights defenders and rights activists to share strategies and
technical advice and support.

**Speakers**

- Gerard Harris, [Deflect](http://Deflect.ca)
- Sarah Clark, [Pen International](http://pen-international.org) 
- Shahzad Ahmad, [Bytes for All Pakistan](http://bytesforall.pk) 
- Octavia Jonsdottir, [IREX](http://irex.org)
- Mahmood Enayat, [Small Media Foundation](http://smallmediafoundation.com)
- Robert Guerra, [Citizen Lab](https://citizenlab.org)
- Chris Riley, Mozilla
- Ellery Biddle, [Global Voices](http://globalvoicesonline.org)


**Event details**

Date/time: 19:00 – 22:00 Monday, 21 October 2013

Venue: Mantra Nusa Dua1 ballroom

Transport: IGF shuttle bus

Attendance: Approx. 100 people

Food: Light snacks and drinks


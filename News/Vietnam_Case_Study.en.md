---
layout: default
title: Vietnam Case Study
lang: en
date: 2013-05-29
ref: Vietnam_Case_Study
tags:
 - Deflect News
---


Vietnam Case Study
==================

In March of this year we were approached by a prominent Vietnamese news media
website interested in joining the Deflect network. The site receives around
100,000 legitimate visitors a day but also experiences regular, high bandwidth
DDoS attacks.

We set up a test environment to evaluate the site's functionality. After some
analysis of the traffic logs, we decided to create a subsidiary Deflect network
purpose-built to get hit by what appeared to be frequent DDoS traffic, keeping
the Deflect infrastructure and our other partners unburdened. The site went live
on 22nd March and immediately attracted a slew of DDoS attacks, the results of
which are documented within

Over a 24 hour period there were around 20 million page requests from 13000
unique IPs. 80% of requests came from IP ranges reserved to the VietNam Post and
Telecom Corporation (VNPT) and hosted by large commercial telecoms providers
such as VietNam Post and Telecom Corporation, FPT Telecom Company, Vietel
Corporation, SaiGon Tourist Cable Television. By contrast, the next most popular
country was the US, with IPs requesting the site 8% of the time. Most botnets
are structurally transnational, built from a wide range of IPs, concentrated in
countries with high-speed, high connectivity to the internet. Since the IP
ranges were so closely bunched together, we do not believe they represent
unique, infected machines distributed randomly across the world. This appears to
be a 'smoking gun' of a botnet built on a national Internet infrastructure.

- Total visitors: 30,861 
- Duration Average: 1,673 s - a large number of short "stay" duration and a
  large number of long "stay" durations
- Top OS: Windows - 99%
- Top Browser: Mozilla - 97%
- Bandwidth served: 483.03 GB
- Unique IPs: 12,942 in roughly 24 hours
- Most commonly requested page: /
- Most commonly returned server status: 502 - Received bad response from real server

![Metrics](../img/Dltotals.png)

![Stats by country](../img/Dltraffic.png)

![IPs](../img/Dlip.png)

In a sense, botnets are like foreign mercenaries or private armies for hire.
They often fulfill a similar purpose: help prop governments up or bring them
down. Though difficult to prove, one theory is that the botnet employed in this
attack has been purpose-built and coordinated at a national level, akin to the
country's armed forces attacking targets perceived as a threat to national
security.

In contrast to most botnet behavior we have witnessed so far, this one appeared
to frequently rotate the attack pattern and adapt to our methods of defense. The
request header would change so as too fool our IP banning technology. 

We are currently working with the client to help them toughen the origin server
and are looking forward to protecting them from similar and new attacks in the
coming future.

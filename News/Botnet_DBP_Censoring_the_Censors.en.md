---
layout: default
title: Botnet DBP - Censoring the Censors
lang: en
date: 2014-03-03
ref: Botnet_DBP_Censoring_the_Censors
tags:
 - Deflect News
---


Taken from the Press Release to respected members of the technical community:
 
The Deflect team has spent the last two years mitigating DDoS attacks against
independent media and human rights websites. We've learnt a thing or two along
the way and have put a lot of effort into developing open source software to
make our lives (and weekends) a bit easier. The [BotnetDBP](../BotnetDBP_Intro)
project consists of four components to detect and ban malicious bots.

Banjax: responsible for early stage filtering, challenging and banning of bots,
identified via regular expression matching Learn2Ban: introduces intelligent,
adaptive features to botnet detection and banning by using a machine-learning
approach Botbanger: uses the support vector machine model constructed by
Learn2Ban to test HTTP traffic and determine the legitimacy of the requester
Swabber: is responsible for managing the actual banning of IP addresses
identified by either Banjax or Learn2ban

Go to the [GitHub repo](https://github.com/equalitie)

Of note. In our experiments, current Learn2Ban accuracy has been determined at
90% and above (i.e. both false positives and true negatives amounted to less
than 10%). In several cases, accuracy of 99% was achieved. We continue to
develop models based on larger attacks the network receives.

- See [Deflect Labs' reports](https://equalit.ie/deflect-labs-reporting)
- See our [News Archive](../News)

We rely on our community of peers and invite you to take a look at the code.
Your commentary and analysis are essential to seeing this open source initiative
mature and become of relevance to anyone running a web server.  For reference,
all components are built modularly and can be adapted to any web service
environment, albeit Banjax was written as an Apache Traffic Server plugin.

Those of you attending Rightscon and interested to hear more about our upcoming
participatory project "Distributed Deflect", come to lightning talk #3 on
Wednesday, March 5th, 4:00-5:15pm

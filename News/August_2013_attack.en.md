---
layout: default
title: August 2013 attack
lang: en
date: 2013-09-03
ref: August_2013_attack
tags:
 - Deflect News
---


August attack
=============

An unusual attack on the Deflect network
----------------------------------------

In late August the Deflect network came under a significant and sustained attack
for a 24 hour period, our banning system reporting a moderate number of bans in
tandem with a high number of connections and a significant amount of traffic.
When it first hit our primary Deflect network the attack reached a peak of close
to 100 megabits a second and over 8000 connections per edge, with the network
continuing to serve pages successfully and banning many of the attack sources.

However, on closer inspection of the attack traffic, the number of bans was
insufficient and the number of connections was attributable to a large number of
attackers using a technique that bypassed our caching mechanism. The attacks
targeted pages in such a manner that caching services would identify the request
as for a unique and uncached page. These requests took the form of the following
URL, with query strings randomised per-request:
```http://protected.site/?ERDCZER=ZYQRAYX```.

After profiling the attack URLs and patterns, we determined that a number of
suspicious and most likely falsified user agents were responsible for all of the
attack traffic. Some of these agents were quite innocuous at first glance but
featured anomalies, for example some attacks used the agent “Mozilla/5.0
(Windows; U; MSIE 9.0; WIndows NT 9.0; en-US)” - the Windows NT brand has been
discontinued and, if the brand were to be continued, [Windows 8 would be NT
6.3](https://en.wikipedia.org/wiki/Windows_NT#Releases Windows 8 would be NT
6.3).

The user agents indicated that the attackers were most likely using a stock or
modified version of the gfudder.py script from the charmingly named “Port
Groper” tool [1](http://sourceforge.net/projects/portgroper/). After absorbing some
of the traffic, we moved the site under attack to a distinct network so as to
avoid negatively affecting other hosted sites. Once the attacking hosts were
identified, we simply classified the attacking IP addresses and batch-blocked
them across all hosts as a temporary solution. Once this was accomplished we
implemented fail2ban rules that catch the attacking hosts with little chance of
false positives. These bans proved effective once the iptables rules for the IP
addresses were removed and the attack subsided majorly.

The botnet comprised over 3000 participants of varying capacity, although it is
somewhat unclear as to whether this botnet was a voluntarily conducted one as
part of a mass action, a privately owned botnet or a botnet rented by attackers
for the period of the attack. Of the easily locatable IP addresses, the vast
majority of attackers came from the United States, followed by a significantly
reduced number of attackers located in Vietnam, Russia, the United Arab
Emirates, Saudi Arabia, Germany and France.

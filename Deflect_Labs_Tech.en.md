---
layout: default
title: Deflect Labs - Technical Details
lang: en
ref: Deflect_Labs_Tech
tags:
 - Deflect Tech
 - Deflect Labs
---


# Deflect Labs Technical Details

Deflect Labs tests and defines the limits of attribution in the current and
evolving landscape of DDoS attacks.

At the core, the Deflect network is capable of logging information about any and
all aspects of the web traffic sent to origin web servers (this includes traffic
over SSL). This means that (unless the client running a Deflected website
requests us not to keep logs) for each visitor accessing the Deflect network it
is possible to record or otherwise ascertain:

- Site accessed
- Browser user agent
- Deflect server queried
- Time of request
- Response code to the request
- Cache status of the request

The key elements of Deflect Labs are:

- [Deflect](https://deflect.ca) itself, by its nature a rich target for DDoS attacks.
- [BotnetDBP](BotnetDBP_Intro) - a set of tools to actively and accurately
  differentiate between legitimate and malicious requests in order to further
  reduce the load on the Deflect network.
- [BotHound](BotHound) and [Grey Memory](Grey_Memory) - Bothound detects and
  classifies the attacks using the anomaly-detection and machine-learning tool
  Grey Memory.
* [Sniffles](Sniffles) and [Edgemanage](Edgemanage) provide an unfiltered view
  of inbound traffic, and give an unmistakable indicator of which site is
  targeted.
* [Opsdash](Opsdash) - ElasticSearch cluster where the majority of collected data is stored and queried.

![Deflect Labs Components](img/Deflect_Labs_Components-digraph.png)


We use these tools to gather, store and analyze information for attack
diagnostics and user-facing statistics, as well as to study series of attacks
and historical behaviours. What we can observe when analyzing bots through
Deflect Labs' components and open third-party resources includes:

- The geographic location of the bot (GeoIP databases lookups): This information
  can be used to inform decisions as to whether the bot is part of a
  malware-based botnet or a voluntary botnet using tools such as LOIC or other
  packaged denial of service tools that are used in participatory DDoS attacks.
  Bots proximity will be noted and can be used to indicate whether a high number
  of attackers are clustered geographically.
- Whether the visitor has used the site regularly: Hits on the aggregation
  system will be used to indicate whether the IP address has been seen before
- How much traffic a particular user has incurred
- Whether the user's profile has changed drastically since last visit:
  [learn2ban](https://github.com/equalitie/learn2ban) profiles visitor activity
  and uses machine-learning techniques to determine whether a user is behaving
  in a malicious non-human manner.
- Whether the user's profile matches that of a known bot type or bot cluster
  (Ban filter rules): Some malware toolkits use very obvious patterns of attack
  or identify themselves in an obvious manner. This information makes
  classification of attackers easy.
- Whether the host has been seen as part of a botnet in the past


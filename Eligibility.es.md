---
layout: default
title: Elegibilidad
lang: es
ref: Eligibility
tags:
 - Get started
---

Elegibilidad
============

Los individuos y grupos que requieren la protección de Deflect deben cumplir los
siguientes criterios:

- Usted es defensor de los derechos humanos; es responsable de una organización
  de la sociedad civil; produce un medio de comunicación independiente; o
  trabaja con personas que hacen alguna de estas cosas.
- Ha sido elegido como objetivo de ataques DDoS o tiene razones para temer o
  anticipar un ataque DDoS a causa del trabajo que realiza.
- Su trabajo no contraviene los [principios establecidos en la Declaración
  Universal de los Derechos
  Humanos](http://www.ohchr.org/EN/UDHR/Pages/Language.aspx?LangID=spn).
- Su trabajo no promueve discursos de odio ni anima a la discriminación.

---
layout: default
title: Support
lang: en
ref: Support
tags:
 - Features
---

Support
=======

The Deflect network is managed by a committed team of human beings scattered all
around the globe. This means that we can reply to support requests within 3
hours between Monday and Friday, and within 6 hours on the weekend. We also
speak several languages, including English, French, Russian, and Spanish.

Deflect clients can contact the support team through the Deflect Dashboard or by
writing to the support email address you will receive after subscribing to
Deflect.

Please, treat the Deflect support team with courtesy and respect, remembering
that this is a free best effort service run by some very dedicated and
overworked people. 

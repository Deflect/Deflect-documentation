---
title: Deflect Documentation
layout: default
lang: en
ref: Home
---


# Welcome to Deflect!

This is the public wiki for the Deflect project - a Distributed Denial of
Service [(DDoS)](href="http://en.wikipedia.org/wiki/Denial-of-service_attack)
mitigation service created to neutralize cyberattacks against independent media
and human rights defenders. Our goal is to create a community-driven, technical
response to the censorship of online voices caused by DDoS attacks.

## About Deflect

- [What is Deflect](About_Deflect "What is Deflect")
- [Why Deflect](Why_Deflect "Why Deflect")
- [Deflect in the Media](Deflect_in_the_Media "Deflect in the Media")
- [Who should consider Deflect](Who_Should_Consider_Deflect "Who should consider Deflect")
- [Who Uses Deflect](Testimonials "Testimonials")

## Get Protected

- [Terms of Service](ToS "Terms of Service") and [Privacy Notice](Privacy_Notice "Privacy Notice")
- [What you need to start](Assumptions "Assumptions")
- [What is the procedure](Procedure "Procedure")
- [Step-by-Step registration walkthrough](Dashboard_Walkthrough/Walkthrough "Walkthrough")

## [Secure Hosting with eQPress](eQPress)

## [Deflect Do-It-Yourself](Deflect_DIY)

<iframe frameborder="0" height="300" src="https://player.vimeo.com/video/93531470?title=0&amp;byline=0&amp;portrait=0"
webkitallowfullscreen="" width="400"></iframe>

This website aims to share collected experience and provide direction for those
requiring their own [Deflect instances](Deflect_DIY).  As
always, it is a work in progress and we welcome contributions and translations.

* Read the [instructions to contribute](contribute) to this documentation.
* Read the [instructions to translate](translate) this documentation.

Deflect is a free service, built on open source software and principles by
[eQualit.ie](https://equalit.ie), a not-for-profit technology collective based
in Montreal, Canada. Currently the team is made up of 7 dedicated members
working together on a large number of diverse tasks.  We all have separate
interests and experiences covering a wide range of disciplines, though we come
together through a shared vision of Internet Freedom and the firm belief that an
organised community is the best strategy to mitigate against DDoS, no matter the
scale of the attack.  Read more on our [website](https://equalit.ie)

![eQualit.ie email](/img/Email.jpg)

---
layout: default
title: Cookies
lang: fa
ref: Cookies
tags:
 - Deflect Tech
---


Cookieها در Deflect
-------------------

در Deflect پروتوکل Cookie به طور وسیعی پشتیبانی می‌شود، اگرچه Cookieها
موضوع مورد بحثی هستند. مثلا در بعضی کشورها Cookieها شامل قوائد قانونی
می‌شوند یا ممکن است توسط کاربران یا مسوولین سیستم‌ها به دلیل حفظ حریم
خصوصی از کار انداخته شده باشند.

سرآیندهای (header) درخواست Cookie و پاسخ Set-Cooke سرآیندهای ‌HTTP
‌end-to-end هستند، و بنابراین معمولا توسط پروکسی‌های HTTP بدون تغییر
منتقل می‌شوند. اما انتقال سرآیند پاسخ Set-Cookie به همان صورتی که هست
ممکن است در یک پروکسی وارونه (reverse proxy) ناکافی باشد. سرآیند
Set-Cookie ممکن است شامل اطلاعات Path و Domain باشد که در سرور اصلی
معتبر هستند، ولی وقتی که ‌reverse proxy فضای URL را دوباره سازی
(remap)می‌کند، بی‌معنی یا نادرست شوند.

Cookieها می‌توانند قابلیت cache شدن و بنابراین عملکرد را هم تحت تاثیر
قرار دهند.

مساله دوباره نویسی URLها در
<http://www.apachetutor.org/admin/reverseproxies> مورد بحث قرار گرفته
است. یکی از عوامل این دوباره نویسی اطلاعات ‌path و domain Cookieهاست.
این کار در Apache HTTPD's mod\_proxy از طریق ProxyPassReverseCookiePath
و ProxyPassReverseCookieDomain انجام شده‌است.

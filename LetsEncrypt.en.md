---
layout: default
title: Let's Encrypt
lang: en
ref: LetsEncrypt
tags:
 - Deflect Tech
 - Security
---

[Let's Encrypt](https://letsencrypt.org/) is a free certificate authority. It
allows users to automatically generate certificates for their web domains, for
free, in order to bypass the cartels that have controlled [SSL key
certification](https://en.wikipedia.org/wiki/Certificate_authority) so far. The
purpose is basically to make the process of setting up a secure website easier
so that secure websites can become ubiquitous.

Since the process of creating SSL certificates with Let's Encrypt is pretty
simple for anyone who can use a command line, many Deflect users may be
interested in generating their SSL certificates in this way. If you are planning
to join Deflect and want to generate SSL certificates with Let's Encrypt before,
follow the [instructions](https://letsencrypt.org/howitworks/) in the Let's
Encrypt website.

If, on the other hand, your website is already under the Deflect network, and
you want to generate new SSL certificates with Let's Encrypt, there are some
details you need to know for the certificates to work correctly.

Let's Encrypt tries to make configuration as easy as possible for users with its
letsencrypt-auto tool, but unfortunately won't work for Deflect, and sites under
Deflect protection fail to verify the certificate names when queried. In order
to prevent this from being an issue, and also to ensure uptime, you can use the
<tt>webroot</tt> option to write the required data directly to the server's
webroot. An example of a generation command for equalit.ie is:

<code>./letsencrypt-auto certonly -d equalit.ie,www.equalit.ie
--webroot-path /var/www/equalit.ie/</code>

*Depending on how your webserver is set up, the path /var/www/example.com/
should be replaced with the correct path. If, for example, your CMS and content
files are in the /wordpress folder, the path will be:
/var/www/example.com/wordpress*


These certificates then need to be
[uploaded](Dashboard_Walkthrough/Step5_Control_Panel#Website_settings) to the Dashboard as normal.

---
layout: default
title: Grey Memory
lang: en
ref: Grey_Memory
tags:
 - Deflect Tech
 - Deflect Labs
---

Grey Memory
===========

Grey Memory is the part of the Deflect Labs system that spots changes in the
patterns of traffic arriving from website visitors and decides whether they are
significant. It can either observe live data or work on historical logs. The
module carves up time into fixed-length segments and then, based on the traffic
it has seen in previous segments, tries to predict what the data in the next
segment should look like. If the data is significantly different from its
prediction, Grey Memory notes this as an anomaly, and when the rate of anomalies
exceeds a certain threshold, it flags the event for analysis by
[BotHound](BotHound).
